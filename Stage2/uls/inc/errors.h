#pragma once

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "uls.h"

#define ARG_RPL "[arg]"

#define USAGE "usage: uls [-[arg]] [file ...]\n"

#define INVALID_OPTION_ERR "uls: illegal option -- [arg]\n"

#define STD_ERR_PREFIX "uls: [arg]"

void mx_show_open_err(string_t file);

void mx_throw_option_err(char option);

void mx_show_usage(void);
