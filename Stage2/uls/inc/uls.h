#pragma once

#define _XOPEN_SOURCE 700

#define STD_BUF_SIZE 1024

#include "../libmx/inc/libmx.h"

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <fts.h>
#include <sys/xattr.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/acl.h>

#include "errors.h"
#include "constants.h"

typedef struct item_s {
    string_t name;
    string_t pathname;
    struct stat stat;
    char type;
    string_t user;
    string_t group;
} item_t;

typedef struct ctx_s {
    int is_first;
    string_t *initial_args;
    int initial_args_len;
} ctx_t;

typedef struct flags_s {
    int accesstime;     /* use time of last access */

    int humanval;       /* show human-readable file sizes */

    int longform;       /* long listing format */

    int listdot;        /* list files beginning with . */

    int pathdot;        /* show . and .. */

    int recursive;      /* ls subdirectories also */

    int reversesort;    /* reverse whatever sort is used */

    int sectime;        /* print the real time for all files */

    int singlecol;      /* use single column output */

    int statustime;     /* use time of last mode change */

    int timesort;       /* sort by time vice name */

    int sizesort;       /* sort by size */

    int color;          /* colored output depending on fle type*/

    int xattr;          /* extended attributes */

    int acl;

    int console_width;
} flags_t;

#define IS_SYSPATH(x) (mx_streq((x), "..") || mx_streq((x), "."))

char *mx_parse_opts(int argc, string_t *argv);

char mx_get_type(unsigned int mode);

string_t *mx_get_filenames(int argc, string_t *argv, int *files_count);

void mx_print_aligned_str(int width, string_t str);

void mx_print_left_aligned_str(int width, string_t str);

void mx_print_long(item_t file, flags_t flags, int * col_length);

int mx_print_items_long(item_t **items, int len, bool (*show)(item_t *), flags_t flags, int simple);

int mx_print_items_short(item_t **items, int len, bool (*show)(item_t *), flags_t flags);

void mx_normalize_items(item_t **items, int *count);

void mx_process_args(string_t *files, int files_count, string_t current_dir, ctx_t ctx);

int mx_print_items(item_t **items, int len, bool (*show)(item_t *), flags_t flags, int simple);

string_t *mx_get_dir_content(string_t dirname, int *items_count);

string_t mx_path_join(string_t a, string_t b);

string_t mx_check_gr(unsigned int id);

string_t mx_check_pw(unsigned int id);

int mx_get_table_size(item_t **items,int file_count, flags_t flags, int*col, int*row);

item_t **mx_filter_items(item_t **items, int len, bool (*show)(item_t *), int *new_size);

string_t mx_mem_recount(int input);
