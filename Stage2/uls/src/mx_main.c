#include "../inc/uls.h"

static flags_t flags;

static void apply_options(int argc, string_t *argv) {
    int is_tty = isatty(STDOUT_FILENO);
    flags.humanval = 0;
    flags.longform = 0;
    flags.listdot = 0;
    flags.pathdot = 0;
    flags.recursive = 0;
    flags.singlecol = !is_tty;
    flags.reversesort = 0;
    flags.accesstime = 0;
    flags.statustime = 0;
    flags.timesort = 0;
    flags.sizesort = 0;
    flags.sectime = 0;
    flags.color = 0;
    flags.xattr = 0;
    flags.acl = 0;

    if (is_tty) {
        struct winsize size;
        ioctl(STDIN_FILENO, TIOCGWINSZ, &size);
        flags.console_width = size.ws_col;
    } else
        flags.console_width = 80;

    char *opts = mx_parse_opts(argc, argv);
    for (int i = 0; opts[i]; i++)
        switch (opts[i]) {
            case '1':
                flags.singlecol = 1;
                flags.longform = 0;
                break;
            case 'C':
                flags.longform = 0;
                flags.singlecol = 0;
                break;
            case 'l':
                flags.longform = 1;
                flags.singlecol = 0;
                break;
            case 'c':
                flags.statustime = 1;
                flags.accesstime = 0;
                break;
            case 'u':
                flags.accesstime = 1;
                flags.statustime = 0;
                break;
            case 'R':
                flags.recursive = 1;
                break;
            case 'a':
                flags.pathdot = 1;
                break;
            case 'A':
                flags.pathdot = 0;
                flags.listdot = 1;
                break;
            case 'h':
                flags.humanval = 1;
                break;
            case 'r':
                flags.reversesort = 1;
                break;
            case 'S':
                flags.sizesort = 1;
                break;
            case 'T':
                flags.sectime = 1;
                break;
            case 't':
                flags.timesort = 1;
                break;
            case 'G':
                flags.color = 1;
                break;
            case '@':
                flags.xattr = 1;
                break;
            case 'e':
                flags.acl = 1;
        }
    mx_strdel(&opts);
}

static bool item_cmp(item_t *a, item_t *b) {
    if (flags.sizesort && a->stat.st_size != b->stat.st_size)
        return (a->stat.st_size < b->stat.st_size) != flags.reversesort;
    if (flags.timesort || flags.accesstime || flags.statustime) {
        time_t a_t = a->stat.st_mtime;
        time_t b_t = b->stat.st_mtime;
        if (flags.accesstime) {
#if __linux__
            a_t = a->stat.st_atime;
            b_t = b->stat.st_atime;
#elif __APPLE__
            a_t = a->stat.st_atimensec;
            b_t = b->stat.st_atimensec;
#endif
        }
        if (flags.statustime) {
            a_t = a->stat.st_ctime;
            b_t = b->stat.st_ctime;
        }
        if (a_t != b_t)
            return (a_t < b_t) != flags.reversesort;
    }
    return (mx_strcmp(a->name, b->name) > 0) != flags.reversesort;
}

static int count_expands(item_t **items, int len, bool (*accept)(item_t *)) {
    int count = 0;
    for (int i = 0; i < len; ++i)
        if (items[i]->type == T_DIR && accept(items[i]) && !IS_SYSPATH(items[i]->name))
            count++;
    return count;
}

static bool accept_name(string_t name) {
    if (flags.pathdot) return true;
    if (flags.listdot) return !IS_SYSPATH(name);
    return name[0] != '.';
}

static bool accept_item(item_t *a) {
    return accept_name(a->name);
}

static bool accept_no_dir(item_t *a) {
    return a->type != T_DIR;
}

void mx_process_args(string_t *names, int items_count, string_t current_dir, ctx_t ctx) {
    item_t **items = (item_t **) malloc(sizeof(item_t *) * items_count);
    for (int i = 0; i < items_count; ++i) {
        item_t *file = items[i] = (item_t *) malloc(sizeof(item_t));
        file->pathname = mx_path_join(current_dir, names[i]);
        if (lstat(ctx.is_first ? names[i] : file->pathname, &(file->stat)) == -1) {
            mx_show_open_err(ctx.is_first ? names[i] : file->pathname);
            free(file->pathname);
            free(file);
            items[i] = NULL;
            continue;
        }
        file->type = mx_get_type(file->stat.st_mode);
        file->name = names[i];
        file->user = mx_check_pw(file->stat.st_uid);
        file->group = mx_check_gr(file->stat.st_gid);
    }
    mx_normalize_items(items, &items_count);
    mx_sort((void **) items, items_count, (cmp_t) &item_cmp);

    if (ctx.is_first) {
        ctx.is_first = 0;
        int files_count = mx_print_items(items, items_count, &accept_no_dir, flags, 1);
        int dir_num = 0;
        int dir_count = items_count - files_count;
        if (files_count && dir_count)
            mx_printchar('\n');
        for (int i = 0; i < items_count; ++i) {
            if (items[i]->type != T_DIR) continue;
            dir_num++;
            int dir_files_count;
            string_t *dir_files = mx_get_dir_content(items[i]->name, &dir_files_count);
            if (items_count > 1 && !flags.recursive) {
                mx_printstr(items[i]->name);
                mx_printstr(":\n");
            }
            if (dir_files_count)
                mx_process_args(dir_files, dir_files_count, items[i]->name, ctx);
            if (dir_num != dir_count)
                mx_printchar('\n');

            for (int j = 0; j < dir_files_count; ++j)
                free(dir_files[j]);
            free(dir_files);
        }
    } else {
        if (!flags.recursive) {
            mx_print_items(items, items_count, &accept_item, flags, 0);
        } else {
            int expands = count_expands(items, items_count, &accept_item);
            if (ctx.initial_args_len > 1 || !mx_some_is((void *)ctx.initial_args, ctx.initial_args_len, (cmp_t)&mx_streq, current_dir)) {
                mx_printstr(current_dir);
                mx_printstr(":\n");
            }
            mx_print_items(items, items_count, &accept_item, flags, 0);
            if (expands) mx_printchar('\n');
            int expanded_count = 0;
            for (int i = 0; i < items_count; ++i) {
                item_t *file = items[i];
                if (file->type != T_DIR || !accept_item(file) || IS_SYSPATH(file->name)) continue;
                expanded_count++;
                int dir_files_count;
                string_t *dir_files = mx_get_dir_content(items[i]->pathname, &dir_files_count);
                mx_process_args(dir_files, dir_files_count, items[i]->pathname, ctx);
                if (expanded_count != expands)
                    mx_printchar('\n');

                for (int j = 0; j < dir_files_count; ++j)
                    free(dir_files[j]);
                free(dir_files);
            }
        }
    }
    for (int i = 0; i < items_count; ++i) {
        item_t *item = items[i];
        free(item->pathname);
        free(item->group);
        free(item->user);
        free(item);
    }
    free(items);
}

int main(int argc, string_t *argv) {
    apply_options(argc, argv);
    int items_count;
    string_t *files = mx_get_filenames(argc, argv, &items_count);
    if (!items_count) {
        free(files);
        files = (string_t *) malloc(sizeof(string_t));
        files[0] = ".";
        items_count++;
    }

    ctx_t ctx;
    ctx.is_first = 1;
    ctx.initial_args = files;
    ctx.initial_args_len = items_count;
    mx_process_args(files, items_count, ".", ctx);
    free(files);
    return 0;
}
