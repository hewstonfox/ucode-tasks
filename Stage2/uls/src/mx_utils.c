#include "../inc/uls.h"

void mx_normalize_items(item_t **items, int *count) {
    int newCount = 0;
    for (int i = 0; i < *count; ++i)
        if (items[i]) items[newCount++] = items[i];;
    for (int i = newCount; i < *count; ++i) items[i] = NULL;
    *count = newCount;
}

char mx_get_type(unsigned int mode) {
    if (S_ISDIR(mode))
        return 'd';
    else if (S_ISLNK(mode))
        return 'l';
    else if (S_ISBLK(mode))
        return 'b';
    else if (S_ISCHR(mode))
        return 'c';
    else if (S_ISFIFO(mode))
        return 'p';
    else if (S_ISSOCK(mode))
        return 's';
    else
        return '-';
}

string_t mx_check_gr(unsigned int id) {
    struct group *gr = getgrgid(id);
    return gr ? mx_strdup(gr->gr_name) : mx_itoa(id);
}

string_t mx_check_pw(unsigned int id) {
    struct passwd *pw = getpwuid(id);
    return pw ? mx_strdup(pw->pw_name) : mx_itoa(id);
}

int mx_get_table_size(item_t **items, int file_count, flags_t flags, int *col, int *row) {
    if (file_count == 0) {
        *col = 0;
        *row = 0;
        return 0;
    }
    int width = flags.console_width;
    int max_len = 0;
    for (int i = 0; i < file_count; i++) {
        int len = mx_strlen(items[i]->name);
        if (max_len < len)
            max_len = len;
    }

    while (++max_len % 8);
    *col = width / max_len;
    if (*col == 0) *col = 1;
    *row = (int) ((float) file_count / (float) (*col)) + (file_count % *col != 0);
    return max_len;
}

item_t **mx_filter_items(item_t **items, int len, bool (*show)(item_t *), int *new_size) {
    *new_size = 0;
    item_t **new_arr = (item_t **) malloc(sizeof(item_t) * len);
    for (int i = 0; i < len; i++)
        if (show(items[i]))
            new_arr[(*new_size)++] = items[i];
    return new_arr;
}

string_t mx_mem_recount(int input)
{
    int counter = 0;
    float number = (float)input;
    string_t numpart;
    string_t sizepart = mx_strnew(2);
    while (number > 1024) {number /= 1024; counter++;}
    if (number < 10) {
        int var = (int)(number*100);
        if (var % 10 < 5)
        var += 10;
        numpart = mx_itoa(var);
        numpart[2] = numpart[1];
        numpart[1] = '.';
    } else 
    {
        numpart = mx_itoa((int)(number+0.5));
    }
    switch (counter)
    {
    case 0:
        sizepart [0] = 'B';
        break;
    case 1:
        sizepart [0] = 'K';
        break;
    case 2:
        sizepart [0] = 'M';
        break;
    case 3:
        sizepart [0] = 'G';
        break;
    case 4:
        sizepart [0] = 'T';
        break;
    }
    string_t answer = mx_strjoin(numpart,sizepart);
    mx_strdel(&numpart);
    mx_strdel(&sizepart);
    return(answer);
}
