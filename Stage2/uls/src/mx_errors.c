#include "../inc/errors.h"

void mx_show_open_err(string_t file) {
    string_t err = mx_replace_substr(STD_ERR_PREFIX, ARG_RPL, file);
    perror(err);
    mx_strdel(&err);
}

void mx_show_usage(void) {
    string_t err = mx_replace_substr(USAGE, ARG_RPL, AVAILABLE_FLAGS);
    mx_printerr(err);
    mx_strdel(&err);
}

void mx_throw_option_err(char option) {
    char str_opt[2] = {option, '\0'};
    string_t err = mx_replace_substr(INVALID_OPTION_ERR, ARG_RPL, str_opt);
    mx_printerr(err);
    mx_strdel(&err);
    mx_show_usage();
    exit(2);
}
