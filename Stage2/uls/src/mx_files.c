#include "../inc/uls.h"

string_t *mx_get_filenames(int argc, string_t *argv, int *files_count) {
    string_t *files = (string_t *) malloc((int) sizeof(string_t) * argc);
    *files_count = 0;
    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') continue;
        files[(*files_count)++] = argv[i];
    }
    string_t *files_ = (string_t *) malloc((int) sizeof(string_t) * *files_count);
    for (int i = 0; i < *files_count; ++i)
        files_[i] = files[i];
    free(files);
    return files_;
}

string_t *mx_get_dir_content(string_t dirname, int *items_count) {
    DIR *dir;
    struct dirent *entry;
    *items_count = 0;

    dir = opendir(dirname);
    if (!dir) {
        mx_show_open_err(dirname);
        return NULL;
    };

    while (readdir(dir) != NULL) (*items_count)++;
    closedir(dir);

    dir = opendir(dirname);
    string_t *items = (string_t *) malloc(sizeof(string_t) * *items_count);

    int i = 0;
    while ((entry = readdir(dir)) != NULL) items[i++] = mx_strdup(entry->d_name);
    closedir(dir);
    return items;
}

string_t mx_path_join(string_t a, string_t b) {
    int len_a = mx_strlen(a);
    int len_b = mx_strlen(b);
    while (len_a > 0 && a[len_a - 1] == '/') len_a -= 1;
    while (len_b > 0 && b[len_b - 1] == '/') len_b -= 1;
    string_t path = mx_strnew(len_a + len_b + 1);

    mx_strncpy(path, a, len_a);
    path[len_a] = '/';
    mx_strncpy(path + len_a + 1, b, len_b);
    path[len_a + len_b + 1] = '\0';
    return path;
}
