#include "../inc/uls.h"

char *mx_parse_opts(int argc, string_t *argv) {
    char *opts = (char *) malloc(sizeof(char));
    int size = 1;
    opts[0] = '\0';
    for (int i = 1; i < argc; ++i) {
        string_t opt = argv[i];
        if (*opt != '-') continue;
        opt++;
        while (*opt) {
            if (mx_get_char_index(AVAILABLE_FLAGS, *opt) == -1) {
                free(opts);
                mx_throw_option_err(*opt);
                return NULL;
            }
            opts = mx_realloc(opts, size + 1);
            opts[size - 1] = *opt;
            opts[size++] = '\0';
            opt++;
        }
    }
    return opts;
}
