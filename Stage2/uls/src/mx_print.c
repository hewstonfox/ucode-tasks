#include "../inc/uls.h"

static void print_exec_permission(unsigned int mode) {
    if (mode & S_IXOTH) {
        if (mode & S_ISVTX) mx_printchar ('t');
        else mx_printchar('x');
    } else {
        if (mode & S_ISVTX) mx_printchar('T');
        else mx_printchar('-');
    }
}

static void print_permissions(unsigned int mode) {
    mx_printchar((mode & S_IRUSR) ? 'r' : '-');
    mx_printchar((mode & S_IWUSR) ? 'w' : '-');
    mx_printchar((mode & S_IXUSR) ? 'x' : '-');
    mx_printchar((mode & S_IRGRP) ? 'r' : '-');
    mx_printchar((mode & S_IWGRP) ? 'w' : '-');
    mx_printchar((mode & S_IXGRP) ? 'x' : '-');
    mx_printchar((mode & S_IROTH) ? 'r' : '-');
    mx_printchar((mode & S_IWOTH) ? 'w' : '-');
    print_exec_permission(mode);
}

static void print_colored_name(string_t name, char mode) {
    switch (mode) {
        case 'd':
            mx_printstr("\033[34m");
            break;
        case 'l':
            mx_printstr("\033[35m");
            break;
        case 'b':
            mx_printstr("\033[34;46m");
            break;
        case 'c':
            mx_printstr("\033[34;43m");
            break;
        case 'p':
            mx_printstr("\033[33m");
            break;
        case 's':
            mx_printstr("\033[32m");
            break;
        default:
            break;
    }
    mx_printstr(name);
    mx_printstr("\x1b[0m");
}

static void print_time(time_t mtime, string_t t, int sectime) {
    if (sectime)
        for (int i = 4; i < t[i]; i++)
            mx_printchar(t[i]);
    else {
        if (time(NULL) - 15768000 >= mtime) {
            int i;
            for (i = 4; i < 10; i++)
                mx_printchar(t[i]);
            mx_printstr("  ");
            for (i = 20; i < 24; i++)
                mx_printchar(t[i]);
        } else
            for (int i = 4; i < 16; i++)
                mx_printchar(t[i]);
    }
    mx_printchar(' ');
}

void mx_print_aligned_str(int width, string_t str) {
    int spaces = width - mx_strlen(str);
    for (int i = 0; i < spaces; i++) mx_printchar(' ');
    mx_printstr(str);
}

void mx_print_left_aligned_str(int width, string_t str) {
    int spaces = width - mx_strlen(str);
    int tabs = (spaces / 8 + (spaces % 8 != 0));
    mx_printstr(str);
    for (int i = 0; i < tabs; i++) mx_printchar('\t');
}

void mx_print_long(item_t file, flags_t flags, int *col_length) {
    time_t *time_info = &file.stat.st_mtime;
    string_t tmp;

    string_t xattr = mx_strnew(STD_BUF_SIZE);
    string_t xattr_value = mx_strnew(STD_BUF_SIZE);
    ssize_t xattr_len = listxattr(file.pathname, xattr, STD_BUF_SIZE, XATTR_NOFOLLOW);

    acl_t file_acl = acl_get_file(file.pathname, ACL_TYPE_EXTENDED);
    ssize_t acl_size;
    string_t acl_str;
    acl_str = acl_to_text(file_acl, &acl_size);

    int has_acl = file_acl != NULL;

    int has_xattr = xattr_len > 0;
    if (flags.accesstime)
        time_info = &file.stat.st_atime;
    if (flags.statustime)
        time_info = &file.stat.st_ctime;
    mx_printchar(file.type);
    print_permissions(file.stat.st_mode);
    if (has_xattr)
        mx_printchar('@');
    else if (has_acl)
        mx_printchar('+');
    else
        mx_printchar(' ');
    mx_printchar(' ');
    tmp = mx_itoa((int) file.stat.st_nlink);
    mx_print_aligned_str(col_length[0], tmp);
    free(tmp);
    mx_printchar(' ');
    mx_print_aligned_str(col_length[1], file.user);
    mx_printstr("  ");
    mx_print_aligned_str(col_length[2], file.group);
    mx_printstr("  ");
    if (!flags.humanval)
    tmp = mx_itoa((int) file.stat.st_size);
    else
    tmp = mx_mem_recount((int) file.stat.st_size);
    mx_print_aligned_str(col_length[3], tmp);
    free(tmp);
    mx_printchar(' ');
    print_time(file.stat.st_mtime, ctime(time_info), flags.sectime);
    if (flags.color)
        print_colored_name(file.name, file.type);
    else
        mx_printstr(file.name);
    if (file.type == T_SLINK) {
        mx_printstr(" -> ");
        string_t link = mx_strnew((int) file.stat.st_size);
        (void) readlink(file.pathname, link, file.stat.st_size);
        mx_printstr(link);
        free(link);
    }
    if (flags.xattr && has_xattr) {
        int gap = 0;
        while (gap <= xattr_len - 1) {
            string_t current_xattr = xattr + gap;
            getxattr(file.pathname, current_xattr, xattr_value, file.stat.st_size, 0, 0);
            mx_printchar('\n');
            mx_printstr("        ");
            mx_printstr(current_xattr);
            mx_printstr("        ");
            mx_printstr(xattr_value);
            gap += mx_strlen(current_xattr) + 1;
        }
    }
    if (has_acl && flags.acl) {
        mx_printchar('\n');
        mx_printstr(acl_str);
    }

    mx_printchar('\n');

    free(xattr);
    free(xattr_value);
    if (has_acl) {
        acl_free(file_acl);
        acl_free(acl_str);
    }
}

int mx_print_items_long(item_t **items, int len, bool (*show)(item_t *), flags_t flags, int simple) {
    unsigned long total = 0;
    int c = 0;
    int sizes[4] = {0, 0, 0, 0};
    for (int i = 0; i < len; ++i) {
        item_t *item = items[i];
        if (!show(item)) continue;
        c++;
        total += item->stat.st_blocks;
        string_t tmp;
        tmp = mx_itoa((int) item->stat.st_nlink);
        int nlink_w = mx_strlen(tmp);
        free(tmp);
        int user_w = mx_strlen(item->user);
        int group_w = mx_strlen(item->group);
        tmp = mx_itoa((int) item->stat.st_size);
        int size_w = mx_strlen(tmp);
        free(tmp);
        if (sizes[0] < nlink_w) sizes[0] = nlink_w;
        if (sizes[1] < user_w) sizes[1] = user_w;
        if (sizes[2] < group_w) sizes[2] = group_w;
        if (sizes[3] < size_w) sizes[3] = size_w;
    }
    if (!simple && c) {
        mx_printstr("total ");
        mx_printul(total);
        mx_printchar('\n');
    }
    for (int i = 0; i < len; ++i)
        if (show(items[i]))
            mx_print_long(*items[i], flags, sizes);
    return c;
}

int mx_print_items_short(item_t **items, int len, bool (*show)(item_t *), flags_t flags) {
    if (flags.singlecol) {
        int c = 0;
        for (int i = 0; i < len; ++i)
            if (show(items[i])) {
                if (c) mx_printchar('\n');
                c++;
                if (flags.color)
                    print_colored_name(items[i]->name, items[i]->type);
                else
                    mx_printstr(items[i]->name);
            }
        if (c) mx_printchar('\n');
        return c;
    }
    int _len;
    int col, row;
    item_t **filtered_items = mx_filter_items(items, len, show, &_len);
    int margin = mx_get_table_size(filtered_items, _len, flags, &col, &row);
    for (int r = 0; r < row; ++r) {
        for (int c = 0; c < col; ++c) {
            int idx = r + (row * c);
            if (idx >= _len) break;

            if (r + (row * (c + 1)) >= _len) {
                mx_printstr(filtered_items[idx]->name);
                mx_printchar('\n');
            } else
                mx_print_left_aligned_str(margin, filtered_items[idx]->name);
        }
    }

    free(filtered_items);
    return _len;
}

int mx_print_items(item_t **items, int len, bool (*show)(item_t *), flags_t flags, int simple) {
    if (flags.longform)
        return mx_print_items_long(items, len, show, flags, simple);
    else
        return mx_print_items_short(items, len, show, flags);
}
