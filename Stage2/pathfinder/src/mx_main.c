#include "../inc/pathfinder.h"

int main(int argc, char const* argv[]) {
    string_t filename = mx_validate_args(argc, argv);
    string_t data = mx_validate_file(filename);
    size_t islands_count;
    island_t** islands = mx_parse_data(data, &islands_count);
    free(data);

    mx_sort_bridges(islands, islands_count);

    for (size_t i = 0; i < islands_count - 1; i++) {
        for (size_t j = i + 1; j < islands_count; j++) {
            path_t* init_path = mx_create_path(islands[i]->name);
            path_arr_t* paths =
                mx_find_path(islands[i], islands[j], init_path, islands_count);
            for (size_t k = 0; k < paths->size; k++)
                mx_print_path(paths->data[k]);
            mx_del_path_arr(&paths);
            mx_del_path(&init_path);
        }
    }

    for (size_t i = 0; islands[i]; i++) mx_del_island(&islands[i]);
    free(islands);
}
