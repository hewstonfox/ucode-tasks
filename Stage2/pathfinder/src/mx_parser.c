#include "../inc/errors.h"
#include "../inc/pathfinder.h"

static void parse_line(island_t **iss, const string_t line, size_t idx,
                       size_t max_is, size_t *inc, size_t *len_sum) {
    if (!mx_is_valid_line(line)) mx_throw_line_err(idx);
    string_t *args = mx_strsplit(line, ',');
    string_t *cur_iss_names = mx_strsplit(args[0], '-');
    if (!mx_strcmp(cur_iss_names[0], cur_iss_names[1])) mx_throw_line_err(idx);
    island_t **cur_iss = (island_t **)malloc(sizeof(island_t *) * 2);
    for (size_t i = 0; i < 2; i++) {
        cur_iss[i] = mx_find_island(iss, cur_iss_names[i]);
        if (!cur_iss[i]) {
            if (max_is <= *inc) mx_throwerr(DATA_INVALID_ISLANDS_COUNT_ERR);
            iss[*inc] = cur_iss[i] = mx_create_island(cur_iss_names[i], *inc);
            (*inc)++;
        }
    }
    bool is_valid_sz;
    size_t len = mx_parse_size(args[1], &is_valid_sz);
    (*len_sum) += len;
    if (*len_sum > INT_MAX) mx_throwerr(DATA_TOO_BIG_BRIDGES_LEN_ERR);
    mx_add_new_bridge(cur_iss[0], len, cur_iss[1]);
    free(cur_iss);
    mx_del_strarr(&cur_iss_names);
    mx_del_strarr(&args);
}

island_t **mx_parse_data(const string_t data_str, size_t *len) {
    string_t *lines = mx_strsplit(data_str, '\n');
    bool valid_size;
    string_t error;
    *len = mx_parse_size(*lines, &valid_size);
    size_t islands_count = *len;
    if (!valid_size) {
        error = mx_replace_substr(DATA_LINE_ERR, REP_LINE, "1");
        mx_printerr(error);
        mx_strdel(&error);
        exit(0);
    }
    island_t **islands =
        (island_t **)malloc(sizeof(island_t *) * (islands_count + 1));
    for (size_t i = 0; i <= islands_count; i++) islands[i] = NULL;
    size_t max_islnd = 0;
    size_t sum_len = 0;
    for (size_t i = 1; lines[i]; i++)
        parse_line(islands, lines[i], i + 1, islands_count, &max_islnd,
                   &sum_len);
    if (max_islnd != islands_count) mx_throwerr(DATA_INVALID_ISLANDS_COUNT_ERR);
    mx_del_strarr(&lines);
    return islands;
}

bool mx_bridges_cmp(bridge_t *a, bridge_t *b) {
    return a->dst->index > b->dst->index;
}

void mx_sort_bridges(island_t **islands, size_t len) {
    for (size_t i = 0; i < len; i++) {
        island_t *isl = islands[i];
        mx_sort((void **)isl->bridges, isl->bridges_count,
                (cmp_t)&mx_bridges_cmp);
    }
}
