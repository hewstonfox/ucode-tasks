#include "../inc/errors.h"

void mx_throw_line_err(size_t index) {
    string_t error = mx_replace_substr(DATA_LINE_ERR, REP_LINE, mx_itoa(index));
    mx_printerr(error);
    mx_strdel(&error);
    exit(0);
}
