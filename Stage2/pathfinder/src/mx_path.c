#include "../inc/errors.h"
#include "../inc/path.h"

island_t* mx_create_island(const string_t name, size_t index) {
    island_t* island = (island_t*)malloc(sizeof(island_t));
    island->name = mx_strdup(name);
    island->bridges = (bridge_t**)malloc(sizeof(bridge_t*) * 0);
    island->bridges_count = 0;
    island->index = index;
    return island;
}

bool mx_is_islands_eq(island_t* a, island_t* b) {
    return mx_streq(a->name, b->name);
}

void mx_add_bridge(island_t* is, bridge_t* br) {
    is->bridges = (bridge_t**)mx_realloc(
        is->bridges, sizeof(bridge_t*) * (is->bridges_count + 1));
    is->bridges[is->bridges_count] = br;
    is->bridges_count++;
}

void mx_add_new_bridge(island_t* a, int len, island_t* b) {
    for (size_t i = 0; i < a->bridges_count; i++)
        if (!mx_strcmp(b->name, a->bridges[i]->dst->name))
            mx_throwerr(DATA_DUPLICATE_BRIDGES_ERR);
    for (size_t i = 0; i < b->bridges_count; i++)
        if (!mx_strcmp(a->name, b->bridges[i]->dst->name))
            mx_throwerr(DATA_DUPLICATE_BRIDGES_ERR);
    mx_add_bridge(a, mx_create_bridge(len, b));
    mx_add_bridge(b, mx_create_bridge(len, a));
}

bridge_t* mx_create_bridge(int len, island_t* is) {
    bridge_t* br = (bridge_t*)malloc(sizeof(bridge_t));
    br->len = len;
    br->dst = is;
    return br;
}

bool is_eq_islands(island_t* a, island_t* b) {
    return mx_strcmp(a->name, b->name) == 0;
}

bool is_island_exist(island_t** iss, const string_t name) {
    for (size_t i = 0; iss[i]; i++)
        if (!mx_strcmp(iss[i]->name, name)) return true;
    return false;
}

island_t* mx_find_island(island_t** iss, const string_t name) {
    for (size_t i = 0; iss[i]; i++)
        if (!mx_strcmp(iss[i]->name, name)) return iss[i];
    return NULL;
}

void mx_del_island(island_t** is) {
    mx_strdel(&(*is)->name);
    for (size_t i = 0; i < (*is)->bridges_count; i++) free((*is)->bridges[i]);
    free((*is)->bridges);
    free(*is);
    *is = NULL;
}

path_t* mx_create_path(const string_t src) {
    path_t* path = (path_t*)malloc(sizeof(path_t));
    path->computes = (int*)malloc(sizeof(int) * 0);
    path->points = (string_t*)malloc(sizeof(string_t) * 1);
    path->points[0] = mx_strdup(src);
    path->points_len = 0;
    path->size = 0;
    return path;
}

void mx_add_path_point(path_t* path, const string_t name, int len) {
    path->computes =
        (int*)mx_realloc(path->computes, (path->points_len + 1) * sizeof(int));
    path->points = (string_t*)mx_realloc(
        path->points, (path->points_len + 2) * sizeof(string_t));
    path->computes[path->points_len] = len;
    path->points[path->points_len + 1] = mx_strdup(name);
    path->points_len++;
    path->size += len;
}

path_t* mx_dup_path(path_t* src) {
    if (!src) return NULL;
    path_t* path = (path_t*)malloc(sizeof(path_t));
    path->points_len = src->points_len;
    path->size = src->size;
    path->computes = (int*)malloc(sizeof(int) * src->points_len);
    for (size_t i = 0; i < src->points_len; i++)
        path->computes[i] = src->computes[i];
    path->points = (string_t*)malloc(sizeof(string_t) * (src->points_len + 1));
    for (size_t i = 0; i <= src->points_len; i++)
        path->points[i] = mx_strdup(src->points[i]);
    return path;
}

void mx_del_path(path_t** path) {
    for (size_t i = 0; i <= (*path)->points_len; i++)
        mx_strdel(&(*path)->points[i]);
    free((*path)->points);
    free((*path)->computes);
    (*path)->computes = NULL;
    free(*path);
    *path = NULL;
}

path_arr_t* mx_create_path_arr() {
    path_arr_t* path_arr = (path_arr_t*)malloc(sizeof(path_arr_t));
    path_arr->size = 0;
    path_arr->data = (path_t**)malloc(sizeof(path_t*) * 0);
    return path_arr;
}

void mx_add_path(path_arr_t* path_arr, path_t* path) {
    path_arr->data = (path_t**)mx_realloc(
        path_arr->data, (path_arr->size + 1) * sizeof(path_t*));
    path_arr->data[path_arr->size] = path;
    path_arr->size++;
}

void mx_extend_path_arr(path_arr_t* dst, path_arr_t* src) {
    dst->data = (path_t**)mx_realloc(dst->data,
                                     (dst->size + src->size) * sizeof(path_t*));
    for (size_t i = 0; i < src->size; i++)
        dst->data[i + dst->size] = src->data[i];
    dst->size += src->size;
    free(src->data);
    free(src);
}

void mx_clean_path_arr(path_arr_t* path_arr) {
    for (size_t i = 0; i < path_arr->size; i++)
        mx_del_path(&(path_arr->data[i]));
    free(path_arr->data);
    path_arr->data = (path_t**)malloc(sizeof(path_t*) * 0);
    path_arr->size = 0;
}

void mx_del_path_arr(path_arr_t** path_arr) {
    for (size_t i = 0; i < (*path_arr)->size; i++)
        mx_del_path(&(*path_arr)->data[i]);
    free((*path_arr)->data);
    free(*path_arr);
    *path_arr = NULL;
}

path_arr_t* mx_find_path(island_t* src, island_t* dst, path_t* proto,
                         size_t max_depth) {
    long min_len = INT_MAX;
    path_arr_t* path_arr = mx_create_path_arr();
    for (size_t i = 0; i < src->bridges_count; i++) {
        bridge_t* br_tmp = src->bridges[i];
        if (mx_some_is((void**)proto->points, proto->points_len + 1,
                       (cmp_t)&mx_streq, br_tmp->dst->name))
            continue;
        int len = proto->size + br_tmp->len;
        if (len > min_len) continue;

        path_t* cur_path;

        if (!mx_strcmp(br_tmp->dst->name, dst->name)) {
            if (len < min_len) {
                min_len = len;
                mx_clean_path_arr(path_arr);
            }
            if (len == min_len) {
                cur_path = mx_dup_path(proto);
                mx_add_path_point(cur_path, br_tmp->dst->name, br_tmp->len);
                mx_add_path(path_arr, cur_path);
            }
            cur_path = NULL;
            continue;
        }

        if (proto->points_len >= max_depth) continue;
        cur_path = mx_dup_path(proto);
        mx_add_path_point(cur_path, br_tmp->dst->name, br_tmp->len);
        path_arr_t* deep_path_arr =
            mx_find_path(br_tmp->dst, dst, cur_path, max_depth);
        for (size_t j = 0; j < deep_path_arr->size; j++) {
            path_t* p = deep_path_arr->data[j];
            if (p->size < min_len) {
                min_len = p->size;
                mx_clean_path_arr(path_arr);
            }
            if (p->size == min_len)
                mx_add_path(path_arr, p);
            else
                mx_del_path(&p);
        }
        free(deep_path_arr->data);
        free(deep_path_arr);
        mx_del_path(&cur_path);
    }
    return path_arr;
}
