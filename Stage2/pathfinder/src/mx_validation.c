#include "../inc/errors.h"
#include "../inc/pathfinder.h"

string_t mx_validate_args(int argc, char const *argv[]) {
    if (argc != 2) mx_throwerr(USAGE_ERR);
    return (string_t)argv[1];
}

string_t mx_validate_file(const string_t filename) {
    string_t data = mx_file_to_str(filename);
    string_t error = NULL;
    if (data == NULL)
        error = mx_replace_substr(FILE_NOT_EXIST_ERR, REP_FILE, filename);
    else if (!mx_strcmp(data, ""))
        error = mx_replace_substr(FILE_EMPTY_ERR, REP_FILE, filename);
    if (error) {
        mx_printerr(error);
        mx_strdel(&error);
        if (data) mx_strdel(&data);
        exit(0);
    }
    return data;
}

bool mx_is_valid_line(string_t line) {
    bool valid = false;
    while (mx_isalpha(*line)) {
        valid = true;
        line++;
    }
    if (!valid) return false;
    if (*(line++) != '-') return false;
    valid = false;
    while (mx_isalpha(*line)) {
        valid = true;
        line++;
    }
    if (!valid) return false;
    if (*(line++) != ',') return false;
    valid = false;
    while (mx_isdigit(*line)) {
        valid = true;
        line++;
    }
    if (!valid) return false;
    return *line == '\0';
}
