#include "../inc/pathfinder.h"

void mx_print_path(path_t *p) {
    mx_printstr("========================================\nPath: ");
    mx_printstr(p->points[0]);
    mx_printstr(" -> ");
    mx_printstr(p->points[p->points_len]);
    mx_printstr("\nRoute: ");
    for (size_t i = 0; i <= p->points_len; i++) {
        if (i) mx_printstr(" -> ");
        mx_printstr(p->points[i]);
    }
    mx_printstr("\nDistance: ");
    if (p->points_len > 1)
        for (size_t i = 0; i < p->points_len; i++) {
            if (i) mx_printstr(" + ");
            mx_printint(p->computes[i]);
            if (i == p->points_len - 1) mx_printstr(" = ");
        }
    mx_printint(p->size);
    mx_printstr("\n========================================\n");
}
