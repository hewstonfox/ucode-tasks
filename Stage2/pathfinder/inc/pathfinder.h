#pragma once

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include "../libmx/inc/libmx.h"
#include "path.h"

string_t mx_validate_args(int argc, char const* argv[]);

string_t mx_validate_file(const string_t filename);

bool mx_is_valid_line(string_t line);

island_t** mx_parse_data(const string_t data_str, size_t* len);

void mx_print_path(path_t *p);

bool mx_bridges_cmp(bridge_t *a, bridge_t *b);

void mx_sort_bridges(island_t** islands, size_t len);
