#pragma once

#include <stdlib.h>

#include "../libmx/inc/libmx.h"

typedef struct island_s island_t;
typedef struct bridge_s bridge_t;
typedef struct path_s path_t;
typedef struct path_arr_s path_arr_t;

struct bridge_s {
    int len;
    island_t* dst;
};

struct island_s {
    string_t name;
    bridge_t** bridges;
    size_t bridges_count;
    size_t index;
};

struct path_s {
    size_t points_len;
    string_t* points;
    int* computes;
    int size;
};

struct path_arr_s {
    path_t** data;
    size_t size;
};

island_t* mx_create_island(const string_t name, size_t index);

void mx_add_bridge(island_t* is, bridge_t* br);

bridge_t* mx_create_bridge(int len, island_t* is);

void mx_add_new_bridge(island_t* a, int len, island_t* b);

bool is_eq_islands(island_t* a, island_t* b);

bool is_island_exist(island_t** iss, const string_t name);

island_t* mx_find_island(island_t** iss, const string_t name);

void mx_del_island(island_t** is);

path_t* mx_create_path(const string_t src);

void mx_add_path_point(path_t* path, const string_t name, int len);

path_t* mx_dup_path(path_t* src);

void mx_del_path(path_t** path);

path_arr_t* mx_create_path_arr();

void mx_add_path(path_arr_t* path_arr, path_t* path);

void mx_extend_path_arr(path_arr_t* dst, path_arr_t* src);

void mx_clean_path_arr(path_arr_t* path_arr);

void mx_del_path_arr(path_arr_t** path_arr);

path_arr_t* mx_find_path(island_t* src, island_t* dst, path_t* proto,
                         size_t max_depth);
