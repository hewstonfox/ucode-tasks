int mx_sqrt(int x) {
    if (x <= 0) {
        return 0;
    }

    float sqrt = x / 2;
    float prev_res = 0;
    while (sqrt != prev_res) {
        prev_res = sqrt;
        sqrt = (x / sqrt + sqrt) / 2;
    }

    return (float)(int)sqrt == sqrt ? (int)sqrt : 0;
}
