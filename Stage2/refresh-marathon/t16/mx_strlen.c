int mx_strlen(const char *s) {
    int count;
    for (count = 0; s[count]; count++);
    return count;
}
