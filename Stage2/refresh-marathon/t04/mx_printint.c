#include <limits.h>
#include <unistd.h>

void mx_printchar(char c);

void mx_printint(int n) {
    if (n == INT_MIN) {
        write(1, "-2147483648", 12);
        return;
    }
    if (n < 0) {
        mx_printchar('-');
        n *= -1;
    }
    if (n < 10) mx_printchar(n + '0');
    else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}
