void mx_printchar(char c);

void mx_print_alphabet(void) {
    char upper = 'A';
    char lower = 'b';

    while (lower <= 'z') {
        mx_printchar(upper);
        mx_printchar(lower);

        upper += 2;
        lower += 2;
    }

    mx_printchar('\n');
}
