#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

static bool isdigit(char c) { return c >= '0' && c <= '9'; }

static bool islower(int c) { return c >= 'a' && c <= 'z'; }

static bool is_hex(char c) {
    return isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

unsigned long mx_hex_to_nbr(const char *hex) {
    unsigned long long value = 0;
    unsigned long long prev_value = 0;
    if (hex == NULL) {
        return 0;
    }
    while (is_hex(*hex)) {
        prev_value = value;
        value *= 16;
        value += isdigit(*hex)   ? *hex - '0'
                 : islower(*hex) ? *hex - 'a' + 10
                                 : *hex - 'A' + 10;
        if (value <= prev_value) return 0;
        ++hex;
    }
    if (*hex != '\0' || value > ULONG_MAX) return 0;
    return value;
}
