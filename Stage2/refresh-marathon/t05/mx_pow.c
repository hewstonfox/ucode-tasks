double mx_pow(double n, unsigned int pow) {
    double result = 1.0;

    while (pow > 0) {
        result *= n;
        --pow;
    }

    return result;
}
