char *mx_strcpy(char *dst, const char *src) {
    char *it = dst;

    while ((*it++ = *src++) != '\0')
        ;

    return dst;
}
