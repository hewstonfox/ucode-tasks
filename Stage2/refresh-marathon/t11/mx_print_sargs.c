void mx_printchar(char c);

int mx_strcmp(const char *s1, const char *s2);

void mx_printstr(const char *s);

int main(int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        int swap = i;

        for (int j = argc - 1; j > i; --j)
            if (mx_strcmp(argv[swap], argv[j]) > 0)
                swap = j;

        char* temp = argv[i];
        argv[i] = argv[swap];
        argv[swap] = temp;
    }

    for (int i = 1; i < argc; ++i) {
        mx_printstr(*(argv + i));
        mx_printchar('\n');
    }

    return 0;
}
