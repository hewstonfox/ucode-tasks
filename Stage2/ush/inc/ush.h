#pragma once
#include <termios.h>
#include <sys/ioctl.h>
#include <spawn.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "../libmx/inc/libmx.h"
#include "pwd_b.h"
#include "env.h"
#include "echo.h"
#include "which.h"
#include "cd.h"
#define ENTER 10
#define BACKSPACE 127
#define UP 65
#define DOWN 66
#define RIGTH 67
#define LEFT 68
#define INPUT_STR "u$h> "
#define WHOAMI "`whoami`"
#ifdef __linux__
    #define WAIT_ANY -1
#endif
typedef struct s_job{
    pid_t pid;
    string_t path;
    string_t *argv;
    bool is_sus;
    bool is_buitin;
    int done;
    struct termios t;
}   t_job;

typedef struct s_shell{
    int shell_is_idle;
    struct termios t;
    string_t PWD;
    pid_t pid;
}   t_shell;

typedef struct s_inptctx{
    int startpos_x;
    int pos_x;
    int c_ch;
    int len_tty;
    string_t input;
}   t_inputctx;

typedef struct s_variable {
    char *key;
    char *value;
}       variable_t;

typedef enum commands {
    _CD,
    _ECHO,
    _ENV,
    _WHICH,
    _PWD,
    _ERROR,
    _EXIT,
    _EXPORT,
    _UNSET,
}   command_t;
 //
 void mx_do_job_notif(t_job *job);
 void mx_update_status(t_job *job);
// replace job
string_t mx_get_string(string_t cmd);
string_t mx_replace_once_quote(string_t s);
string_t mx_strarr_to_str(string_t *arr);
string_t mx_replace_hook(string_t s);
void mx_replace_spaces(string_t s);
string_t mx_remove_command_sum(string_t str);
string_t mx_replace_username(string_t s);
string_t mx_repalce_wildcard(string_t s);
string_t mx_get_wildcard(string_t to_rep);
string_t mx_del_wildcard_quote(string_t to_rep);
 // jobs
void mx_wait_for_job(t_job *job);
void mx_move_job_to_fg(t_job *job, int cont, t_shell *shl);
void mx_move_job_to_bg(t_job *job, int cont);
int mx_proc_stat(pid_t pid, int status, t_job *job);


int kill(pid_t pid, int sig);
bool mx_is_buitin_func(string_t name);
bool mx_is_in_dir(string_t path_file ,string_t name);
bool mx_is_buitin(string_t name);
int mx_check_jobs(t_list *jobs);
string_t mx_get_bin_path(void);
//signals
void mx_sigint();

// cd func
void mx_launch_exe_proces(t_job *job, t_shell *shl);
int mx_set_target(string_t target,string_t full_path, t_curflags *curflags, t_shell *shl);
void mx_restore_cur(t_inputctx *ctx);
void mx_set_cur_start(t_inputctx *ctx);
int mx_getch();
bool mx_is_empty(char *input);
bool mx_can_mv_left(t_inputctx *ctx);
bool mx_can_mv_right(t_inputctx *ctx);
bool mx_is_last_char(t_inputctx *ctx);
string_t mx_pop_char(t_inputctx *ctx, int pop_index);
string_t mx_insert_char(t_inputctx *ctx, int ch_index, char ch);
// mainloop
string_t mx_args_to_str(int argc, string_t *argv);
void mx_move_argv_left(string_t *argv);
command_t mx_fabric(string_t input);
string_t mx_get_util_path(string_t input);
char **mx_strsplit_modified(const char *s, char c);

int mx_mainloop(int argc, string_t *argv, t_shell *shl);

string_t mx_get_input(t_shell *shl);
//void mx_export(char *input, variable_t **variables, int *size);
int mx_export(string_t *input);
int mx_unset(string_t *input);
// & 
string_t *mx_replace_param(string_t *argv);
//replace

string_t mx_tilda(string_t s);
string_t mx_whoami(string_t s);
//oldc
string_t *mx_del_quote(string_t *argv);

//jobs
t_list *mx_update_job_list(t_list *jobs, string_t input);
int mx_check_jobs(t_list *jobs);
int mx_job_init(t_job *job, t_shell *shl);
int mx_init_buitin_job(t_job *job, t_shell *shl);
int mx_init_exe_job(t_job *job, t_shell *shl);
bool mx_is_separator(string_t input);
char *mx_get_util_name(string_t input);
    //builtin
int mx_cd_main(string_t *argv, t_shell *shl);

//free memory
void mx_del_job_list(t_list **jobs);
void mx_delete_job(t_job **job);

//errors
void mx_err_no_such_file(string_t name);
void mx_err_cmnd_not_found(string_t name);
string_t mx_remove_command_sum(string_t);
int main_env(string_t argv[], t_shell *shl);
int main_pwd(string_t *argv, t_shell *shell);
