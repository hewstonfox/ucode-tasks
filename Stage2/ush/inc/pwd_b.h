#pragma once

#include <pwd.h>

#include "../libmx/inc/libmx.h"

#define OPTIONS_PWD "LP"

#define ARG_RPL "[arg]"

#define USAGE "pwd: usage: pwd [-[arg]]\n"

#define INVALID_OPTION_ERR "ush: pwd: illegal option -- [arg]\n"


