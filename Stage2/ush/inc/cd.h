#pragma once

#include "../libmx/inc/libmx.h"

#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/stat.h>
#include <errno.h>

#define CD_OPTIONS "sP"
#define CD_COUNT_OPTIONS 2

typedef struct s_curflags {
    bool is_s;
    bool is_P;
}       t_curflags;

int setenv(const char *name, const char *value, int overwrite);
char *realpath(const char *restrict path,
                      char *restrict resolved_path);

//errors
void mx_not_a_directory(string_t target);
void mx_usage(void);
void mx_flag_err(char opt);
void mx_count_arg_err(void);
void mx_oldpwd_err(void);
// flag -P
int mx_set_cdpoint(void);
// cd func
bool mx_is_flag(string_t argv);

string_t mx_cd_shortcut(string_t target);
string_t mx_get_target(string_t *argv);
bool mx_is_one_charline(string_t str, char cmp);
char *mx_parse_cd_options(string_t *argv);
string_t mx_normalize_path(char* pwd, const char * src, char* res);
bool mx_check_path(string_t target, string_t path);

