#pragma once
#include "../libmx/inc/libmx.h"
#define OPTIONS_ENV "iPu"
#define COUNT_OPTIONS 3
#define OVERWRITE 1
#define NO_OVERWRITE 0

extern char **environ;
#if __linux__
    #define environ __environ
#endif

bool mx_is_empty_env(char *input);
int clearenv(void);
int setenv(const char *name, const char *value, int overwrite);
int unsetenv(const char *name);

void mx_clearenv(void);
void mx_del_arg_env(int argc, string_t argv[]);
int mx_count_size_newenv(int argc, string_t argv[]);
int mx_get_first_arg_index(int argc, string_t argv[]);
int mx_get_first_eq_sign(int argc, string_t argv[]);
string_t * mx_parse_newenv(int argc, string_t argv[]);
void mx_setarrenv(string_t* to_rep);
void mx_printenv(string_t *env);
// errors 
void mx_option_needs_arg(char opt);
void mx_usage(void);
void mx_invalid_option(char option);
void mx_no_such_file(string_t file);

