#pragma once

#include "../libmx/inc/libmx.h"

typedef struct options {
    bool is_n;
    bool is_E;
    bool is_e;
} options_t;

options_t *mx_opt_init(int opt_count);

int mx_process_options(int argc, char **argv, options_t *opt);

void mx_opt_fill(options_t *opt);

void mx_process_str(char *src, char *dst, options_t *opt);

void mx_del_opt(options_t *opt);

int main_echo(string_t *argv);

bool mx_is_empty(char *input);
