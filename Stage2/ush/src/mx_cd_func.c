#include "cd.h"
#include "ush.h"

char *mx_parse_cd_options(string_t *argv) {
    bool was_target = false;
    char *opts = (char *) malloc(sizeof(char));
    int size = 1;
    opts[0] = '\0';
    for (int i = 1; argv[i] != NULL; ++i) {
        string_t opt = argv[i];
        if (was_target) {
            return opts;
        }
        if (opt[0] != '-') {
            was_target = true;
            continue;
        } 
        opt++;
        while (*opt) {
            if (mx_get_char_index(CD_OPTIONS, *opt) == -1) {
                return NULL;
            }
            opts = mx_realloc(opts, size + 1);
            opts[size - 1] = *opt;
            opts[size++] = '\0';
            opt++;
        }
    }
    return opts;
}
bool mx_is_one_charline(string_t str, char cmp) {
    if (mx_strlen(str) == 0) return false;
    for (int i = 0; str[i]; i++) {
        if (str[i] != cmp) return false;
    }
    return true;
}


string_t mx_get_target(string_t *argv) {
    string_t target = NULL;
    for (int i = 1; argv[i] != NULL; i++) {
        if ( !mx_is_flag(argv[i]) ) {
            if (target != NULL) {
                mx_count_arg_err();
                return "ERRR _ Cd ARG";
            }
            else target = argv[i];
        }
        
    }
    return target;
}

bool mx_is_flag(string_t argv) {
    if (mx_streq(argv,  "-s")) return true;
    if (mx_streq(argv,  "-P")) return true;
    if (mx_streq(argv,  "-sP")) return true;
    if (mx_streq(argv,  "-Ps")) return true;
    return false;
}

string_t mx_cd_shortcut(string_t target) {
    if (mx_strlen(target) == 1 && mx_streq(target, "-")) {
        target = getenv("OLDPWD");
        if (target == NULL) {
            mx_oldpwd_err();
        }
    }

    return target;
}

int mx_set_target(string_t target,string_t full_path, t_curflags *curflags, t_shell *shl) {
    char buf[2028];
    string_t path = NULL;
    
    if (curflags->is_P) {
        path = realpath(full_path, buf);
    }
    else {
        path = mx_normalize_path(shl->PWD, full_path, buf);
    }

    if (mx_check_path(target, path) && path != NULL) {
        setenv("OLDPWD", shl->PWD, 1);
        setenv("PWD", path, 1);
        shl->PWD = mx_strdup(path);
        chdir(path);
    }
    else {
        return -1;
    }
    return 0;
}
