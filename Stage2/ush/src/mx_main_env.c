#include "ush.h"

void mx_invalid_option(char option) {
    mx_printerr("env: invalid option -- '");
    write(2, &option, 1);
    mx_printerr("\n");
}

void mx_usage_env(void) {
    mx_printerr("usage: env [-iv] [-P utilpath] [-S string] [-u name]\n");
    mx_printerr("\t[name=value ...] [utility [argument ...]]\n");
}

void mx_option_needs_arg(char opt) {
    mx_printerr("env: option requires an argument -- ");
    write(2, &opt, 1);
    mx_printerr("\n");
}

void mx_no_such_file(string_t file) {
    mx_printerr("env: ");
    mx_printerr(file);
    mx_printerr(": No such file or directory\n");
}

void mx_clearenv(void) {
    int eq_sign = 0;
    string_t key = NULL;
    for (int i = 0; environ[0]; i++) {
        eq_sign = mx_get_char_index(environ[0], '=');
        key = mx_strndup(environ[0], eq_sign);
        unsetenv(key);
        mx_strdel(&key);
    }
}

int mx_get_first_arg_index(int argc, string_t argv[]) {
    int result = 1;
    for (int i = 1; i < argc; ++i) {
        if (argv[i] == NULL) {
            result++;
            continue;
        } 
        if (argv[i][0] != '-') {
            if (mx_get_char_index(argv[i], '=') < 0) {
                return result;
            }
        }
        result++;
    }
    
    return result;
}

int mx_get_first_eq_sign(int argc, string_t argv[]) {
    int first_sign = 1;
    for (int i = 1; i < argc; i++) {
        if (mx_get_char_index(argv[i], '=') >= 0) {
            return first_sign + 1;
        }
        first_sign++;
    }
    return first_sign;
}

int mx_count_size_newenv(int argc, string_t argv[]) {
    int size = 0;
    for (int i = 0; i < argc; i++) {
        if (mx_get_char_index(argv[i], '=') >= 0) {
            size++;
        }
    }
    return size;
}

string_t * mx_parse_newenv(int argc, string_t argv[]) {
    int size_newenv = mx_count_size_newenv(argc, argv);
    if (size_newenv == 0) {
        return NULL;
    }
    int index_newenv = 0;
    string_t *newenv = (string_t *) malloc(sizeof(string_t) * (size_newenv + 1));
    for (int i = 1; i < argc; ++i) {
        if (mx_get_char_index(argv[i], '=') >= 0) {
            newenv[index_newenv] = mx_strdup(argv[i]);
            index_newenv++;
        }
    }
    newenv[size_newenv] = NULL;
    return newenv;
}

void mx_del_arg_env(int argc, string_t argv[]) {
    for (int i = 0; i < argc; i++) {
        if (!mx_strncmp(argv[i], "-u", 2)) {
            if (mx_strlen(argv[i]) > 2) {
                unsetenv((argv[i] + 2));
                argv[i] = NULL;
            }
            else {
                if (i + 1 < argc) {
                    unsetenv(argv[i + 1]);
                    argv[i] = NULL;
                    i++;
                    argv[i] = NULL;
                }
                else {
                    mx_option_needs_arg('u');
                    mx_usage_env();
                }
            }
        }
    }
}

void mx_setarrenv(string_t* arrenv) {  
    if (!arrenv) return;
    string_t key = NULL;
    string_t value = NULL;
    int index_eq_sign = -1;

    for (size_t i = 0; arrenv[i] != NULL; i++) {
        index_eq_sign = mx_get_char_index(arrenv[i], '=');
        key = mx_strndup(arrenv[i], index_eq_sign);
        value = (arrenv[i] + index_eq_sign + 1);
        setenv(key, value, OVERWRITE);
        mx_strdel(&key);
    }
}

void mx_cleararrenv(string_t *arrenv) {
    if (!arrenv) return;
    for (int i = 0; arrenv[i]; i++) {
        if (unsetenv(arrenv[i]) == 0) {
            mx_strdel(&arrenv[i]);
        }
    }
}

void mx_printenv(string_t *env) {
    if (!env) return;
    for (int i = 0; env[i]; i++) {
        if (!mx_is_empty_env(env[i])) {
            mx_printstr(env[i]);
            mx_printchar('\n'); 
        }
    }
}

bool mx_is_empty_env(string_t env) {
    int eq = mx_get_char_index(env, '=');
    if (mx_streq((env + eq + 1), "''")) return true;
    return false;
}

int main_env(string_t argv[], t_shell *shl) {
    int argc = 0;
    while (argv[argc]) argc++;
    int done = 0;
    int first_eq_sign = mx_get_first_eq_sign(argc, argv);

    mx_del_arg_env(first_eq_sign, argv);
    int first_arg = mx_get_first_arg_index(first_eq_sign, argv);
    string_t cur_opt =
        mx_parse_options(first_arg, argv, OPTIONS_ENV, mx_invalid_option);
    string_t *to_replace_env = mx_parse_newenv(argc, argv);

    if (mx_get_char_index(cur_opt, 'i') >= 0) {
#if __APPLE__
        mx_clearenv();
#elif __linux__
        clearenv();
#endif
    }
    mx_setarrenv(to_replace_env);

    if (first_arg == first_eq_sign) {
        mx_printenv(environ);
    } else {
        struct termios term;
        tcgetattr(STDIN_FILENO, &term);
        t_job *env_job = malloc(sizeof(t_job));
        env_job->path = mx_get_util_path(argv[first_arg]);
        env_job->argv = (char**)malloc(sizeof(char*) * argc);
        env_job->t = term;
        for (int i = 0; i < argc - 1; i++) {
            env_job->argv[i] = mx_strdup(argv[i+1]);
            env_job->argv[i+1] = NULL;
        }
        done = mx_job_init(env_job, shl);
        mx_delete_job(&env_job);
    }

    mx_strdel(&cur_opt);
    mx_del_strarr(&to_replace_env);
    return done;
}
