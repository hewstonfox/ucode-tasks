#include "ush.h"

FILE *popen(const char *command, const char *mode);

int pclose(FILE *stream);

string_t mx_tilda(string_t s) {
    string_t pwd = getenv("PWD");
    string_t home = getenv("HOME");
    string_t oldpwd = getenv("OLDPWD");
    if (!oldpwd) oldpwd = home;
    if (pwd == NULL || home == NULL) return s;

    if (mx_get_substr_index(s, "~+") != -1 && s[0] == '~') {
        s = mx_replace_substr(s, "~+", pwd);
    } else if (mx_get_substr_index(s, "~-") != -1 && s[0] == '~') {
        s = mx_replace_substr(s, "~-", oldpwd);
    } 
    else if (mx_strlen(s) > 1 && s[0] == '~' &&  mx_isalpha(s[1])) {
        s = mx_replace_username(s);
    }
    else if (mx_get_substr_index(s, "~ ") != -1 ||
               mx_get_substr_index(s, "~/") != -1 ||
               mx_get_substr_index(s, "~") == mx_strlen(s) - 1) {
        s = mx_replace_substr(s, "~", home);
    }

    return s;
}

string_t mx_replace_username(string_t s) {
    string_t username = mx_strnew(mx_strlen(s));
    int i = 1;
    for (i = 1; i < mx_strlen(s); i++) {
        if (s[i] == '/') break;
        username[i - 1] = s[i];
    }
    if (mx_is_in_dir("/Users/", username)) {
        username = mx_strjoin("/Users/", username);
        if(s[i] == '/') {
            username = mx_strjoin(username, (s + i));
        }
        return username;
    }
    else {
        mx_printerr("ush: no such user or named directory: ");
        mx_printerr(username);
        mx_printerr("\n");
        return NULL;
    }
}

string_t mx_filter_param(string_t param) {
    if (!param) return mx_strnew(0);
    string_t result = mx_strnew(mx_strlen(param));
    int index = 0;
    for (int i = 1; i < mx_strlen(param); i++) {
        if (param[i] == '{' || param[i] == '}') continue;
        result[index] = param[i];
        index++;
    }

    return result;
}
  
string_t *mx_replace_param(string_t *argv) {
    if (!argv) return NULL;

    for (int i = 1; argv[i]; i++) {
        argv[i] = mx_repalce_wildcard(argv[i]);
        argv[i] = mx_tilda(argv[i]);
        argv[i] = mx_replace_once_quote(argv[i]);
        argv[i] = mx_replace_hook(argv[i]);
    }
    return argv;
}

string_t mx_get_string(string_t cmd) {
    size_t max_size = 512;
    string_t buf = mx_strnew(max_size);
    size_t pos = 0;
    FILE *fp = popen(cmd, "r");
    if (fp == NULL) return NULL;

    while ((!feof(fp)) && (pos < (max_size - 1))) {
        size_t n = fread(buf + pos, 1, max_size - (pos + 1), fp);
        if (n <= 0) break;
        pos += n;
    }

    buf[pos++] = '\0';
    pclose(fp);
    return (buf);
}

void mx_replace_spaces(string_t s) {
    for (int i = 0; i < mx_strlen(s) - 1; i++) {
        if (mx_isspace(s[i])) s[i] = ' ';
    }
    if (s[mx_strlen(s) - 1] == '\n') {
        s[mx_strlen(s) - 1] = '\0';
    }
}
