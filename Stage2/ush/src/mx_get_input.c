#include "ush.h"

string_t mx_get_input(t_shell *shl) {
    if (shl->shell_is_idle) mx_printstr(INPUT_STR);
    t_inputctx *ctx = malloc(sizeof(t_inputctx));

    struct winsize tty;
    ioctl(0, TIOCGWINSZ, &tty);

    ctx->startpos_x = mx_strlen(INPUT_STR);
    ctx->pos_x = ctx->startpos_x;
    ctx->len_tty = tty.ws_col;
    ctx->input = (string_t)mx_strnew(ctx->len_tty + 1);
    ctx->c_ch = 0;

    int tmp_x;

    char c;

    while ((c = mx_getch()) != ENTER) {
        if (c == '\033' && shl->shell_is_idle) {
            c = mx_getch();
            if (c == '[') {
                c = mx_getch();

                switch (c)
                {
                case LEFT:
                    if (mx_can_mv_left(ctx)) {
                        mx_printstr("\033[D");
                        ctx->pos_x--;
                    }
                    else mx_printstr("\a");
                    break;
                case RIGTH:
                    if (mx_can_mv_right(ctx)) {
                        mx_printstr("\033[C");
                        ctx->pos_x++;
                    }
                    else mx_printstr("\a");
                default:
                    break;
                }
            }
        }
        else {
            switch (c)
            {
            case BACKSPACE:
                if (mx_can_mv_left(ctx) && shl->shell_is_idle) {
                    tmp_x = ctx->pos_x;
                    int char_to_rm = (ctx->pos_x - ctx->startpos_x - 1);
                    ctx->input = mx_pop_char(ctx, char_to_rm);
                    tmp_x = ctx->pos_x;
                    ctx->c_ch--;
                    
                    mx_set_cur_start(ctx);
                    mx_printstr(ctx->input);
                    mx_printchar(' ');
                    mx_printstr("\033[D");
                    
                    ctx->pos_x = ctx->c_ch + ctx->startpos_x;
                    mx_set_cur_start(ctx);
                    ctx->pos_x = tmp_x;
                    mx_restore_cur(ctx);
                        mx_printstr("\033[D");
                        ctx->pos_x--;
                    
                }
                else mx_printstr("\a");
                break;
            
            default:
                if ((ctx->c_ch + ctx->startpos_x + 1 >= ctx->len_tty)) {
                    // len input > len term
                    continue;
                }
                if (c == 9)  {
                    mx_printstr("\a");
                    continue;
                }
                if (mx_is_last_char(ctx)) {
                    if (shl->shell_is_idle) mx_printchar(c);
                    ctx->input[ctx->c_ch] = c;
                    ctx->c_ch++;
                    ctx->pos_x++;
                }
                else {
                    int ch_to_insert = (ctx->pos_x - ctx->startpos_x);
                    ctx->input = mx_insert_char(ctx, ch_to_insert, c);
                    ctx->c_ch++;
                    mx_set_cur_start(ctx);
                    if (shl->shell_is_idle) mx_printstr(ctx->input);
                    tmp_x = ctx->pos_x;
                    ctx->pos_x = ctx->c_ch + ctx->startpos_x;
                    mx_set_cur_start(ctx);
                    ctx->pos_x = ctx->startpos_x;
                    ctx->pos_x = tmp_x;
                    mx_restore_cur(ctx);
                        mx_printstr("\033[C");
                        ctx->pos_x++;
                }
                break;
            }
        }
    }
    if (shl->shell_is_idle)mx_printchar('\n');
    return ctx->input;
}
