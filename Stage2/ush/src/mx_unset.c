#include "ush.h"

int mx_unset(string_t *input) {
    if (input == NULL)
        return 0;
    for (int j = 0; input[j]; j++) {
        for (int i = 0; i < mx_strlen(input[j]); i++) {
            if (!mx_isdigit(input[j][i]) && !mx_isalpha(input[j][i])) {
                perror("unset");
                return 1;
            }
        }
        unsetenv(input[j]);
    }
    return 0;
}
