#include "cd.h"
#include "ush.h"

int mx_cd_main(string_t *argv, t_shell *shl)
{
    char *options = mx_parse_cd_options(argv);
    if (errno == 48) return 0;
    t_curflags *curflags = malloc(sizeof(t_curflags)); 
    if (options) {
        curflags->is_P = mx_strchr(options, 'P') ? true : false;
        curflags->is_s = mx_strchr(options, 's') ? true : false;    
    }

    mx_strdel(&options);

    string_t target = mx_get_target(argv);
    if (target == NULL) {
        target = getenv("HOME");
    }
    if (mx_streq(target, "ERRR _ Cd ARG")) return 0;
    target = mx_cd_shortcut(target); 
    
    string_t full_path = NULL;
    string_t tmp = NULL;
    int target_res;
    if (target) {
        if (*target != '/') {
            // target != fullpath
            tmp = mx_strjoin(shl->PWD, "/");
            full_path = mx_strjoin(tmp, target);
            mx_strdel(&tmp);
            target_res = mx_set_target(target, full_path, curflags, shl);
            mx_strdel(&full_path);
        }
        else {
            // target == fulpath 
            target_res = mx_set_target(target, target, curflags, shl);
            if (target_res == -1) {
            }
        }
    }
    
    free(curflags);
    return 0;
}
