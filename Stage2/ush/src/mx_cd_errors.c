#include "cd.h"
// TI PIDOR
void mx_usage(void) {
    mx_printerr("cd: usage: cd [-sP] [dir]\n");
}

void mx_not_a_directory(string_t target) {
    mx_printerr("ush: cd: ");
    mx_printerr(target);
    mx_printerr(": Not a directory");
    //cd: psdf/sd: No such file or directory
    mx_printerr("\n");
}

void mx_flag_err(char opt) {
    mx_printerr("ush: cd: -");
    write(2, &opt ,1);
    mx_printerr(": invalid option\n");
}

void mx_count_arg_err(void) {
    mx_printerr("ush: cd: too many arguments\n");
}

void mx_oldpwd_err(void) {
    mx_printerr("bash: cd: OLDPWD not set\n");
}


bool mx_check_path(string_t target, string_t path) {
    if (!path) {
        path = "";
    }
    struct stat path_stat;
    int res = stat(path, &path_stat);
    if (res <= -1) {
        mx_printerr("ush: cd: ");
        mx_printerr(target);
        mx_printerr(": No such file or directory\n");
        return false;
    }
    else if (!S_ISDIR(path_stat.st_mode)){
        mx_printerr("ush: cd: ");
        mx_printerr(target);
        mx_printerr(": Not a directory");
        return false;
    }
    return true;
}
