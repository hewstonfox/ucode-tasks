#include "ush.h"

int main(int argc, string_t *argv) {
    string_t shlvl = NULL;
    if (getenv("SHLVL") == NULL) {
        setenv("SHLVL", "1", 1);
    }
    else {
        shlvl = mx_itoa(atoi(getenv("SHLVL")) + 1);
        setenv("SHLVL", shlvl, OVERWRITE);
        mx_strdel(&shlvl);
    }

    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd))) {
        string_t shell = mx_strjoin(cwd, (argv[0] + 1));
        setenv("SHELL", shell, OVERWRITE);
        mx_strdel(&shell);
    }
    // set shell cwd

    t_shell *shl = malloc(sizeof(t_shell));

    mx_memset(cwd, '\0', 1024);
    if (getcwd(cwd, sizeof(cwd))) {
        shl->PWD = mx_strdup(cwd);
    } else if (getenv("PWD")!= NULL){
        shl->PWD = mx_strdup(getenv("PWD"));
    } else {
        shl->PWD = mx_strdup(getenv("PWD"));
    }

    shl->shell_is_idle = isatty(STDIN_FILENO);
    
    if (shl->shell_is_idle) {
        shl->pid = getpid();
        /* Grab control of the terminal.  */
        tcsetpgrp (STDIN_FILENO, shl->pid);

      /* Save default terminal attributes for shell.  */
        tcgetattr (STDIN_FILENO, &shl->t);  

        signal (SIGINT, SIG_IGN);
        signal (SIGQUIT, SIG_IGN);
        signal (SIGTSTP, SIG_IGN);
        signal (SIGTTIN, SIG_IGN);
        signal (SIGTTOU, SIG_IGN);
        signal (SIGCHLD, SIG_IGN);
    }

    mx_mainloop(argc, argv, shl);
}
