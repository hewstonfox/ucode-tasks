#include "../inc/echo.h"

int mx_parse_flags_echo(string_t input, options_t *options) {
    for (int i = 1; i < mx_strlen(input); i++) {
        if (input[i] == 'n') {
            options->is_n = true;
        }
        else if (input[i] == 'E') {
            options->is_E = true;
            options->is_e = false;
        }
        else if (input[i] == 'e') {
            options->is_E = false;
            options->is_e = true; 
        }
        else {
            options->is_E = true;
            options->is_e = false;
            options->is_n = false;
            write(1, input, mx_strlen(input));
            return 1;
        }
    }

    return 0;
}

void mx_process_str(char *src, char *dst, options_t *opt) {
        //vot tut pizda
    if (opt->is_E) {
        mx_strcpy(dst, src);
        return;
    }
    int i = 0, j = 0, cur = 0, clen;
    char code[5];
    // \c
    for (; i < mx_strlen(src); i++) {
        if (cur == 0) {
            if (src[i] != '\\')
                dst[j++] = src[i];
            else
                cur = 1;
        } else if (cur == 1) {
            switch (src[i]) {
                case 'b':
                    dst[j++] = '\b';
                    break;
                case 't':
                    dst[j++] = '\t';
                    break;
                case 'v':
                    dst[j++] = '\v';
                    break;
                case 'n':
                    dst[j++] = '\n';
                    break;
                case 'a':
                    dst[j++] = '\a';
                    break;
                // case 'c':
                //     dst[j++] = '\c';
                //     break;
                case 'r':
                    dst[j++] = '\r';
                    break;
                case '\\':
                    dst[j++] = '\\';
                    break;
                case '0':
                    cur = 2;
                    clen = 0;
                    code[clen++] = '0';
                    break;
                default:
                    dst[j++] = '\\';
                    dst[j++] = src[i];
                    break;
            }
            if (cur == 1) cur = 0;
        } else if (cur == 2) {
            if (src[i] >= '0' && src[i] <= '9')
                code[clen++] = src[i];
            else {
                code[clen] = 0;
                dst[j++] = (char)mx_atoi(code);
                if (src[i] == '\\')
                    cur = 1;
                else {
                    dst[j++] = src[i];
                    cur = 0;
                }
            }
        }
    }
    if (cur == 2) {
        code[clen] = 0;
        dst[j++] = (char)mx_atoi(code);
    }
    dst[j] = 0;
}

int main_echo(char **argv) {
    int argc = 0;
    while (argv[argc]) argc++;
    
    options_t *options = malloc(sizeof(options_t *));
    options->is_E = true;
    options->is_e = false;
    options->is_n = false;
    char res[256];
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] == '-' && i != argc) {
            if(mx_parse_flags_echo(argv[i], options) == 1 
                && i != argc - 1 && !mx_is_empty(argv[i])) {
                mx_printchar(' ');
            }
        } else {
            mx_process_str(argv[i], res, options);
            write(1, &res, mx_strlen(res));
            if (i + 1 < argc && !mx_is_empty(argv[i])) mx_printchar(' ');
        }
    }
    if (!options->is_n) mx_printchar('\n');
    free(options);
    return 0;
}
