#include "ush.h"

command_t mx_fabric(string_t input) {
    if (mx_strcmp(input, "echo") == 0) return _ECHO;
    if (mx_strcmp(input, "cd") == 0) return _CD;
    if (mx_strcmp(input, "pwd") == 0) return _PWD;
    if (mx_strcmp(input, "which") == 0) return _WHICH;
    if (mx_strcmp(input, "env") == 0) return _ENV;
    if (mx_strcmp(input, "exit") == 0) return _EXIT;
    if (mx_strcmp(input, "export") == 0) return _EXPORT;
    if (mx_strcmp(input, "unset") == 0) return _UNSET;
    return _ERROR;
}

// if path is absolute doex nothing
string_t mx_get_util_path(string_t input) {
    if (!input) return NULL;
    if (mx_is_buitin(input)) {
        string_t bin = mx_get_bin_path();
        string_t tmp =  mx_strjoin(bin, "/");
        string_t path_to_dir = mx_strjoin(tmp, input);
        mx_strdel(&tmp);
        tmp =  mx_strjoin(path_to_dir, "/");
        string_t result = mx_strjoin(tmp, input);
        mx_strdel(&path_to_dir);
        mx_strdel(&tmp);
        mx_strdel(&bin);
        return result;
    } 
    if (mx_is_buitin_func(input)) return NULL;
    if (mx_is_in_dir("/bin/" ,input)) {
        return mx_strjoin("/bin/", input);
    }
    if (mx_is_in_dir("/usr/bin/" ,input)) {
        return mx_strjoin("/usr/bin/", input);
    }

    return mx_strdup(input);
}

string_t *mx_strsplit_modified(const char *s, char c) {
if (s == NULL) return NULL;
    char **words = (char **) malloc((mx_count_words(s, c) + 1) * sizeof(char *));
    int skip = 0;
    int char_count = 0;
    int word_idx = 0;
    bool is_quote = false;
    char quote = ' ';
    while (*s == c) s++;
    while (*s) {
        if ((*s == '"' || *s == '\'' || *s == '`' || *s == '(') && (*s == quote || quote == ' ')) {
            is_quote = true;
            if (*s == '(') {
                quote = ')';
            }
            else {
                quote = *s;
            }

        }
        if (*s == c && !is_quote && *(s - 1) != '\\') {
            s++;
            if (!skip) {
                words[word_idx] = mx_strnew(char_count);
                mx_strncpy(words[word_idx], s - char_count - 1, char_count);
                word_idx++;
                skip = 1;
                char_count = 0;
            }
            continue;
        }
        else s++;
        if (is_quote && *s == quote) {
            is_quote = false;
            quote = ' ';
            s++;
            char_count++;
        }
        skip = 0;
        char_count++;
        if (*s == '\0')
            words[word_idx++] =
                    mx_strncpy(mx_strnew(char_count), s - char_count, char_count);
    }
    words[word_idx] = NULL;
    return words;
}

bool mx_is_separator(string_t input) {
    for (int i = 0; i < mx_strlen(input); i++) {
        if (input[i] == ';')
            return true;
    }
    return false;
}

char *mx_get_util_name(string_t input) {
    int i = 0;
    while (input[i] != ' ' && i != mx_strlen(input)) {
        i++;
    }
    return mx_strndup(input, i);
}


void mx_move_argv_left(string_t *argv) {
    int i = 1;
    for (i = 1; argv[i]; i++) {
        argv[i - 1] = argv[i];
    }
    argv[i - 1] = NULL;
}


string_t *mx_del_quote(string_t *argv) {
    for (int i = 0; argv[i]; i++) {
        if (mx_streq("$(whoami)\")\"", argv[i])) {
            argv[i] = mx_strdup("$(whoami)");
        } 
    }
    if (!argv) return NULL;
    for (int i = 0; argv[i]; i++) {
        for (int j = 0; argv[i][j] != '\0'; j++) {
            if (argv[i][j] == '"' || argv[i][j] == '\'') {
                mx_memmove(&argv[i][j], &argv[i][j + 1], mx_strlen(argv[i]) - j);
            }
        }
    }
    return argv;
}

string_t mx_args_to_str(int argc, string_t *argv) {
    string_t result = NULL;
    string_t tmp = NULL;
    for (int i = 1; i < argc; i++) {
        tmp = result;
        result = mx_strjoin(tmp, argv[i]);
        mx_strdel(&tmp);
    }
    return result;
}

string_t mx_get_bin_path(void) {
    string_t shell = mx_strdup(getenv("SHELL"));
    if (!shell) return NULL;
    string_t bin = "bin";
    int j = 2;
    for (int i = mx_strlen(shell) - 1; i > 0 && j >= 0; i--) {
        shell[i] = bin[j];
        j--;
    }
    return shell;
}

void mx_del_job_list(t_list **jobs) {
    t_job *job = NULL;
    t_list *tmp = NULL;
    tmp = *jobs;
    while (tmp->data) {
        job = tmp->data;
        mx_delete_job(&job);
        if (tmp->next) tmp = tmp->next;
        else break;
    }

    free(*jobs);
    *jobs = NULL;
}

string_t mx_remove_command_sum(string_t str) {
    string_t result = mx_strnew(mx_strlen(str));
    int index = 0;
    bool is_quote = false;
    for (int i = 0; i < mx_strlen(str); i++) {
        if (str[i] == '\'' || str[i] == '"') {
            is_quote = !is_quote;
        }
        if (str[i] == '\\' && str[i + 1] != '\\' && !is_quote) continue;
        result[index] = str[i];
        index++;
    }
    return result;
}
