#include "ush.h"
// Change error handler

static bool mx_is_one_line_input(string_t input) {
    int qm_count = 0;
    for (int i = 0; i < mx_strlen(input); i++) {
        if (input[i] == '"'
        || input[i] == '\'')
            qm_count++;
    }
    return !(qm_count & 1);
}

int mx_mainloop(int argc, string_t *argv, t_shell *shl) {
    string_t input;
    int exit_status = 0;
    bool is_onetime = argc > 1 ? true : false;
    bool is_insert = (isatty(STDIN_FILENO) != 1) ? true : false;
    bool is_inf = true;
    if (is_onetime == true && is_insert == true) is_inf = false;
    else if (is_onetime == true && is_insert == false) is_inf = false;
    else if (is_onetime == false && is_insert == true) is_inf = false;

    bool first_iter = true;
    //int done;
    
    t_list *jobs = NULL;
    t_job *job = NULL;
    while ((is_inf) || first_iter) {
        if (shl->shell_is_idle) {
            signal(SIGTSTP, SIG_IGN);
            signal(SIGINT, SIG_IGN);
        }
        if (!is_onetime)input = mx_get_input(shl);
        else input = mx_args_to_str(argc, argv);
        input = mx_strtrim(input);
        if (mx_is_one_line_input(input) == false) {
            mx_printerr("Odd number of quotes.\n");
            continue;
        } 
        if (mx_is_empty(input) == true) continue;
        jobs = mx_update_job_list(jobs, input);
        // do job from job list
        while (mx_check_jobs(jobs) != -1) {
            job = jobs->data;
            job->argv = mx_replace_param(job->argv);
            for (int i = 1; job->argv[i]; i++) {
                if(mx_strcmp("$?", job->argv[i]) == 0) {
                    job->argv[i] = mx_itoa(exit_status);
                }
            }
            exit_status = mx_job_init(job, shl);
            // here if done != 0 add to sus
            mx_delete_job(&job);
            if (jobs->next) {
                jobs = jobs->next;
            }  
            else break;
        } 
        jobs->data = NULL;
        jobs = NULL;
        if (input != NULL)
            mx_strdel(&input);
        first_iter = false;
    }

    return 0;
}
