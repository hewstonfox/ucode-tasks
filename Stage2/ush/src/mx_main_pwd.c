#include "ush.h"

static void invalid_option_handler(char opt) {
    char str_opt[2] = {opt, '\0'};
    string_t err = mx_replace_substr(INVALID_OPTION_ERR, ARG_RPL, str_opt);
    mx_printerr(err);
    mx_strdel(&err);
    err = mx_replace_substr(USAGE, ARG_RPL, OPTIONS_PWD);
    mx_printerr(err);
    mx_strdel(&err);
    exit(2);
}

int main_pwd(string_t *argv, t_shell *shell) {
    int argc = 0;
    while (argv[argc]) argc++;
    char *opts = mx_parse_options(argc, argv, OPTIONS_PWD, &invalid_option_handler);
    int p = 0;
    for (int i = 0; opts[i]; i++)
        switch (opts[i]) {
            case 'L':
                p = 0;
                break;
            case 'P':
                p = 1;
        }
    free(opts);

    mx_printstr(p ? getcwd(NULL, 2024) : shell->PWD);
    mx_printchar('\n');

    return 0;
}
