#include "ush.h"

t_list *mx_update_job_list(t_list *jobs, string_t input) {
    string_t *input_commands = mx_strsplit(input, ';');
    string_t *splited_command = NULL;
    string_t command_tmp = NULL;
    string_t path_tmp = NULL;
    t_job *job = NULL;

    struct termios term;
    tcgetattr(STDIN_FILENO, &term);

    for (int i = 0; input_commands[i]; i++) {
        command_tmp = mx_strtrim(input_commands[i]);
        // sigseg
        input_commands[i] = command_tmp;
        splited_command = mx_strsplit_modified(input_commands[i], ' ');
        // splited_command = mx_replace_param(splited_command);
        for (int j = 0; splited_command[j]; j++) {
            splited_command[j] = mx_remove_command_sum(splited_command[j]);
        }
        splited_command = mx_del_quote(splited_command);

        job = malloc(sizeof(t_job));
        path_tmp = mx_strdup(splited_command[0]);
        job->path = mx_get_util_path(path_tmp);
        job->argv = splited_command;
        job->t = term;
        mx_push_back(&jobs, job);
    }
    mx_del_strarr(&input_commands);
    return jobs;
}

int mx_check_jobs(t_list *jobs) {
    if (!jobs) return -1;
    if (jobs->data) return 0;
    return -1;
}

bool mx_is_variable(string_t arg) {
    for (int i = 0; i < mx_strlen(arg); i++) {
        if (arg[i] == '=' && i != 0) {
            return true;
        }
    }
    return false;
}
int mx_job_init(t_job *job, t_shell *shl) {
    int done;
    if (mx_is_buitin_func(job->argv[0])) {
        done = mx_init_buitin_job(job, shl);
    } else if (mx_is_variable(job->argv[0])) {
        int size = 0;
        while (job->argv[size]) size++;
        char **tmp = (char**)malloc(sizeof(char) * (size + 1));
        tmp[0] = mx_strdup("export");
        for (int i = 0, j = 1; i < size; i++, j++) {
            tmp[j] = mx_strdup(job->argv[i]);
            printf("%s\n", tmp[j]);
        }
        tmp[size + 2] = NULL;
        done = mx_export(tmp);
    } else {
        done = mx_init_exe_job(job, shl);
    }
    return done;
}
/**/
int mx_init_buitin_job(t_job *job, t_shell *shl) {
    command_t command = mx_fabric(job->argv[0]);
    shl->shell_is_idle = shl->shell_is_idle;
    switch (command) {
        case _CD:
            return mx_cd_main(job->argv, shl);
        case _EXIT:
            if (job->argv[1] == NULL) {
                exit(0);
            }
            exit(mx_atoi(job->argv[1]));
        case _PWD:
            return main_pwd(job->argv, shl);
        case _ENV:
            return main_env(job->argv, shl);
        case _EXPORT:
            return mx_export(job->argv);
        case _UNSET:
            return mx_unset(job->argv);
        case _ECHO:
            return main_echo(job->argv);
        case _WHICH:
            return main_which(job->argv);
        default:
            return -1;
    }

    return -1;
}

int mx_init_exe_job(t_job *job, t_shell *shl) {
    bool foreground = true;
    /*
    case -1 == case of error
    case 0 == child case
    case default = parent case
    */
    int status = 0;
    job->pid = fork();
    switch (job->pid) {
        case -1:
            mx_printstr(strerror(errno));
            mx_printstr("error in fork (fork == -1)\n");
            break;
        case 0:
            mx_launch_exe_proces(job, shl);
            break;
        default:
            if (shl->shell_is_idle) {
                setpgid(job->pid, job->pid);
            }
            break;
    }

    if (!shl->shell_is_idle) {
        mx_wait_for_job(job);
    } else if (foreground) {
        mx_move_job_to_fg(job, 0, shl);
    } else {
        mx_move_job_to_bg(job, 0);
    }

    if (job->is_sus) {
        mx_do_job_notif(job);
    }

    return status;
}

void mx_delete_job(t_job **job) {
    if ((*job)->path) mx_strdel(&(*job)->path);
    if ((*job)->argv) mx_del_strarr(&(*job)->argv);
    free(*job);
    *job = NULL;
}

bool mx_is_buitin_func(string_t name) {
    string_t builtin[] = {"cd",  "exit", "export", "unset", "pwd",
                          "env", "echo", "which",  NULL};
    for (int i = 0; builtin[i]; i++) {
        if (mx_streq(name, builtin[i])) return true;
    }
    return false;
}

bool mx_is_buitin(string_t name) {
    string_t builtin[] = {"echo",  "env", "export", "which",
                          "unset", "pwd", NULL};
    for (int i = 0; builtin[i]; i++) {
        if (mx_streq(name, builtin[i])) return true;
    }
    return false;
}

bool mx_is_in_dir(string_t path_file, string_t name) {
    string_t path = mx_strjoin(path_file, name);
    if (!access(path, F_OK)) {
        mx_strdel(&path);
        return true;
    }
    mx_strdel(&path);
    return false;
}

void mx_launch_exe_proces(t_job *job, t_shell *shl) {
    if (shl->shell_is_idle) {
        signal(SIGINT, SIG_DFL);
        signal(SIGTSTP, SIG_DFL);
    }
    execve(job->path, job->argv, environ);
    mx_printerr("ush: command not found: ");
    mx_printerr(job->path);
    mx_printerr("\n");
    exit(1);
}

int mx_proc_stat(pid_t pid, int status, t_job *job) {
    if (pid > 0) {
        if (WIFSTOPPED(status)) {
            job->is_sus = true;
            mx_printstr("\nStopped 1+ \n");
            signal(SIGCHLD, SIG_DFL);
        } else {
            job->done = true;
            if (WIFSIGNALED(status)) mx_printstr("\nTerminated\n");
            WTERMSIG(status);
        }
        return 0;
    } else {
        return -1;
    }
}

void mx_move_job_to_fg(t_job *job, int cont, t_shell *shl) {
    tcsetpgrp(STDIN_FILENO, job->pid);

    if (cont) {
        tcsetattr(STDIN_FILENO, TCSADRAIN, &job->t);
        if (kill(job->pid, SIGCONT) < 0) perror("kill (SIGCONT)");
    }

    mx_wait_for_job(job);

    tcsetpgrp(STDIN_FILENO, shl->pid);

    tcgetattr(STDIN_FILENO, &job->t);
    tcsetattr(STDIN_FILENO, TCSADRAIN, &shl->t);
}

void mx_wait_for_job(t_job *job) {
    int status = 0;
    pid_t pid;
    waitpid(job->pid, &status, WUNTRACED);
    while (!mx_proc_stat(job->pid, status, job) && !job->is_sus && !job->done) {
        pid = waitpid(job->pid, &status, WUNTRACED);
    }
}

void mx_move_job_to_bg(t_job *job, int cont) {
    if (cont) {
        if (kill(job->pid, SIGCONT) < 0) perror("kill (SIGCONT)");
    }
}

void mx_update_status(t_job *job) {
    int status;
    pid_t pid;
    pid = waitpid(-1, &status, WUNTRACED | WNOHANG);
    while (!mx_proc_stat(pid, status, job)) {
        pid = waitpid(-1, &status, WUNTRACED | WNOHANG);
    }
}

void mx_do_job_notif(t_job *job) { mx_update_status(job); }
