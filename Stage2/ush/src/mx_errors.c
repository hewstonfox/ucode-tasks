#include "ush.h"

void mx_err_no_such_file(string_t name) {
    mx_printerr("ush: no such file or directory: ");
    mx_printerr(name);
    mx_printerr("\n");
}

void mx_err_cmnd_not_found(string_t name) {
    mx_printerr("ush: command not found: ");
    mx_printerr(name);
    mx_printerr("\n");
}
