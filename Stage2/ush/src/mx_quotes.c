#include "ush.h"

// replace with \` arg \` (once quote)
string_t mx_replace_once_quote(string_t s) {
    if (s == NULL) return NULL;

    bool is_open = false;
    string_t cmd = NULL;
    string_t to_rep = NULL;
    string_t result = mx_strnew(mx_strlen(s) * 5);
    int index = 0;
    int cmd_index = 0;
    int i = 0;
    for (i = 0; i < mx_strlen(s); i++) {
        if (s[i] == '`' && s[i - 1] != '\\') {
            is_open = true;
            i++;
            cmd_index = 0;
            cmd = mx_strnew(mx_strlen(s) - i);
            while (s[i] != '`' && s[i - 1] != '\\' && i < mx_strlen(s)) {
                cmd[cmd_index] = s[i];
                cmd_index++;
                i++;
            }
            if (i < mx_strlen(s)) is_open = false;
            to_rep = mx_get_string(cmd);
            if (to_rep != NULL) {
                mx_replace_spaces(to_rep);
                mx_strcat(result, to_rep);
                index += mx_strlen(to_rep); 
            }
        }
        else {
            result[index] = s[i];
            index++;
        }
    }
    
    if (i >= mx_strlen(s) && is_open) {
        mx_printerr("not enough \n");
    }
    return result;
}

string_t mx_replace_hook(string_t s) {
    if (s == NULL) return NULL;
    bool is_open = false;
    string_t cmd = NULL;
    string_t to_rep = NULL;
    string_t result = mx_strnew(mx_strlen(s) * 5);
    int index = 0;
    int cmd_index = 0;
    int i = 0;
    for (i = 0; i < mx_strlen(s); i++) {

        if (s[i] == '(' && i > 0 && (s[i -1] != '$' || s[i-1] != '\\')) {
            mx_printerr("ush: syntax error near unexpected token\n");
            return NULL;
        }
        if (s[i] == '$' && s[i + 1] == '(') {
            i++;
            i++;
            cmd_index = 0;
            cmd = mx_strnew(mx_strlen(s) - i);
            is_open = true;
            while (s[i] != ')' && i < mx_strlen(s)) {
                cmd[cmd_index] = s[i];
                cmd_index++;
                i++;
            }
            if (i < mx_strlen(s)) is_open = false;
            to_rep = mx_get_string(cmd);
            if (to_rep != NULL) {
                mx_replace_spaces(to_rep);
                mx_strcat(result, to_rep);
                index += mx_strlen(to_rep); 
            }
        }
        else {
            result[index] = s[i];
            index++;
        }
    }

    /* if (i >= mx_strlen(s) && is_open) {
        mx_printerr("not enough ) \n");
    } */
    return result;
}

string_t mx_repalce_wildcard(string_t s) {
    if (s == NULL) return NULL;
    int index = 0;
    bool once_q = false;
    string_t torep;
    string_t insert;
    string_t result = mx_strdup(s);
    for (int i = 0; i < mx_strlen(s); i++) {
        if (s[i] == '\'') once_q = !once_q;
        
        if (s[i] == '$' && mx_strlen(s) > i+1 && s[i + 1] != '"' && s[i + 1] != '?') {
            if  ( (i == 0 && mx_strlen(s) > i +1  && s[i + 1] != '(' && !once_q)
                || (i > 0 && mx_strlen(s) > i+1 && s[i + 1] != '(' && s[i-1] != '\\' && !once_q)
            ) {
                index = 0;
                torep = mx_strnew(mx_strlen(s));
                torep[index] = s[i];
                index++;
                i++;
                while (s[i] != '\0' && s[i] != ' ' && s[i] != '\'' && s[i] != '"' && s[i] != '$') {
                    torep[index] = s[i];
                    index++;
                    i++;
                }
                insert = mx_get_wildcard(torep + 1);
                result = mx_replace_substr(result, torep, insert);
            }
            
        }
    }
    return result;
}

string_t mx_get_wildcard(string_t to_rep) {
    if (to_rep && to_rep[0] == '{') {
        to_rep = mx_del_wildcard_quote(to_rep);
    }
    string_t result = getenv(to_rep);
    if(result == NULL) return "\0";
    return mx_strdup(result);
}

string_t mx_del_wildcard_quote(string_t to_rep) {
    if (!to_rep) return NULL;
    string_t result = mx_strnew(mx_strlen(to_rep));
    int res_index = 0;
    for (int i = 0; i < mx_strlen(to_rep); i++) {
        if (to_rep[i] == '{' || to_rep[i] == '}') continue;
        result[res_index] = to_rep[i];
        res_index++;
    }
    return result;
}
