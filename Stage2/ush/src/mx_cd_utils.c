#include "ush.h"

bool mx_is_empty(char *input) {
    if (!input) 
        return true;
    if (mx_strcmp(input, "") == 0)
        return true; 
    for (int i = 0; i < mx_strlen(input); i++) {
        if (input[i] != ' ') {
            return false;
        }
    }
    return true;
}

bool mx_can_mv_left(t_inputctx *ctx) {
    if (ctx->pos_x - ctx->startpos_x > 0) return true;
    return false;
}

bool mx_can_mv_right(t_inputctx *ctx) {
    if (ctx->c_ch > ctx->pos_x - ctx->startpos_x ) return true;
    return false;
}

bool mx_is_last_char(t_inputctx *ctx) {
    if (ctx->c_ch == ctx->pos_x - ctx->startpos_x ) return true;
    return false;
}

void mx_set_cur_start(t_inputctx *ctx) {
    for (int i = ctx->pos_x; i > ctx->startpos_x ; i--) {
        mx_printstr("\033[D");
    }
}

void mx_restore_cur(t_inputctx *ctx) {
    for (int i = ctx->startpos_x; i < ctx->pos_x; i++) {
        mx_printstr("\033[C");
    }
}

int mx_getch() {
    int ch;
    struct termios oldt, newt;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON|ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

    return ch;
}

string_t mx_pop_char(t_inputctx *ctx, int pop_index) {
    string_t newstr = mx_strnew(mx_strlen(ctx->input));
    int index = 0;
    for (int i = 0; i < mx_strlen(ctx->input); i++) {
        if (i == pop_index) continue;
        newstr[index] = ctx->input[i];
        index++;
    }
    mx_memset(ctx->input, '\0', ctx->len_tty);
    mx_strcpy(ctx->input, newstr);
    mx_strdel(&newstr);

    return ctx->input;
}

string_t mx_insert_char(t_inputctx *ctx, int ch_index, char ch) {
    string_t newstr = mx_strnew(ctx->len_tty);
    int index = 0;
    for (int i = 0; i < mx_strlen(ctx->input); i++) {
        if (i == ch_index) {
            newstr[index] = ch;
            index++;
            newstr[index] = ctx->input[i];
            index++;
        }
        else {
            newstr[index] = ctx->input[i];
            index++;
        }
    }
    mx_memset(ctx->input, '\0', ctx->len_tty);
    mx_strcpy(ctx->input, newstr);
    mx_strdel(&newstr);
    return ctx->input;
}
