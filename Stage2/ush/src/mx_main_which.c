#include "../inc/which.h"
#include <sys/stat.h>
#define MX_PATH() (mx_getenv("PATH"))
#define MX_WHICH_US "which: illegal option -- \
%c\nusage: which [-as] program ...\n"

typedef struct curent_flags {
    bool is_a;
    bool is_s;
}   cur_flags_t;


int is_built(char *command) {
    if (strcmp("cd", command) == 0)
        return 1;
    if (strcmp("pwd", command) == 0)
        return 2;
    if (strcmp("echo", command) == 0)
        return 9;
    return 0;
}

bool mx_is_builtin(string_t command) {
    int size = 0;
    string_t builtins[] = { "alias", "bg", "bind", "break", 
                            "builtin", "case", "cd", "command", 
                            "compgen", "complete", "continue", 
                            "declare", "dirs", "disown", "echo",
                            "enable", "eval", "exec", "exit", 
                            "export",  "fc", "fg", "getopts",
                            "hash", "help", "history", "if", 
                            "jobs", "kill", "let", "local",
                            "logout", "popd", "printf", "pushd",
                            "pwd", "read", "readonly", "return",
                            "set", "shift", "shopt", "source",
                            "suspend", "test", "times", "trap",  
                            "type", "typeset", "ulimit", "umask",
                            "unalias", "unset", "until", "wait", 
                            "while", NULL};
    while (builtins[size]) size++;
    for (int i = 0; i < size; i++) {
        if (mx_strcmp(command, builtins[i]) == 0) 
            return true;
    }
    return false;
}


int mx_file_exist(char *path) {
    // *
    struct stat *st = (struct stat*) malloc(sizeof(struct stat));
    int flag = 0;

    if ((stat(path, st) != -1))
        flag = 1;
    if (st != NULL) {
        free(st);
        st = NULL;
    }
    return flag;
}


int mx_count_arr_el(char **arr) {
    int counter = 0;
    
    if (arr != NULL){
        while (arr[counter])
            counter++;
        return counter;
    }
    return 0;
}


 char *check_in_path(char *command, char **path) {
    char *command_p = NULL;

    for (int i = 0; i < mx_count_arr_el(path); i++) {
        command_p = mx_strnew(mx_strlen(command) + mx_strlen(path[i]) + 1);
        mx_strcpy(command_p, path[i]);
        command_p = mx_strcat(command_p, "/");
        command_p = mx_strcat(command_p, command);
        if (mx_file_exist(command_p))
            break;
        mx_strdel(&command_p);
    }
    return command_p;
}

char *mx_coomand_in_path(char *comm, char *str_path) {
    char **path = NULL;
    char *command_p = NULL;
    int paths = 0;
    int i = 0;

    if (str_path != NULL) {
        path = mx_strsplit(str_path, ':');
        paths = mx_count_arr_el(path);
        if (strstr(str_path, "PATH=") != NULL) {
            for (; i < mx_strlen(path[0]) - 5; i++)
                path[0][i] = path[0][i + 5];
            path[0][i] = '\0';
        }
        if (mx_strcmp(comm, "/") != 0 && mx_get_char_index(comm, '/') == -1)
            command_p = check_in_path(comm, path);
        //mx_free_void_arr((void **) path, paths);
    }
    if (command_p == NULL)
        command_p = mx_strdup(comm);
    return command_p;
}

char *homedir() {
    struct passwd *pw = getpwuid(getuid());

    if (getenv("HOME") == NULL)
        return mx_strdup(pw->pw_dir);

    return mx_strdup(getenv("HOME"));
}

char *mx_getenv(char *var) {
    if (strcmp(var, "PWD") == 0)
        return getcwd(NULL, 0);
    if (strcmp(var, "HOME") == 0)
        return homedir();
    if (strcmp(var, "SHLVL") == 0) {
        if (getenv("SHLVL") == NULL)
            return mx_strdup("1");
        return mx_strdup(getenv("SHLVL"));
    }
    if (strcmp(var, "OLDPWD") == 0) {
        if (getenv("OLDPWD") == NULL)
            return NULL;
        return getenv("OLDPWD");
    }
    if (strcmp(var, "PATH") == 0) {
        if (getenv("PATH") == NULL)
            return "/bin:/usr/bin:/usr/ucb:/usr/local/bin";
        return getenv("PATH");
    }
    return NULL;
}

int is_comm_rw(char *str) {
    char *reserved_words[] = {"!", "{", "}", "time", "export",
                            "case", "coproc", "do", "done", 
                            "elif", "else", "esac", "fi",
                            "for", "function", "if", "in",
                            "select", "then", "until", "while", 
                            "[", "]", NULL};
    int size = 0;
    while (reserved_words[size])size++;
    for (int i = 0; i < size; i++) {
        if (strcmp(str, reserved_words[i]) == 0)
            return 1;
    }
    return -1;
}

bool check_builtin(string_t input) {
    char *tmp = NULL;
    int a = 0;
    //tmp = mx_coomand_in_path(input, MX_PATH());
    if ((a = mx_is_builtin(input))) {
        mx_printstr(input);
        mx_printstr(": shell built-in command\n");
    }
    mx_strdel(&tmp);
    return a;
}

int check_command(string_t input, cur_flags_t *flags) {
    //char **path = NULL;
    if (is_comm_rw(input) == 1) {
        mx_printerr(input);
        mx_printerr(": shell reserved word\n");
        return 0;
    }
    if(!check_builtin(input) || flags->is_a) {
        char *tmp = NULL;
        if (mx_is_in_dir("/bin/", input)) {
            tmp = mx_strjoin("/bin/", input);
        }
        else if (mx_is_in_dir("/usr/bin/" ,input)) {
            tmp = mx_strjoin("/usr/bin/", input);
        } else {
            mx_printerr(input);
            mx_printerr(" not found\n");
            return 1;
        }
        if (tmp != NULL) {
            mx_printstr(tmp);
            if (flags->is_s == true) {
                char buff[2028];
                char *res = realpath(tmp, buff);
                if (res) {
                    mx_printstr(" -> ");
                    mx_printstr(res);
                    mx_printchar('\n');
                }
            }
            else {
                mx_printchar('\n');
            }
        }
    }
    return 0;
}

 int mx_parse_flags(string_t input, cur_flags_t *flags) {
    for (int i = 1; i < mx_strlen(input); i++) {
        if (input[i] == 's')
            flags->is_s = true;
        else if (input[i] == 'a')
            flags->is_a = true;
        else if (input[i] == '-' && mx_strlen(input) > 3) {
            continue;
        }
        else {
            fprintf(stderr, "which: bad option -- ");
            fprintf(stderr, "%c\n", input[i]);
            return -1;
        }
    }
    return 0;
}


int main_which(char *argv[]) {
    int argc = 0;
    while (argv[argc]) argc++;
    int status = 0;
    cur_flags_t *flags = malloc(sizeof(cur_flags_t*));
    flags->is_a = false;
    flags->is_s = false;

    for (int i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            if (mx_parse_flags(argv[i], flags) < 0) {
                free(flags);
                return 1;
            }
        } else {
            status = check_command(argv[i], flags);
        }
    }
    free(flags);
    return status;    
    // int status = 0;
    // int tmp = 0;
    // for (int i = 1; i < argc; i++) {
    //     if (strcmp(argv[i], "--") == 0) status = 3;
    //     else if (argv[i][0] == '-' && status < 3) {
    //        if ((tmp = check_flag(argv[i], &status)) == 0)
    //             continue;
    //         } 
    //     check_command(argv[i], &tmp , status);
    // }
}


