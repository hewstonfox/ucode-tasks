#include "../inc/ush.h"

void mx_process_hard_str(string_t input) {
    if (input == NULL) {
        return;
    }
    for (int i = 0; i < mx_strlen(input); i++) {
        if (input[i] == '=') {
            string_t variable = mx_strndup(input, i);
            string_t value = mx_strndup((input + i + 1), mx_strlen(input));
            setenv(variable, value, OVERWRITE);
            return;
        }
    }
}

int mx_export(string_t *input) {
    int argc = 0;
    while (input[argc] != NULL) argc++;
    
    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            int status = 0;
            string_t *tmp = mx_strsplit(input[i], '=');
            if (!tmp) 
                continue;
            while (tmp[status]) status++;
            switch (status) {
            case 1:
                setenv(tmp[0], "''", OVERWRITE);
                mx_del_strarr(&tmp);
                tmp = NULL;
                break;
            case 2:
                setenv(tmp[0], tmp[1], OVERWRITE);
                mx_del_strarr(&tmp);
                tmp = NULL;
                break;
            default:
                mx_process_hard_str(input[i]);
                mx_del_strarr(&tmp);
                tmp = NULL;
                break;
            }
        }
        return 0;
    }
 
    int env_count = 0;
    while (environ[env_count]) env_count++;
    char **environ_dup = mx_dup_string_arr((const char **)environ, env_count);
    mx_bubble_sort(environ_dup, env_count);
    env_count = 0;
    while (environ_dup[env_count] != NULL) {
        printf("%s\n", environ_dup[env_count]);
        env_count++;
    }
    mx_del_str_arr(environ_dup, env_count);
    return 0;
}
