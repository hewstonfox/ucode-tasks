const Anonymous = class {
  constructor(name, alias, affiliation) {
    this.name = name
    this.alias = alias
    this.affiliation = affiliation
  }
}

module.exports = {
  getAnonymous: (...args) => new Anonymous(...args),
}
