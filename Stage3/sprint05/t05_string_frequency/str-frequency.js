module.exports = class StrFrequency {
  constructor(str) {
    this._str = str
  }

  reverseString() {
    return this._str.split('').reverse().join('')
  }

  letterFrequencies() {
    return [...new Set(this._str.toUpperCase())]
      .filter(v => v.match(/[A-Z]/))
      .reduce(
        (res, val) => ({
          ...res,
          [val]: (this._str.match(new RegExp(val, 'gi')) ?? []).length,
        }),
        {}
      )
  }

  wordFrequencies() {
    return [...new Set(this._str.toUpperCase().split(/\s+/))]
      .map(word => [...word].filter(c => c.match(/[A-Z]/)).join(''))
      .filter(Boolean)
      .reduce(
        (res, val) => ({
          ...res,
          [val]: (
            this._str.match(new RegExp(`\\b${val}\\b`, 'gi')) ?? []
          ).length,
        }),
        {}
      )
  }
}
