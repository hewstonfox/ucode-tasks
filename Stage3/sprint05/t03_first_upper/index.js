const firstUpper = str => {
  if (!str) return ''
  str = str.trim()
  if (!str) return ''
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
}

module.exports = {
  firstUpper,
}
