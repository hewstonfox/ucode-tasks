const checkDivision = (start = 1, stop = 60) => {
  for (let i = start; i <= stop; i++) {
    const computed = [];
    if (!(i % 2)) computed.push('is divisible by 2')
    if (!(i % 3)) computed.push('is divisible by 3')
    if (!(i % 10)) computed.push('is divisible by 10')
    console.log(`The number ${i} ${computed.join(', ') || '-'}`);
  }
};

module.exports = {
  checkDivision,
};
