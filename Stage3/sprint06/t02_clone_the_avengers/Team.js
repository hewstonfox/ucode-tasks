exports.Team = class Team {
  constructor(id, avangers) {
    this.id = id
    this.avangers = [...avangers]
  }

  battle({ damage }) {
    const newAvangers = [...this.avangers]
    for (const avanger of this.avangers) {
      avanger.aHp -= damage
      if (avanger.aHp <= 0)
        newAvangers.splice(
          newAvangers.findIndex(v => v === avanger),
          1
        )
    }
    this.avangers = newAvangers
  }

  clone() {
    return new Team(this.id, this.avangers)
  }

  calculateLosses(clonedTeam) {
    const lost = clonedTeam.avangers.filter(
      v => this.avangers.indexOf(v) < 0
    ).length
    console.log(
      lost
        ? `In this battlewe lost ${lost} Avengers.`
        : "We haven't lost anyone in this battle!"
    )
  }
}
