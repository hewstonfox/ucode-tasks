const { LLData } = require('./LLData')

exports.LList = class LList {
  #head = null;

  [Symbol.iterator]() {
    let cursor = this.#head
    return {
      next: () => {
        const res = { value: cursor?.data, done: !cursor }
        cursor = cursor?.next ?? null
        return res
      },
    }
  }

  getIterator() {
    return this[Symbol.iterator]()
  }

  getFirst() {
    return this.#head
  }

  getLast() {
    let cur = this.#head
    while (cur?.next) cur = cur.next
    return cur
  }

  add(value) {
    const data = new LLData(value)
    if (this.#head) this.getLast().next = data
    else this.#head = data
  }

  addFromArray(arr) {
    if (!Array.isArray(arr))
      throw new Error('LList.addFromArray: input data should be array')

    for (const elem of arr) this.add(elem)
  }

  remove(value) {
    if (!this.#head) return
    if (this.#head.data === value) {
      this.#head = this.#head.next
      return
    }

    let cur = this.#head
    while (cur.next.data !== value) cur = cur.next
    cur.next = cur.next.next
  }

  removeAll(value) {
    while (this.#head && this.#head.data === value) this.#head = this.#head.next
    if (!this.#head) return

    let cur = this.#head
    while (cur.next) {
      if (cur.next.data === value) cur.next = cur.next.next
      cur = cur.next
    }
  }

  contains(value) {
    if (!this.#head) return

    let cur = this.#head
    while (cur)
      if (cur.data === value) return true
      else cur = cur.next
    return false
  }

  clear() {
    this.#head = null
  }

  count() {
    let count
    let cur = this.#head
    while (cur) {
      count++
      cur = cur.next
    }
    return count
  }

  toString() {
    const values = []
    for (const value of this) values.push(value)
    return values.join(',')
  }

  filter(callback) {
    const filtered = []
    for (const value of this) if (callback(value)) filtered.push(value)
    const list = new LList()
    list.addFromArray(filtered)
    return list
  }
}
