exports.Product = class Product {
  constructor(name, kcal_per_portion) {
    this.name = name
    this.kcal_per_portion = kcal_per_portion
  }

  get isJunk() {
    return this.kcal_per_portion > 200
  }
}
