const { EatException } = require('./eat-exception')

exports.Ingestion = class Ingestion {
  constructor(meal_type, id) {
    this.meal_type = meal_type
    this.id = id
    this.products = []
  }

  getFromFridge(product) {
    if (this.products.find(p => product === p.name).isJunk) throw new EatException()
  }

  setProduct(product) {
    this.products.push(product)
  }

  getProductInfo(product) {
    return {
      kcal: this.products.find(p => product === p.name)?.kcal_per_portion,
    }
  }
}
