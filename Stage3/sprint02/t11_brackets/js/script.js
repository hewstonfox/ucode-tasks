const checkBrackets = str => {
  if (
    !str ||
    typeof str !== 'string' ||
    (str.indexOf('(') === -1 && str.indexOf(')') === -1)
  )
    return -1
  let toAdd = 0
  while (str.indexOf('(') !== -1) {
    if (
      str.indexOf(')') !== -1 &&
      str.indexOf('(') < str.indexOf(')', str.indexOf('('))
    ) {
      str = str.replace('(', '')
      str = str.replace(')', '')
    } else {
      str = str.replace('(', '')
      toAdd += 1
    }
  }
  while (str.indexOf(')') !== -1) {
    str = str.replace(')', '')
    toAdd += 1
  }
  return toAdd
}
