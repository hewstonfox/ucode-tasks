const concat = (...args) => {
  const second = () => {
    const str = prompt('Enter string: ', '')
    if (str === null) return args[0]
    second.count++
    return args[0].concat(' ', str)
  }
  second.count = 0

  if (args.length == 1) return second
  if (args.length == 2) {
    return args[0].concat(' ', args[1])
  }
}
