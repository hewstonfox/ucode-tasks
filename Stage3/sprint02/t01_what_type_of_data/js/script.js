let vNumber = 123
let vBigInt = 123n
let vString = 'A string'
let vBoolean = true
let vNull = null
let vUndefined = undefined
let vObject = {}
let vSymbol = Symbol()
let vFunction = () => {}

alert(`vNumber is ${typeof vNumber}
vBigInt is ${typeof vBigInt}
vString is ${typeof vString}
vBoolean is ${typeof vBoolean} 
vNull is ${typeof vNull}
vObject is ${typeof vObject}
vSymbol is ${typeof vSymbol}
vFunction is ${typeof vFunction}`)
