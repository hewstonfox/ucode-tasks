const copyObj = obj => {
  const copy = {}
  for (v in obj) copy[v] = typeof obj[v] === 'object' ? copyObj(obj[v]) : obj[v]
  return copy
}
