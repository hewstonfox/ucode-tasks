const isValidAnimal = text =>
  text && text.match(/^[A-Za-z]+$/) && text.length <= 20

const isValidAge = text => text && text.match(/^[0-9]+$/) && text.length <= 5

;(() => {
  const animal = prompt('What animal is the superhero most similar to?')
  if (!isValidAnimal(animal)) {
    alert('hero animal: len <= 20 and only letters and shouldn§t be empty')
    return
  }

  const gender = prompt(
    'Is the superhero male or female? Leave blank if unknown or other.'
  ).toLowerCase()
  if (!['male', 'female', ''].includes(gender)) {
    alert('hero gender: male/female/blank')
    return
  }

  const age = prompt('How old is the superhero?')
  if (!isValidAge(age)) {
    alert('hero age: only digits and len < 5')
    return
  }

  let description
  switch (gender) {
    case 'male':
      description = age < 18 ? 'boy' : 'man'
      break

    case 'female':
      description = age < 18 ? 'girl' : 'woman'
      break

    default:
      description = age < 18 ? 'kid' : 'hero'
      break
  }

  alert(`${animal}-${description}`)
})()
