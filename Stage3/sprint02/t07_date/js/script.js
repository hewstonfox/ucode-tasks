const toFixed2 = num =>
  num.toLocaleString('en-US', {
    minimumIntegerDigits: 2,
    useGrouping: false,
  })

function getFormattedDate(incomingDate) {
  let formatDate = {
    day: incomingDate.getDate(),
    month: incomingDate.getMonth() + 1,
    year: incomingDate.getFullYear(),
    hours: incomingDate.getHours(),
    minutes: incomingDate.getMinutes(),
    weekday: incomingDate.toLocaleString('en-US', { weekday: 'long' }),
  }

  return `${toFixed2(formatDate.day)}.${toFixed2(formatDate.month)}.${
    formatDate.year
  } ${toFixed2(formatDate.hours)}:${toFixed2(formatDate.minutes)} ${
    formatDate.weekday
  }`
}
