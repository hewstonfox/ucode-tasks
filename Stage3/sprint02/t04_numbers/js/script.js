const start = +prompt('insert num1', 1)
const end = +prompt('insert num2', 100)

const checkDivision = (start = 1, stop = 100) => {
  if (!start && start !== 0) start = 1
  if (!stop && stop !== 0) start = 100
  for (let i = start; i <= stop; i++) {
    const computed = []
    if (!(i % 2)) computed.push('is even')
    if (!(i % 3)) computed.push('a multiply of 3')
    if (!(i % 10)) computed.push('a multiply of 10')
    console.log(`${i} ${computed.join(', ') || '-'}`)
  }
}

checkDivision(start, end)
