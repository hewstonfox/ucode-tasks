const total = (addCount, addPrice, currentTotal) =>
  (currentTotal || 0) + (+addCount * +addPrice || 0)
