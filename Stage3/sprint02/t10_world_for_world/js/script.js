const trimWords = words => words.replace(/\s+/g, ' ').trim()
const clearDuplicate = wordsArr => [...new Set(wordsArr)]

const prepareWords = wordsStr => [...new Set(wordsStr.split(/\s+/))]

const addWords = (obj, word) =>
  (obj['words'] = prepareWords(obj['words'] + ' ' + word))

const removeWords = (obj, word) => {
  let clearWords = clearDuplicate(trimWords(obj['words']).split(' '))
  word = trimWords(word).split(' ')

  for (i in word)
    if (clearWords.indexOf(word[i]) != -1)
      clearWords.splice(clearWords.indexOf(word[i]), 1)

  obj['words'] = clearWords.join(' ')
}

const changeWords = (obj, toDelete, toReplace) => {
  removeWords(obj, toDelete)
  addWords(obj, toReplace)
}
