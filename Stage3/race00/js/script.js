function calc(x) {
    var a = document.getElementById('text').value;
    if (a == 'Ошибка') {
        a = ''
    }
    if (a == "0" && x != '.') {
        a = ''
    }
    if ((a == '') && (x == '*' || x == '/' || x == '+' || x == '-')) {
        x = ''
    }
    if ((a.slice(-1) == '*' || a.slice(-1) == '/' || a.slice(-1) == '+' || a.slice(-1) == '-') && (x == '*' || x == '/' || x == '+' || x == '-')) {
        a = a.slice(0, -1);
    }
    a += x;
    document.getElementById('text').value = a;
}

function res() {
    var a = document.getElementById('text').value;
    a = eval(a);
    document.getElementById('text').value = a;
}

function reset() {
    document.getElementById('text').value = '';
}

function back() {
    var a = document.getElementById('text').value;
    a = a.slice(0, -1);
    document.getElementById('text').value = a;
}

function coren() {
    var a = document.getElementById('text').value;
    a = eval(a);
    if (a < 0) {
        document.getElementById('text').value = 'Ошибка';
    } else if (a != '') {
        a = Math.sqrt(a);
        document.getElementById('text').value = a;
    }
}

function kv() {
    var a = document.getElementById('text').value;
    if (a != '') {
        a = eval(a);
        a = a * a;
        document.getElementById('text').value = a;
    }
}

function del() {
    var a = document.getElementById('text').value;
    if (a != '') {
        a = eval(a);
        a = 1 / a;
        document.getElementById('text').value = a;
    }
} 