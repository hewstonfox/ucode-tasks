USE ucode_web;
SELECT
  heroes.id,
  heroes.name,
  SUM(hp.power_points) AS points
FROM
  heroes
  JOIN heroes_powers as hp ON hp.hero_id = heroes.id
GROUP BY
  heroes.id
ORDER BY
  points DESC
LIMIT
  1;
SELECT
  h.name,
  p.type,
  SUM(hp.power_points) AS points
FROM
  heroes as h
  JOIN heroes_powers as hp on hp.hero_id = h.id
  JOIN powers as p on hp.power_id = p.id
WHERE
  p.type = "defense"
GROUP BY
  h.id
ORDER BY
  points
LIMIT
  1;
SELECT
  ht.id,
  ht.name,
  SUM(hp.power_points) as points
FROM
  (
    SELECT
      h.id,
      h.name,
      COUNT(ht.team_id) as t_count
    FROM
      heroes AS h
      LEFT OUTER JOIN heroes_teams AS ht ON ht.hero_id = h.id
    GROUP BY
      h.id
  ) as ht
  LEFT OUTER JOIN heroes_powers as hp on ht.id = hp.hero_id
WHERE
  ht.t_count < 2
GROUP BY
  ht.id
ORDER BY
  points DESC;
SELECT
  t.name,
  SUM(hp.points) as power
FROM
  (
    SELECT
      h.id,
      h.name,
      SUM(hp.power_points) as points
    FROM
      heroes as h
      LEFT OUTER JOIN heroes_powers as hp on h.id = hp.hero_id
    GROUP BY
      h.id
  ) as hp
  JOIN heroes_teams as ht ON hp.id = ht.hero_id
  JOIN teams as t ON ht.team_id = t.id
WHERE
  t.name in ("Hydra", "Avengers")
GROUP BY
  ht.team_id
ORDER BY
  power;