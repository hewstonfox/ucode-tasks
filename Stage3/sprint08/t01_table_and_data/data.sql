USE ucode_web;
INSERT INTO
  heroes (name, description, class_role)
VALUES
  (
    'Captain America',
    'When creating America''s first-ever Super Soldier, you have to consider the character of the man first and foremost. Courage, honor, and honesty are paramount, and that''s why undersized, emaciated bully-basher Steve Rogers was the right choice to become the world''s pioneering (and, as it would turn out, only) perfect Serum specimen.',
    'dps'
  ),
  (
    'Iron Man',
    'There is no Avengers without Iron Man, and there is no Iron Man without a traumatized and terrorized Tony Stark having a change of heart and transforming from an arrogant womanizing weapons dealer into a (still arrogant) armored hero who helps Nick Fury spearhead, and mold, the Avengers. ',
    'dps'
  ),
  (
    'Thor',
    'Once the arrogant heir to all of Asgard, and then an exiled brat in need of absolution, Thor watched as his fallen adopted brother Loki waged war on a planet the God of Thunder had come to love',
    'dps'
  ),
  (
    'Spider-Man',
    'Gifted high-school student Peter Parker had barely ever stepped outside of Queens before being recruited by Tony Stark to help stop Captain America and his allies from protecting wanted fugitive Bucky B',
    'tankman'
  ),
  (
    'Loki',
    'Loki Laufeyson was the biological son of Laufey, the ruler of the Frost Giants in Jotunheim, who was abandoned and left to die shortly after his birth.',
    'healer'
  ),
  (
    'Winter Soldier',
    'Sergeant James Buchanan Bucky Barnes is a World War II veteran, a former officer of the 107th Infantry Regiment and best friend of Steve Rogers since childhood.',
    'dps'
  ),
  (
    'Falcon',
    'Samuel Thomas Sam Wilson, better known as Falcon, is a former United States Air Force pararescue airman, who left active duty when his wing-man Riley died in combat, instead choosing to help other veterans suffering from post-traumatic stress disorder.',
    'dps'
  ),
  (
    'Scarlet Witch',
    'Wanda Maximoff, also known as the Scarlet Witch, is a native of Sokovia who grew up with her fraternal twin brother, Pietro.Wanda Maximoff, also known as the Scarlet Witch, is a native of Sokovia who grew up with her fraternal twin brother, Pietro.',
    'healer'
  ),
  (
    'Baron Zemo',
    'Baron Helmut Zemo is a former Colonel with the Sokovian Armed Forces and a commander of EKO Scorpion.',
    'tankman'
  ),
  (
    'Sharon Carter',
    'Sharon Carter is the great-niece of legendary S.H.I.E.L.D. founder and Director Peggy Carter. Following in her aunts footsteps, Carter became an agent of S.H.I.E.L.D., although she never revealed her relation.',
    'healer'
  );