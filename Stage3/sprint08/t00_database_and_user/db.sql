DROP DATABASE IF EXISTS ucode_web;
CREATE DATABASE ucode_web;
DROP USER 'vyushko' @'localhost';
CREATE USER 'vyushko' @'localhost' IDENTIFIED WITH mysql_native_password BY 'securepass';
GRANT ALL ON ucode_web.* TO 'vyushko' @'localhost';