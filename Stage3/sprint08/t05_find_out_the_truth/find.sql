USE ucode_web;
SELECT
  ht.id,
  ht.name,
  ht.teams_count,
  ht.class_role,
  r.name
FROM
  (
    SELECT
      h.id,
      h.name,
      h.class_role,
      h.race_id,
      COUNT(ht.team_id) as teams_count
    FROM
      heroes AS h
      LEFT OUTER JOIN heroes_teams AS ht ON ht.hero_id = h.id
    GROUP BY
      h.id
  ) as ht
  LEFT OUTER JOIN races as r ON ht.race_id = r.id
WHERE
  ht.teams_count >= 2
  AND r.name <> "human"
  AND ht.name LIKE BINARY "%a%"
  AND ht.class_role IN ("tankman", "healer")
ORDER BY
  ht.id
LIMIT
  1