USE ucode_web;
SELECT
  heroes.name,
  teams.name
FROM
  heroes
  LEFT JOIN heroes_teams ON (heroes.id = heroes_teams.hero_id)
  LEFT JOIN teams ON (heroes_teams.team_id = teams.id);
SELECT
  h.name,
  powers.name
FROM
  (
    SELECT
      heroes.name,
      heroes.class_role,
      heroes_powers.power_id
    FROM
      heroes
      LEFT JOIN heroes_powers ON heroes.id = heroes_powers.hero_id
  ) h
  RIGHT JOIN powers ON h.power_id = powers.id;
SELECT
  ht.hero_name,
  ht.team_name,
  p.name as power_name
FROM
  heroes_powers as hp
  JOIN powers as p ON p.id = hp.power_id
  JOIN (
    SELECT
      h.name as hero_name,
      ht.hero_id,
      t.name as team_name
    FROM
      heroes_teams as ht
      JOIN teams as t ON t.id = ht.team_id
      JOIN heroes as h ON ht.hero_id = h.id
  ) as ht ON hp.hero_id = ht.hero_id;