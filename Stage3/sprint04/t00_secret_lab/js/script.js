const transformation = () => {
  if (hero.textContent === 'Bruce Banner') {
    hero.textContent = 'HULK'
    hero.style.fontSize = '130px'
    lab.style.backgroundColor = '#70964b'
    hero.style.letterSpacing = '6px'
  } else {
    hero.textContent = 'Bruce Banner'
    hero.style.fontSize = '60px'
    lab.style.backgroundColor = '#ffb300'
    hero.style.letterSpacing = '2px'
  }
}
