const path = require('path')

const COOKIE_KEY_USER = 'ucode_sprint09_vyusho__user'
const PORT = 3000
const PUBLIC_DIR = path.join(__dirname, 'public')
const VIEW_DIR = path.join(__dirname, 'views')
const FILE_TYPES = {
  'html': 'text/html',
  'htm': 'text/html',
  'shtml': 'text/html',
  'css': 'text/css',
  'xml': 'text/xml',
  'gif': 'image/gif',
  'jpeg': 'image/jpeg',
  'jpg': 'image/jpeg',
  'js': 'application/x-javascript',
  'txt': 'text/plain',
  'png': 'image/png',
  'tif': 'image/tiff',
  'tiff': 'image/tiff',
  'ico': 'image/x-icon',
  'bmp': 'image/x-ms-bmp',
  'svg': 'image/svg+xml',
  'webp': 'image/webp',
  'pdf': 'application/pdf',
}

module.exports = {
  COOKIE_KEY_USER,
  PORT,
  PUBLIC_DIR,
  VIEW_DIR,
  FILE_TYPES,
}
