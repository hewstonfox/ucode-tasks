const fs = require('fs')

const { FILE_TYPES } = require('./constants')

const readFile = async filename => new Promise((res, rej) => fs.readFile(filename, 'utf8', (err, data) => err ? rej(err) : res(data)))
const interpolate = (str, data) => Object.entries(data).reduce((acc, [k, v]) => acc.replaceAll(`{{${k}}}`, v), str)
const interpolateFile = async (filename, data) => interpolate(await readFile(filename), data)
const getFileTypeByExt = ext => FILE_TYPES[ext.replace(/^\./, '')]

module.exports = {
  readFile,
  interpolate,
  interpolateFile,
  getFileTypeByExt,
}
