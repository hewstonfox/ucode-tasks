const Koa = require('koa')
const Router = require('koa-router')
const BodyParser = require('koa-bodyparser')

const path = require('path')
const url = require('url')

const User = require('./models/user')
const { COOKIE_KEY_USER, VIEW_DIR, PUBLIC_DIR, PORT } = require('./constants')
const { interpolateFile, getFileTypeByExt, readFile } = require('./utils')
const { sendMail } = require('./mail')


const app = new Koa()
const router = new Router()

const obj2query = obj => Object.entries(obj).reduce((acc, [k, v]) => `${acc}&${k}=${v}`, '')

const authorize = async ctx => {
  const id = +ctx.cookies.get(COOKIE_KEY_USER)
  return id >= 0 ? User.find(id) : null
}

router.get('/', async ctx => {
  const user = await authorize(ctx)
  if (!user) {
    ctx.redirect('/login')
    return
  }
  ctx.body = await interpolateFile(path.join(VIEW_DIR, 'main.html'), { status: user.status })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.get('/registration', async ctx => {
  if (await authorize(ctx)) {
    ctx.redirect('/')
    return
  }
  const preloadData = ctx.query
  ctx.body = await interpolateFile(path.join(VIEW_DIR, 'register.html'), {
    login: preloadData.login ?? '',
    fullName: preloadData.fullName ?? '',
    email: preloadData.email ?? '',
    error: preloadData.error ?? '',
  })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.post('/register', async ctx => {
  const obj = ctx.request.body
  try {
    const user = User.fromObject(obj)
    user.rawPassword = ctx.request.body.password
    await user.save()
    ctx.cookies.set(COOKIE_KEY_USER, user.id)
    ctx.redirect('/')
  } catch (e) {
    const error = e.message.startsWith('Duplicate entry') ? 'User already registered' : e.message
    ctx.redirect(`/registration?${obj2query({ login: obj.login, fullName: obj.fullName, email: obj.email, error })}`)
  }
})

router.get('/login', async ctx => {
  if (await authorize(ctx)) {
    ctx.redirect('/')
    return
  }
  const preloadData = ctx.query
  ctx.body = await interpolateFile(path.join(VIEW_DIR, 'login.html'), {
    login: preloadData.login ?? '',
    error: preloadData.error ?? '',
  })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.post('/login', async ctx => {
  const obj = ctx.request.body
  try {
    const user = await User.authorize(obj.login, obj.password)
    ctx.cookies.set(COOKIE_KEY_USER, user.id)
    ctx.redirect('/')
  } catch (e) {
    ctx.redirect(`/login?${obj2query({ login: obj.login, error: e.message })}`)
  }
})

router.get('/reset-password', async ctx => {
  if (await authorize(ctx)) {
    ctx.redirect('/')
    return
  }
  ctx.body = await interpolateFile(path.join(VIEW_DIR, 'resetPassword.html'), { message: '' })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.post('/reset-password', async ctx => {
  const user = await User.findByLogin(ctx.request.body.login)
  if (!user)
    ctx.body = await interpolateFile(path.join(VIEW_DIR, 'resetPassword.html'), {
      message: 'User doesn\'t exist',
      class: 'danger',
    })
  else {
    sendMail(user.email, user.rawPassword ? `Your password was ${user.rawPassword}` : 'No password found. Sorry')
      .catch(console.log)
    ctx.body = await interpolateFile(path.join(VIEW_DIR, 'resetPassword.html'), {
      message: 'Message with password sent to your email',
      class: 'success',
    })
  }
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.all('/logout', async ctx => {
  ctx.cookies.set(COOKIE_KEY_USER)
  ctx.redirect('/')
})

app
  .use(BodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .use(async (ctx, next) => {
    await next()
    if (ctx.status !== 404) return
    try {
      const reqUrl = new url.URL(ctx.url, `${ctx.req.protocol}://${ctx.req.headers.host}`)
      const pathname = reqUrl.pathname === '/' ? '/index.html' : reqUrl.pathname
      ctx.body = await readFile(path.join(PUBLIC_DIR, pathname))
      ctx.set('Content-Type', getFileTypeByExt(path.extname(pathname)))
    } catch (err) {
      ctx.body = await readFile(path.join(VIEW_DIR, '404.html'))
      ctx.set('Content-Type', getFileTypeByExt(path.extname('html')))
    }
  })
  .listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}/`)
  })
