const nodemailer = require('nodemailer')

let testUser = null

const regenerateTestUser = async () =>
  testUser = await nodemailer.createTestAccount()

const getTransporter = async () => {
  if (!testUser)
    await regenerateTestUser()
  return nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: testUser.user,
      pass: testUser.pass,
    },
  })
}

const sendMail = async (to, text) => {
  const info = await (await getTransporter()).sendMail({
    from: '"vyushko 👻" <vyushko@example.com>',
    to,
    subject: 'Service message',
    text,
    html: text,
  })
  console.log('Mail sent\nPreview URL: %s', nodemailer.getTestMessageUrl(info))
  return info
}

module.exports = {
  sendMail
}
