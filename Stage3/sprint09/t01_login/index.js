const fs = require('fs')
const Koa = require('koa')
const Router = require('koa-router')
const BodyParser = require('koa-bodyparser')

const path = require('path')
const url = require('url')

const User = require('./models/user')

const COOKIE_KEY_USER = 'ucode_sprint09_vyusho__user'
const PORT = 3000
const PUBLIC_DIR = path.join(__dirname, 'public')
const FILE_TYPES = {
  'html': 'text/html',
  'htm': 'text/html',
  'shtml': 'text/html',
  'css': 'text/css',
  'xml': 'text/xml',
  'gif': 'image/gif',
  'jpeg': 'image/jpeg',
  'jpg': 'image/jpeg',
  'js': 'application/x-javascript',
  'txt': 'text/plain',
  'png': 'image/png',
  'tif': 'image/tiff',
  'tiff': 'image/tiff',
  'ico': 'image/x-icon',
  'bmp': 'image/x-ms-bmp',
  'svg': 'image/svg+xml',
  'webp': 'image/webp',
  'pdf': 'application/pdf',
}


const readFile = async filename => new Promise((res, rej) => fs.readFile(filename, 'utf8', (err, data) => err ? rej(err) : res(data)))
const interpolate = (str, data) => Object.entries(data).reduce((acc, [k, v]) => acc.replaceAll(`{{${k}}}`, v), str)
const interpolateFile = async (filename, data) => interpolate(await readFile(filename), data)
const getFileTypeByExt = ext => FILE_TYPES[ext.replace(/^\./, '')]

const app = new Koa()
const router = new Router()

const obj2query = obj => Object.entries(obj).reduce((acc, [k, v]) => `${acc}&${k}=${v}`, '')

const authorize = async ctx => {
  const id = +ctx.cookies.get(COOKIE_KEY_USER)
  return id >= 0 ?  User.find(id) : null
}

router.get('/', async ctx => {
  const user = await authorize(ctx)
  if (!user) {
    ctx.redirect('/login')
    return
  }
  ctx.body = await interpolateFile(path.join(PUBLIC_DIR, 'main.html'), { status: user.status })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.get('/registration', async ctx => {
  if (await authorize(ctx)) {
    ctx.redirect('/')
    return
  }
  const preloadData = ctx.query
  ctx.body = await interpolateFile(path.join(PUBLIC_DIR, 'register.html'), {
    login: preloadData.login ?? '',
    fullName: preloadData.fullName ?? '',
    email: preloadData.email ?? '',
    error: preloadData.error ?? '',
  })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.post('/register', async ctx => {
  const obj = ctx.request.body
  try {
    const user = User.fromObject(obj)
    await user.save()
    ctx.cookies.set(COOKIE_KEY_USER, user.id)
    ctx.redirect('/')
  } catch (e) {
    const error = e.message.startsWith('Duplicate entry') ? 'User already registered' : e.message
    ctx.redirect(`/registration?${obj2query({ login: obj.login, fullName: obj.fullName, email: obj.email, error })}`)
  }
})

router.get('/login', async ctx => {
  if (await authorize(ctx)) {
    ctx.redirect('/')
    return
  }
  const preloadData = ctx.query
  ctx.body = await interpolateFile(path.join(PUBLIC_DIR, 'login.html'), {
    login: preloadData.login ?? '',
    error: preloadData.error ?? '',
  })
  ctx.set('Content-Type', getFileTypeByExt('html'))
})

router.post('/login', async ctx => {
  const obj = ctx.request.body
  try {
    const user = await User.authorize(obj.login, obj.password)
    ctx.cookies.set(COOKIE_KEY_USER, user.id)
    ctx.redirect('/')
  } catch (e) {
    ctx.redirect(`/login?${obj2query({ login: obj.login, error: e.message })}`)
  }
})

router.all('/logout', async ctx => {
  ctx.cookies.set(COOKIE_KEY_USER)
  ctx.redirect('/')
})

app
  .use(BodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .use(async (ctx, next) => {
    await next()
    if (ctx.status !== 404) return
    try {
      const reqUrl = new url.URL(ctx.url, `${ctx.req.protocol}://${ctx.req.headers.host}`)
      const pathname = reqUrl.pathname === '/' ? '/index.html' : reqUrl.pathname
      ctx.body = await readFile(path.join(PUBLIC_DIR, pathname))
      ctx.set('Content-Type', getFileTypeByExt(path.extname(pathname)))
    } catch (err) {
      ctx.body = await readFile(path.join(PUBLIC_DIR, '404.html'))
      ctx.set('Content-Type', getFileTypeByExt(path.extname('html')))
    }
  })
  .listen(3000, () => {
    console.log(`Server running at http://localhost:${PORT}/`)
  })
