DROP DATABASE IF EXISTS ucode_sprint09_vyushko;

CREATE DATABASE ucode_sprint09_vyushko;

USE ucode_sprint09_vyushko;

DROP USER IF EXISTS 'vyushko' @'localhost';
CREATE USER 'vyushko' @'localhost' IDENTIFIED BY 'securepass';
GRANT ALL ON ucode_sprint09_vyushko.* TO 'vyushko' @'localhost';

DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    login       VARCHAR(30)  NOT NULL UNIQUE,
    hash        VARCHAR(130) NOT NULL,
    fullName    VARCHAR(255),
    email       VARCHAR(60)  NOT NULL UNIQUE,
    status      ENUM ('admin', 'user') DEFAULT 'user',
    rawPassword VARCHAR(255) default NULL
)
