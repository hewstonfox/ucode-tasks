const crypto = require('crypto')
const Model = require('../model')

const SALT = ')S=kKw_TQuTJ-3KME11OVzu9n:JLjmi/uY)a+tH)5z1b!9Cu-YYtEdg/7QM)mQN-'

module.exports = class User extends Model {
  static tableName = 'users'

  constructor(id = null, login, hash, fullName, email) {
    super(id)
    this.login = login
    this.hash = hash
    this.fullName = fullName
    this.email = email
  }

  static #makeHash(passwd) {
    return crypto.pbkdf2Sync(passwd, new TextEncoder().encode(SALT), 1000, 64, 'sha512').toString('hex')
  }

  static fromObject(obj) {
    this.validateObj(obj)
    return new this(obj.id ?? null, obj.login, obj.hash ?? this.#makeHash(obj.password), obj.fullName, obj.email)
  }

  static validateObj(obj) {
    super.validateObj(obj)
    const { login, password, confirmPassword, fullName, email, hash, id } = obj
    if (typeof login !== 'string') throw new Error('Login should be a string')
    if (login.length === 0) throw new Error('Login can`t be empty')
    if (login.length > 30) throw new Error('Login can`t exceed 30 characters')
    if (typeof fullName !== 'string') throw new Error('Full name should be a string')
    if (fullName.length === 0) throw new Error('Full name can`t be empty')
    if (fullName.length > 255) throw new Error('Full name can`t exceed 255 characters')
    if (typeof email !== 'string') throw new Error('Email should be a string')
    if (email.length === 0) throw new Error('Email can`t be empty')
    if (email.length > 60) throw new Error('Email can`t exceed 60 characters')
    if (!email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) throw new Error('Email is not valid')
    if (password !== confirmPassword) throw new Error('Passwords not match')
    if (!hash && !password) throw new Error('Password required')
    if (hash && typeof hash !== 'string') throw new Error('Hash should be a string')
    if (id && (typeof id !== 'number' || id < 0)) throw new Error('Id should be a positive number')
  }

}

