"use strict";

const validator = {
  set(target, PropertyKey, receiver) {
    if (PropertyKey === "age") {
      if (!Number.isInteger(receiver)) {
        throw new TypeError("The age is not an integer");
      }
      if (receiver < 0 || receiver > 200) {
        throw new RangeError("The age is invalid");
      }
    }
    target[PropertyKey] = receiver;
    return true;
  },
  get: function (target, PropertyKey) {
    if (PropertyKey in target) {
      console.log(`Trying to access the property \'${PropertyKey}\'...`);
      return target[PropertyKey];
    } else {
      throw new Error(`Property ${PropertyKey} do not exist...`);
    }
  },
};
