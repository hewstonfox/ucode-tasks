"use strict";

const rotLetter = (chr, rot) => {
  if (!chr.match(/[a-z]/i)) return chr;
  const chrCode = chr.charCodeAt(0);
  const isCap = chrCode < 97;
  let newIdx = (chrCode - (isCap ? 65 : 97) + rot) % 26;
  newIdx < 0 && (newIdx = 26 + newIdx);
  return String.fromCharCode(newIdx + (isCap ? 65 : 97));
};

const houseMixin = {
  wordReplace(word, replacement) {
    this.description = this.description.replaceAll(word, replacement);
  },
  wordInsertAfter(word, insertion) {
    this.description = this.description.replaceAll(
      word,
      `${word} ${insertion}`
    );
  },
  wordDelete(word) {
    this.description = this.description.replaceAll(word, "");
  },

  wordEncrypt() {
    this.description = [...this.description].reduce(
      (acc, chr) => `${acc}${rotLetter(chr, 13)}`,
      ""
    );
  },
  wordDecrypt() {
    this.description = [...this.description].reduce(
      (acc, chr) => `${acc}${rotLetter(chr, -13)}`,
      ""
    );
  },
};
