function* sumPrompt() {
  let res = 1;
  while (1) {
    const input = prompt(`Previous result: ${res}. Enter a new number:`);
    if (input === null) return;
    if (!input || isNaN(input)) {
      console.error("Invalid number!");
      continue;
    }
    res += +input;
    yield res;
    if (res > 10_000) res = 1;
  }
}

for (const res of sumPrompt()) console.log(res);
