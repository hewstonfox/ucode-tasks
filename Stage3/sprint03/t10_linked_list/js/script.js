class Node {
  constructor(value) {
    this.data = value;
    this.next = null;
  }
}

class LinkedList {
  #head = null;

  [Symbol.iterator] = function* () {
    if (!this.#head) return;
    let cursor = this.#head;
    yield cursor.data;
    while ((cursor = cursor.next)) yield cursor.data;
  };

  getIterator() {
    return this[Symbol.iterator]();
  }

  getLast() {
    let cur = this.#head;
    while (cur?.next) cur = cur.next;
    return cur;
  }

  add(value) {
    const data = new Node(value);
    if (this.#head) this.getLast().next = data;
    else this.#head = data;
  }

  addFromArray(arr) {
    if (!Array.isArray(arr))
      throw new Error("LinkedList.addFromArray: input data should be array");
    for (const elem of arr) this.add(elem);
  }

  remove(value) {
    while (this.#head && this.#head.data === value)
      this.#head = this.#head.next;
    if (!this.#head) return;

    let cur = this.#head;
    while (cur?.next) {
      if (cur.next.data === value) cur.next = cur.next.next;
      cur = cur.next;
    }
  }

  contains(value) {
    if (!this.#head) return;

    let cur = this.#head;
    while (cur)
      if (cur.data === value) return true;
      else cur = cur.next;
    return false;
  }

  clear() {
    this.#head = null;
  }

  count() {
    let count;
    let cur = this.#head;
    while (cur) {
      count++;
      cur = cur.next;
    }
    return count;
  }

  log() {
    console.log([...this].join(", "));
  }
}

function createLinkedList(arr) {
  const ll = new LinkedList();
  ll.addFromArray(arr);
  return ll;
}
