const inputController = new Proxy(
  {},
  {
    get(target, p) {
      if (target[p]) return target[p];
      target[p] = document.getElementById(p).innerText;
      return target[p];
    },
    set(target, p, value) {
      document.getElementById(p).innerText = value;
      target[p] = value;
      return true;
    },
  }
);

class Human {
  constructor(firstName, lastName, gender, age) {
    this.calories = 0;
    this.status = "Idle";

    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.age = age;
    this.blocked = false;
  }

  feed() {
    if (this.calories >= 500 || this.blocked) return;
    inputController.status = "Nom nom nom";
    this.blocked = true;
    setTimeout(() => {
      this.calories += 200;
      if (this.calories > 500) {
        this.calories = 500;
        inputController.status = "I'm not hungry.";
      }
      if (this.calories < 500) inputController.status = "I'm still hungry";
      inputController.calories = this.calories;
      this.blocked = false;
    }, 1000);
  }

  sleepFor(sec) {
    inputController.status = "I'm sleeping";
    this.blocked = true;
    setTimeout(() => {
      inputController.status = "I'm awake now";
      this.blocked = false;
    }, sec * 1000);
  }
}

class Superhero extends Human {
  static fromHuman(h) {
    return new Superhero(h.firstName, h.lastName, h.gender, h.age);
  }

  fly() {
    inputController.status = "I'm flying";
    this.blocked = true;
    setTimeout(() => {
      this.blocked = false;
      inputController.status = "Idle";
    }, 10_000);
  }
  fightWithEvil() {
    inputController.status = 'Khhhh-chh... Bang-g-g-g... Evil is defeated!'
  }
}

let human = new Human("Some", "guy", "male", 19);

const turnIntoSuperhero = () => {
  if (human.calories < 500) return;
  const flyButton = document.createElement("button");
  flyButton.innerText = "Fly";
  flyButton.addEventListener("click", () => human.fly());
  const fightWithEvilButton = document.createElement("button");
  fightWithEvilButton.innerText = "Fight with evil";
  fightWithEvilButton.addEventListener("click", () => human.fightWithEvil());
  const elem = document.getElementById("humanDiv");
  elem.append(flyButton, fightWithEvilButton);

  human = Superhero.fromHuman(human);
};
