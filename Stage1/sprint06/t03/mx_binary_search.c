int mx_strcmp(const char *s1, const char *s2);

int mx_binary_search(char **arr, int size, const char *s, int *count) {
    if (!arr) return -1;
    int pointer = size / 2 - 1;
    int result;
    int max = size;
    int min = 0;
    while (pointer != min && pointer != max){
        (*count)++;
        result = mx_strcmp(arr[pointer], s);
        if (!result) return pointer;
        if (result > 0) max = pointer;
        if (result < 0) min = pointer;
        pointer = min + ((max - min) / 2);
    }
    (*count) = 0;
    return -1;
}
