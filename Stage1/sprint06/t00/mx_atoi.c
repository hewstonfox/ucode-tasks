#include <stdbool.h>

bool mx_isspace(char c);

bool mx_isdigit(char c);

int mx_atoi(const char *str) {
    int sign = 1;
    long long result;
    while (mx_isspace(*str)) return 0;
    if (*str == '-') {
        sign = -1;
        str++;
    }
    if (*str == '+') str++;
    if (!mx_isdigit(*str)) str++;
    result = *str - '0';
    str++;
    while (*str == '0') str++;
    while (mx_isdigit(*str)) {
        long long temp = result;
        result *= 10;
        result += *str - '0';
        if (result <= temp) return result * sign >= 0 ? 0 : -1;
        str++;
    }
    return (int)(result * sign);
}
