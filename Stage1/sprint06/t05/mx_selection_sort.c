int mx_strlen(const char *s);

int mx_strcmp(const char *s1, const char *s2);

int mx_selection_sort(char **arr, int size) {
    int swaps = 0;
    int min_idx;
    for (int i = 0; i < size; i++) {
        min_idx = i;
        for (int j = i; j < size; j++) {
            int la = mx_strlen(arr[j]);
            int lb = mx_strlen(arr[min_idx]);
            if (la < lb) min_idx = j;
            else
                if (la == lb) 
                    if (mx_strcmp(arr[j], arr[min_idx]) < 0) 
                        min_idx = j;
        }
        if (min_idx == i) continue;
        swaps++;
        char *tmp = arr[min_idx];
        arr[min_idx] = arr[i];
        arr[i] = tmp;
    }
    return swaps;
}
