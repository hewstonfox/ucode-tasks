int mx_mid(int a, int b, int c) {
    return a > b ? b > c ? b : c > a ? a : c : b < c ? b : c < a ? a : c;
}
