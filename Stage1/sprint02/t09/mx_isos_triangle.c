void mx_printchar(char c);

void mx_isos_triangle(unsigned int length, char c) {
    if (length == 0) {
        return;
    }
    for (unsigned int i = 1; i <= length; i++) {
        for (unsigned int num = 0; num < i; num++) {
            mx_printchar(c);
        }
        mx_printchar('\n');
    }
}
