int mx_max(int a, int b, int c) {
    int nums[] = {a, b, c};
    int max = a;
    for (int i = 0; i < 3; i++) {
        int num = nums[i];
        if (num >= max) {
            max = num;
        }
    }
    return max;
}
