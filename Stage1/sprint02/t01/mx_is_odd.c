#include <stdbool.h>

bool mx_is_odd(int value) {
    return (bool)(value & 1);
}
