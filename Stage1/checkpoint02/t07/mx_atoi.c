#include <stdbool.h>

static bool isspace(char c) {
    return (c >= '\t' && c <= '\r') || c == ' ';
}

static bool isdigit(int c) {
    return c >= '0' && c <= '9';
}

int mx_atoi(const char *str) {
    int sign = 1;
    long long result;
    while (isspace(*str)) str++;
    if (*str == '-') {
        sign = -1;
        str++;
    }
    if (*str == '+') str++;
    while (*str == '0') str++;
    if (!isdigit(*str)) return 0;
    result = *str - '0';
    str++;
    while (isdigit(*str)) {
        long long temp = result;
        result *= 10;
        result += *str - '0';
        if (result <= temp) return result * sign >= 0 ? 0 : -1;
        str++;
    }
    return (int)(result * sign);
}
