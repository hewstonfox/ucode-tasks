#include <unistd.h>
#include <fcntl.h>

#define ERR_USAGE "usage: ./read_file [file_path]\n"
#define ERR_ERR "error\n"

int mx_strlen(const char *s) {
    const char *iter = s;

    while (*iter != 0) {
        ++iter;
    }

    return iter - s;
}

int main(int argc, char **argv) {
    int fd;
    char buf;
    char status;
    if (argc != 2) {
        write(STDERR_FILENO, ERR_USAGE, mx_strlen(ERR_USAGE));
        return 0;
    }
    fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        write(STDERR_FILENO, ERR_ERR, mx_strlen(ERR_ERR));
        return 0;
    }
    while ((status = read(fd, &buf, 1)) != 0)
        if (status == -1) {
            write(STDERR_FILENO, ERR_ERR, mx_strlen(ERR_ERR));
            close(fd);
            return 0;
        }
        else
            write(STDOUT_FILENO, &buf, 1);
    close(fd);
    return 0;
}
