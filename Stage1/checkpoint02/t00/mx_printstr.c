#include <unistd.h>

static int strlen(const char *s) {
    int length = 0;
    while (*s++) length++;
    return length;
}

void mx_printstr(const char *s) {
    write(1, s, strlen(s));
}
