#include "list.h"

void mx_clear_list(t_list** list) {
  if (!*list) return;
  t_list* next;
  while (*list) {
    next = (*list)->next;
    (*list)->next = NULL;
    free(*list);
    *list = next;
  }
}
