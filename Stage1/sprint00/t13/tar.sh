#!/bin/bash
case $1 in 
  -c) 
    all_args=("$@")
    rest_args=("${all_args[@]:2}")
    tar -c -f $2 $rest_args
    ;;
  -e)
    tar -xf $2
    ;;
esac

