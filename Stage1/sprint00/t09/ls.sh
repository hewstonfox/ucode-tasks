#!/bin/bash
ls -Alh $@ | while IFS= read -r line
do
  echo $line | awk '$9{print $9" "$5}'
done
