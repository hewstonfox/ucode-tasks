void mx_sort_arr_int(int *arr, int size) {
    for (int i = 1; i < size; i++) {
        int tmp = arr[i];
        int j = i - 1;
        while (tmp < arr[j] && j >= 0) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = tmp;
    }
}
