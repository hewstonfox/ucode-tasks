static int strlen(const char *s) {
    int length = 0;
    while (*s++) length++;
    return length;
}

static void swap_char(char *s1, char *s2) {
    char tmp = *s1;
    *s1 = *s2;
    *s2 = tmp;
}

void mx_str_reverse(char *s) {
    int length = strlen(s) - 1;
    int half_length = length / 2 + (length & 1 ? 1 : 0);
    for (int i = 0; i < half_length; i++) {
        swap_char(&s[i], &s[length - i]);
    }
}
