char *mx_strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src != '\0') *dst++ = *src++;
    *dst = '\0';
    return orig;
}
