#include <unistd.h>

void mx_only_printable(void) {
    int count = 126;
    while (count >= 32) {
        write(1, &count, 1);
        count--;
    }
    write(1, "\n", 1);
}
