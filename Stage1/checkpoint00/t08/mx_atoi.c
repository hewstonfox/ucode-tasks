#include <stdbool.h>

static bool isspace(char c) {
    return (c >= '\t' && c <= '\r') || c == ' ';
}

static bool isdigit(int c) {
    return c >= '0' && c <= '9';
}

int mx_atoi(const char *str) {
    int should_invert = 0;
    long long result;
    while (isspace(*str)) str++;
    if (*str == '-') {
        should_invert = 1;
        str++;
    }
    if (*str == '+') str++;
    if (!isdigit(*str)) return 0;
    result = *str - '0';
    str++;
    while (*str == '0') str++;
    while (isdigit(*str)) {
        long long temp = result;
        result *= 10;
        result += *str - '0';
        str++;
        if (result <= temp) return should_invert ? 0 : -1;
    }
    if (should_invert) result *= -1;
    return (int)result;
}
