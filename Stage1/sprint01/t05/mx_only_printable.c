#include <unistd.h>
#include "mx_printchar.c"

void mx_only_printable(void) {
    int count = 127;
    while (count >= 32) {
        mx_printchar(count);
        count--;
    }
    write(1, "\n", 1);
}
