#include <unistd.h>
#include "mx_printchar.c"

void mx_hexadecimal(void) {
    for (int hex = '0'; hex <= 'F'; hex++) {
        if (hex >= ':' && hex <= '@') {
            continue;
        }
        mx_printchar(hex);
    }
    write(1, "\n", 1);
}
