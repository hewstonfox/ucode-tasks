#include <unistd.h>
#include "mx_printchar.c"

void mx_print_alphabet(void) {
    int start = 'A';
    int stop = start + 26;
    for (int cur = start; cur < stop; cur++) {
        mx_printchar(cur % 2 ? cur : cur + 32);
    }
    write(1, "\n", 1);
}
