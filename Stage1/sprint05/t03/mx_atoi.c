#include <stdbool.h>

bool mx_isspace(char c);

bool mx_isdigit(int c);

int mx_atoi(const char *str) {
    int should_invert = 0;
    long long result;
    while (mx_isspace(*str)) return 0;
    if (*str == '-') {
        should_invert = 1;
        str++;
    }
    if (*str == '+') str++;
    if (!mx_isdigit(*str)) return 0;
    result = *str - '0';
    str++;
    while (*str == '0') str++;
    while (mx_isdigit(*str)) {
        long long temp = result;
        result *= 10;
        result += *str - '0';
        str++;
        if (result <= temp) return should_invert ? 0 : -1;
    }
    if (*str != '\0') return 0;
    if (should_invert) result *= -1;
    return (int)result;
}
