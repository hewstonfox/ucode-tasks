char *mx_strchr(const char *s, int c);

void mx_printchar(char c);

void mx_printstr(const char *s);

int main(int argc, char const *argv[]) {
    if (!argc) return 0;
    char *path = (char *)argv[0];
    char *tmp = mx_strchr(path, '/');
    while ((tmp = mx_strchr(path, '/'))) path = tmp + 1;
    mx_printstr(path);
    mx_printchar('\n');
    return 0;
}
