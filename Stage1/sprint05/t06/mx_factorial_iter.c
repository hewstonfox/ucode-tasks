int mx_factorial_iter(int n) {
    if (n < 1) return 0;
    int fac = 1;
    int prev_fac = 0;
    while (n > 1) {
        fac *= n;
        if (prev_fac >= fac) return 0;
        prev_fac = fac;
        n--;
    }
    return fac;
}
