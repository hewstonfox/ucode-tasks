int mx_gcd(int a, int b) {
    return b != 0 ? mx_gcd(b, a % b) : a < 0 ? a * -1 : a;
}
