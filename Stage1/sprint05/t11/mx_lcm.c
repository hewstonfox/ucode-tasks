int mx_gcd(int a, int b);

int mx_lcm(int a, int b) {
    int gcd = mx_gcd(a, b);
    int lcm = gcd;
    while (lcm % a != 0 || lcm % b != 0) lcm += gcd;
    return lcm;
}
