int mx_atoi(const char *str);

void mx_printchar(char c);

void mx_printint(int n);

int mx_strlen(const char *s);

int main(int argc, char const *argv[]) {
    if (argc != 3) return 0;
    if (mx_strlen(argv[1]) > 1 || mx_strlen(argv[2]) > 1) return 0;
    int a = mx_atoi(argv[1]);
    int b = mx_atoi(argv[2]);
    if (a < 1 || b < 1) return 0;
    if (a > b) {
        int tmp = a;
        a = b;
        b = tmp;
    }
    for (int i = a; i <= b; i++) {
        for (int j = a; j <= b; j++) {
            if (j != a) mx_printchar('\t');
            mx_printint(i * j);
        }
        mx_printchar('\n');
    }
    return 0;
}
