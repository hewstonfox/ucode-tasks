#include <unistd.h>

int mx_atoi(const char *str);

void mx_printchar(char c);

int main(int argc, char const *argv[]) {
    for (int i = 1; i < argc; i++) {
        int num = mx_atoi(argv[i]);
        for (int j = 31; j >= 0; j--) 
            mx_printchar(num & (1 << j) ? '1' : '0');
        mx_printchar('\n');
    }
    return 0;
}
