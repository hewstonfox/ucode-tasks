int mx_strncmp(const char *s1, const char *s2, int n) {
    if (n <= 0) {
        return 0;
    }

    while (*s1 && *s1 == *s2 && n > 0) {
        if (!--n) {
            break;
        }
        s1++;
        s2++;
    }

    return *s1 - *s2;
}
