char *mx_strstr(const char *s1, const char *s2);

int mx_count_substr(const char *str, const char *sub) {
    int count = 0;
    while ((str = mx_strstr(str, sub))) {
        count++;
        str++;
    }
    return count;
}
