#include <stdbool.h>

bool mx_isspace(int c);

bool mx_isdigit(int c);

int mx_atoi(const char *str) {
    while (mx_isspace(*str)) {
        str++;
    }

    int should_invert = 0;
    if (*str == '-') {
        should_invert = 1;
        str++;
    }

    if (!mx_isdigit(*str)) {
        return 0;
    }

    int max_int_char = 10;
    long long result = *str - '0';
    str++;

    while (mx_isdigit(*str) && max_int_char) {
        result *= 10;
        result += *str - '0';
        str++;
        max_int_char--;
    }

    if (should_invert) {
        result *= -1;
    }

    if (result > 2147483647LL) {
        return 2147483647;
    }

    if (result < -2147483648LL) {
        return -2147483648;
    }

    return (int)result;
}
