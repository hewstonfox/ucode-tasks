int mx_popular_int(const int *arr, int size) {
    int num = arr[0];
    int max_frq = 1;
    for (int i = 0; i < size - 1; i++) {
        int curr_frq = 1;

        for (int j = i + 1; j < size; j++) {
            if (arr[i] == arr[j]) {
                curr_frq += 1;
            }
        }

        if (curr_frq > max_frq) {
            max_frq = curr_frq;
            num = arr[i];
        }
    }

    return num;
}
