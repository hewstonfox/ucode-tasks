void mx_printchar(char c);

void mx_printint(int n) {
    if (n == -2147483648) {
        char *minint = "-2147483648";
        for (int i = 0; i < 11; ++i) {
            mx_printchar(minint[i]);
        }
        return;
    }

    if (n < 0) {
        mx_printchar('-');
        n *= -1;
    }

    if (n < 10) {
        mx_printchar(n + '0');
    } else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}
