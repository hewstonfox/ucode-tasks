#include <stdlib.h>

static char *strnew(const int size) {
    if (size < 0) return NULL;
    char *str = malloc(size + 1);
    for (int i = 0; i <= size; i++) 
        str[i] = '\0';
    return str;
}

char *mx_nbr_to_hex(unsigned long nbr) {
    if (nbr < 0) return 0;
    int str_size = 1;
    unsigned long nbr_cpy = nbr;
    while (nbr_cpy >= 16) {
        nbr_cpy /= 16;
        str_size++;
    }
    char *hex = strnew(str_size--);
    while (nbr >= 16) {
        int rest = nbr % 16;
        nbr /=  16; 
        hex[str_size--] = rest + (rest < 10 ? '0' : 'W');
    }
    hex[str_size] = nbr + (nbr < 10 ? '0' : 'W');
    return hex;
}
