#include <unistd.h>

static void printchar(char c) {
    write(1, &c, 1);
}

static int strlen(const char *s) {
    int count;
    for (count = 0; s[count]; count++);
    return count;
}

static void printstr(const char *s) {
    write(1, s, strlen(s));
}

static void printint(int n) {
    if (n < 0) {
        printchar('-');
        n *= -1;
    }
    if (n < 10) {
        printchar(n + '0');
    }
    else {
        printint(n / 10);
        printchar(n % 10 + '0');
    }
}

int main(int argc, char const *argv[]) {
    printstr(argv[0]);
    printchar('\n');
    printint(argc);
    printchar('\n');
    return 0;
}
