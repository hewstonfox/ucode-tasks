#include <unistd.h>

static int strlen(const char *s) {
    int count;
    for (count = 0; s[count]; count++);
    return count;
}

static void printchar(char c) {
    write(1, &c, 1);
}

static void printstr(const char *s) {
    write(1, s, strlen(s));
}

int main(int argc, char const *argv[]) {
    for (int i = 1; i < argc; i++) {
        printstr(argv[i]);
        printchar('\n');
    }
    return 0;
}
