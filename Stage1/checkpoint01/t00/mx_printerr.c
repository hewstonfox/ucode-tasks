#include <unistd.h>
#include <stdio.h>

static int strlen(const char *s) {
    int count;
    for (count = 0; s[count]; count++);
    return count;
}

void mx_printerr(const char *s) {
    write(STDERR_FILENO, s, strlen(s));
}
