#include <stdlib.h>

static char *strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src) *dst++ = *src++;
    return orig;
}

static int strlen(const char *s) {
    int count;
    for (count = 0; s[count]; count++);
    return count;
}

static char *strnew(const int size) {
    if (size < 0) return NULL;
    char *str = malloc(size + 1);
    for (int i = 0; i <= size; i++) 
        str[i] = '\0';
    return str;
}

char *mx_strdup(const char *str) {
    char *dup = strnew(strlen(str));
    strcpy(dup, str);
    return dup;
}
