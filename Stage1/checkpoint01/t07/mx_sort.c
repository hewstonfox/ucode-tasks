#include <stdbool.h>

void mx_sort(int *arr, int size, bool (*f)(int, int)) {
    int tmp;
    int j;
    for (int i = 1; i < size; i++) {
        tmp = arr[i];
        j = i - 1;
        while (f(arr[j], tmp) && j >= 0) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = tmp;
    }
}
