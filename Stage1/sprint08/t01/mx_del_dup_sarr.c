#include "duplicate.h"

t_intarr *mx_del_dup_sarr(t_intarr *src) {
    if (src == NULL || src -> arr == NULL) return NULL;
    int *safe_copy = mx_copy_int_arr(src -> arr, src -> size);
    int pointer = 0;
    int current = 0;
    while (pointer < src -> size) {
        int skip = 0;
        for (int i = 0; i < pointer; i++) {
            if (safe_copy[pointer] == safe_copy[i]) {
                pointer++;
                skip = 1;
                break;
            }
        }
        if (skip)
            continue;
        safe_copy[current++] = safe_copy[pointer++];
    }
    t_intarr *result = (t_intarr*)malloc(16);
    result->arr = mx_copy_int_arr(safe_copy, current);
    result->size = current;
    return result;
}
