#include <stdbool.h>

bool mx_isalpha(int c);

bool mx_isdigit(int c);

bool mx_islower(int c);

bool mx_isupper(int c);

unsigned long mx_hex_to_nbr(const char *hex) {
    if (!hex) return 0;
    unsigned long result = 0;
    unsigned long mult;
    int num; 
    int len;
    for (len = 0; hex[len]; len++);
    while (*hex) {
        if (mx_isdigit(*hex)) num = *hex - '0';
        else if (mx_islower(*hex) && *hex <= 'f') num = *hex - 'W';
        else if (mx_isupper(*hex) && *hex <= 'F') num = *hex - '7';
        else return 0;
        hex++;
        len--;
        mult = 1;
        for (int i = 0; i < len; i++) mult *= 16;
        result += num * mult;
    }
    return result;
}
