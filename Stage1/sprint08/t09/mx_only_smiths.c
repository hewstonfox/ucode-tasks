#include "only_smiths.h"

#include <stdio.h>

t_agent **mx_only_smiths(t_agent **agents, int strength) {
    if (agents == NULL) return NULL;
    int len;
    for (len = 0; agents[len]; len++);
    t_agent **filtered = (t_agent **)malloc(16 *  len);
    int filtered_len = 0;
    for (int i = 0; i < len; i++) {
        if (mx_strcmp(agents[i]->name, "Smith") != 0 || agents[i] -> strength >= strength) continue;
        filtered[filtered_len++] = agents[i];
    }
    t_agent **small_filtered = (t_agent **)malloc(16 *  (filtered_len + 1));
    for (int i = 0; i < filtered_len; i++)
        small_filtered[i] = mx_create_agent("Smith", filtered[i]->power, filtered[i]->strength);
    small_filtered[filtered_len] = NULL;
    return small_filtered;
}
