char *mx_nbr_to_hex(unsigned long nbr);

char *mx_strnew(const int size);

int mx_strlen(const char *s);

char *mx_strcpy(char *dst, const char *src);

char *mx_get_address(void *p) {
    char *hex = mx_nbr_to_hex((unsigned long)p);
    char *prefix = mx_strnew(mx_strlen(hex) + 2);
    *prefix = '0';
    *(prefix + 1) = 'x';
    mx_strcpy(prefix + 2, hex);
    return prefix;
}
