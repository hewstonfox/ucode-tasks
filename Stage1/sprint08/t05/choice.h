#pragma once

#include <stdlib.h>
#include <stdio.h>

#ifndef MX_SUCCESS_PHRASE
#define MX_SUCCESS_PHRASE "Follow me!"
#endif /* !MX_SUCCESS_PHRASE */

#ifndef MX_FAIL_PHRASE
#define MX_FAIL_PHRASE "Perhaps I was wrong about you, Neo."
#endif /* !MX_FAIL_PHRASE */

#ifndef MX_UNDEFINED_PHRASE
#define MX_UNDEFINED_PHRASE "Are you sure about that?"
#endif /* !MX_UNDEFINED_PHRASE */

#ifndef MX_RED_PILL
#define MX_RED_PILL 1
#endif /* !MX_RED_PILL */

#ifndef MX_BLUE_PILL
#define MX_BLUE_PILL 2
#endif /* !MX_BLUE_PILL */

char *mx_strnew(const int size) {
    if (size < 0) return NULL;
    char *str = malloc(size + 1);
    for (int i = 0; i <= size; i++) 
        str[i] = '\0';
    return str;
}

int mx_strlen(const char *s) {
    int count = 0;
    while (s[count] != '\0')
        count++;
    return count;
}

char *mx_strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src) 
        *dst++ = *src++;
    return orig;
}

char *mx_strdup(const char *str) {
    char *dup = mx_strnew(mx_strlen(str));
    mx_strcpy(dup, str);
    return dup;
}

typedef char t_phrase;
