#include <stdlib.h>

int *mx_copy_int_arr(const int *src, int size);

int *mx_del_dup_arr(int *src, int src_size, int *dst_size) {
    if (src == NULL) return NULL;
    int *safe_copy = mx_copy_int_arr(src, src_size);
    int pointer = 0;
    int current = 0;
    while (pointer < src_size) {
        int skip = 0;
        for (int i = 0; i < pointer; i++) {
            if (safe_copy[pointer] == safe_copy[i]) {
                pointer++;
                skip = 1;
                break;
            }
        }
        if (skip) continue;
        safe_copy[current++] = safe_copy[pointer++];
    }
    *dst_size = current;
    return mx_copy_int_arr(safe_copy, current);
}
