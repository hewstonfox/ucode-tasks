#include <stdbool.h>
#include <stdlib.h>

bool mx_isspace(char c);

int mx_strlen(const char *s);

char *mx_strnew(const int size);

char *mx_strncpy(char *dst, const char *src, int len);

char *mx_strtrim(const char *str) {
    if (str == NULL) return NULL;
    while (mx_isspace(*str)) str++;
    int len = mx_strlen(str);
    while (mx_isspace(str[len - 1])) len--;
    return mx_strncpy(mx_strnew(len), str, len);
}
