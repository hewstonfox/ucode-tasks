int mx_count_words(const char *str, char delimiter) {
    while (*str == delimiter) str++;
    if (!*str) return 0;
    int count = 0;
    int skip = 0;
    while (*str) {
        char c = *str++;
        if (c == delimiter && skip) continue;
        if (c == delimiter) {
            skip = 1;
            count++;
            continue;
        }
        skip = 0;
    }
    if (*(str - 1) != delimiter) count++;
    return count;
}
