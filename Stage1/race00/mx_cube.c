void mx_printchar(char c);

void mx_cube(int n) {
    int s = -(n  /  2 + 1);
    int halpth = (int)(n / 2.0 + 0.5);
    if (n <= 1) return;
    for (int fr = s; fr <= n + 1; fr++) {
        int is_corner = fr == s || fr == 0 || fr == n + 1;
        int ss = fr < 0 ? -(s - fr) - 1 : fr < halpth ? n / 2 : n - fr;
        for (int i = fr; i < 0; i++) mx_printchar(' ');
        mx_printchar(is_corner ? '+' : fr < 0 ? '/' : '|');
        for (int i = 0; i < n * 2; i++) 
            mx_printchar(fr == s || fr == 0 || fr == n + 1 ? '-' : ' ');
        mx_printchar(is_corner ? '+' : fr < 0 ? '/' : '|');
        for (int i = 0; i < ss; i++) mx_printchar(' ');
        if (fr > s && fr < n + 1) 
            mx_printchar(fr < halpth ? '|' : fr == halpth? '+' : '/');
        mx_printchar('\n');
    }
}
