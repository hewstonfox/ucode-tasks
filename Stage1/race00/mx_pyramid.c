void mx_printchar(char c);

void mx_pyramid(int n) {
    if (n <= 1 || n & 1) return;
    for (int frame = 0; frame < n; ++frame) {
        for (int space = 1; space < n - frame; ++space) mx_printchar(' ');
        mx_printchar('/');
        for (int space = 0; space < frame + frame - 1; ++space) 
            mx_printchar(frame + 1 == n ? '_' : ' ');
        mx_printchar('\\');
        if (!frame) {
            mx_printchar('\n');
            continue;
        }
        if (frame < n / 2)
            for (int space = 0; space < frame; ++space) mx_printchar(' ');
        else
            for (int space = 1; space < n - frame; ++space) mx_printchar(' ');
        mx_printchar(frame < n / 2 ? '\\': '|');
        mx_printchar('\n');
    }
}
