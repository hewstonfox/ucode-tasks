#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MX_E_NO_FILE "map does not exist\n"
#define MX_E_MAP "map error\n"
#define MX_E_UNKNOWN "error\n"
#define MX_E_USAGE "usage: ./way_home [file_name] [x1] [y1] [x2] [y2]\n"
#define MX_E_OUT_OF_RANGE "points are out of map range\n"
#define MX_E_ENTRY_OBSTACLE "entry point cannot be an obstacle\n"
#define MX_E_EXIT_OBSTACLE "exit point cannot be an obstacle\n"
#define MX_E_ROUTE "route not found\n"

typedef enum e_vstatus {
    VS_VALID,
    VS_USAGE,
    VS_NO_FILE,
    VS_MAP_ERR,
    VS_OUT_OF_RANGE,
    VS_ENTRY_OBSTACLE,
    VS_EXIT_OBSTACLE,
    VS_ROUTE,
    VS_UNKNOWN,
}            t_vstatus;

int mx_strlen(const char *s);
void mx_print(int fd, const char *msg);
void mx_printerr(t_vstatus err);

bool mx_isspace(char c);
bool mx_isdigit(char c);
t_vstatus mx_atoi(const char *str, long long *number);
void mx_printint(int num);
