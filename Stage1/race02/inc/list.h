#pragma once
#include "map.h"
#include <stdbool.h>
#include <stdlib.h>

typedef struct s_list {
    t_point data;
    struct s_list *next;
    struct s_list *from;
    struct s_list *prev;
    int cost;
} t_list;

typedef bool (*t_data_cmp)(const t_point *a, const t_point *b);

t_list *mx_create_node(const t_point *data, t_list *from, int cost);
void mx_free_node(t_list **node);
void mx_clear_list(t_list **list);

void mx_push_back(t_list **list, t_list *node);

t_list *mx_pop_back(t_list **list);
t_list *mx_pop_front(t_list **list);
t_list *mx_pop_index(t_list **list, int index);

int mx_list_size(t_list *list);
bool mx_in_list(t_list *list, const t_point *sought, t_data_cmp cmp);
t_list *mx_find_in_list(t_list *list, const t_point *sought, t_data_cmp cmp);
int mx_max_cost(t_list *list);
