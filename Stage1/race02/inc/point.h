#pragma once

#include "list.h"
#include "map.h"

bool mx_eq_point(const t_point *a, const t_point *b);

t_list *mx_get_neighbors(t_list *point_l, t_map *map);

int mx_estimate_distance(const t_point *a, const t_point *b);

t_list *mx_choose_node(t_list **list, const t_point *goal);
