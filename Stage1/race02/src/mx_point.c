#include "point.h"

bool mx_eq_point(const t_point *a, const t_point *b) {
    return a->y == b->y && a->x == b->x;
}

t_list *mx_get_neighbors(t_list *point_l, t_map *map) {
    const t_point *point = &point_l->data;
    t_list *neighbors = NULL;
    int cost = point_l->cost + 1;
    if (point->x - 1 >= 0
        && map->map[point->y][point->x - 1] == '.') {
        t_list *tmp_node = mx_create_node(&(t_point){point->x - 1, point->y},
                                          point_l, cost);
        mx_push_back(&neighbors, tmp_node);
    }
    if (point->x + 1 < (long long)map->width
        && map->map[point->y][point->x + 1] == '.') {
        t_list *tmp_node = mx_create_node(&(t_point){point->x + 1, point->y},
                                          point_l, cost);
        mx_push_back(&neighbors, tmp_node);
    }
    if (point->y - 1 >= 0
        && map->map[point->y - 1][point->x] == '.') {
        t_list *tmp_node = mx_create_node(&(t_point){point->x, point->y - 1},
                                          point_l, cost);
        mx_push_back(&neighbors, tmp_node);
    }
    if (point->y + 1 < (long long)map->height
        && map->map[point->y + 1][point->x] == '.') {
        t_list *tmp_node = mx_create_node(&(t_point){point->x, point->y + 1},
                                          point_l, cost);
        mx_push_back(&neighbors, tmp_node);
    }
    return neighbors;
}

int mx_estimate_distance(const t_point *a, const t_point *b) {
    int x = a->x - b->x;
    int y = a->y - b->y;
    if (x < 0) x *= -1;
    if (y < 0) y *= -1;
    return x + y;
}

t_list *mx_choose_node(t_list **list, const t_point *goal) {
    int count = 0;
    int to_del_count = -1;

    int min_cost = __INT_MAX__;
    int cost_node_to_goal;
    for (t_list *cur = *list; cur; cur = cur->next) {
        cost_node_to_goal = mx_estimate_distance(&cur->data, goal);
        if (min_cost > cur->cost + cost_node_to_goal) {
            to_del_count = count;
            min_cost = cur->cost + cost_node_to_goal;
        }
        count++;
    }
    if (to_del_count >= 0)
        return mx_pop_index(list, to_del_count);
    return NULL;
}
