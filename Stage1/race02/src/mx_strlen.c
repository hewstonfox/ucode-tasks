int mx_strlen(const char *s) {
    const char *iter = s;

    while (*iter != 0) {
        ++iter;
    }

    return iter - s;
}
