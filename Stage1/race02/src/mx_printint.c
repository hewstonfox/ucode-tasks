#include "way_home.h"

void mx_printint(int num) {
    long long number = num > 0 ? num : -num;
    char digit = '0' + number % 10;
    if (num < 0)
        write(STDOUT_FILENO, "-", 1);
    if (number >= 10)
        mx_printint(number / 10);
    write(STDOUT_FILENO, &digit, 1);
}
