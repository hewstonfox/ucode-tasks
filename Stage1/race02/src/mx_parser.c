#include "parser.h"

static bool alloc_map(t_map *map) {
    map->map = malloc(map->height * sizeof(char *));
    if (map->map == NULL)
        return false;
    for (unsigned int i = 0; i < map->height; ++i) {
        map->map[i] = malloc(map->width);
        if (map->map[i] == NULL) {
            return false;
        }
    }
    return true;
}

static t_vstatus load_map(int fd, t_map *map) {
    unsigned int x = 0;
    unsigned int y = 0;
    char buf;
    while (y < map->height) {
        if (read(fd, &buf, 1) <= 0)
            return VS_UNKNOWN;
        if (buf == '\n') {
            ++y;
            x = 0;
        } else if (mx_is_valid_symbol(buf)) {
            map->map[y][x] = buf;
            ++x;
        }
    }
    return VS_VALID;
}

t_vstatus mx_load_map(const char *filename, t_map *map) {
    int fd;
    t_vstatus status;
    if (filename == NULL || map == NULL)
        return VS_UNKNOWN;
    fd = open(filename, O_RDONLY);
    if (fd == -1)
        return VS_NO_FILE;
    if (!alloc_map(map)) {
        close(fd);
        return VS_UNKNOWN;
    }
    status = load_map(fd, map);
    close(fd);
    return status;
}
