#include "way_home.h"
#include <limits.h>

static const char *get_sign(const char *str, int *sign) {
    while (mx_isspace(*str))
        ++str;
    if (*str == '\0')
        return str;
    if (*str == '-' || *str == '+') {
        if (*str == '-')
            *sign = -1;
        ++str;
    }
    return str;
}

t_vstatus mx_atoi(const char *str, long long *number) {
    int sign = 1;
    long long value = 0;
    str = get_sign(str, &sign);
    if (!mx_isdigit(*str))
        return VS_UNKNOWN;
    while (mx_isdigit(*str)) {
        value *= 10;
        value += *str - '0';
        if ((sign == 1 && value > INT_MAX)
            || (sign == -1 && value < INT_MAX + 1LL))
            return VS_UNKNOWN;
        ++str;
    }
    *number = value * sign;
    return VS_VALID;
}
