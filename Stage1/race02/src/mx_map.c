#include "map.h"

void mx_write_map(int fd, t_map *map) {
    for (unsigned int y = 0; y < map->height; ++y) {
        write(fd, map->map[y], map->width);
        write(fd, "\n", 1);
    }
}

t_vstatus mx_write_map_file(const char *filename, t_map *map) {
    int fd;
    if (filename == NULL || map == NULL)
        return VS_UNKNOWN;
    fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC,
              S_IRWXU | S_IRWXG | S_IRWXO);
    if (fd == -1)
        return VS_UNKNOWN;
    mx_write_map(fd, map);
    close(fd);
    return VS_VALID;
}

t_map *mx_create_map() {
    t_map *res = malloc(sizeof(t_map));
    if (res == NULL)
        return NULL;
    res->map = NULL;
    res->width = 0;
    res->height = 0;
    res->entry = (t_point){0, 0};
    res->exit = (t_point){0, 0};
    return res;
}

void mx_free_map(t_map **map) {
    if (map == NULL || *map == NULL)
        return;
    if ((*map)->map != NULL) {
        for (unsigned int i = 0; i < (*map)->height; ++i) {
            if ((*map)->map[i] != NULL)
                free((*map)->map[i]);
        }
        free((*map)->map);
    }
    free(*map);
    *map = NULL;
}

// '.' - space, '#' - obstacle.
bool mx_is_valid_symbol(char c) {
    return c == '.' || c == '#';
}
