#include "../inc/list.h"

t_list *mx_create_node(const t_point *data, t_list *from, int cost) {
    t_list *node = (t_list *) malloc(sizeof(t_list));
    if (node == NULL)
        return NULL;
    node->data = *data;
    node->next = NULL;
    node->prev = NULL;
    node->from = from;
    node->cost = cost;
    return node;
}

void mx_free_node(t_list **node) {
    if (node != NULL && *node != NULL) {
        free(*node);
        *node = NULL;
    }
}

void mx_clear_list(t_list **list) {
    t_list *it;
    if (list != NULL && *list != NULL) {
        it = *list;
        while (it != NULL) {
            t_list *next = it->next;
            mx_free_node(&it);
            it = next;
        }
        *list = NULL;
    }
}

void mx_push_back(t_list **list, t_list *node) {
    t_list *cur;
    if (*list == NULL) {
        node->next = NULL;
        node->prev = NULL;
        *list = node;
        return;
    }
    for (cur = *list; cur->next != NULL; cur = cur->next)
        ;
    cur->next = node;
    node->next = NULL;
    node->prev = cur;
}

t_list *mx_pop_front(t_list **list) {
    if (list == NULL || *list == NULL)
        return NULL;
    t_list *cur = *list;
    if (cur->next)
        cur->next->prev = NULL;
    *list = cur->next;
    cur->next = NULL;
    cur->prev = NULL;
    return cur;
}

t_list *mx_pop_back(t_list **list) {
    t_list *cur;
    t_list *to_pop;
    if (list == NULL || *list == NULL)
        return NULL;
    cur = *list;
    if (cur->next == NULL) {
        *list = NULL;
        return cur;
    }
    for (; cur->next->next != NULL; cur = cur->next)
        ;
    to_pop = cur->next;
    cur->next = NULL;
    to_pop->next = NULL;
    to_pop->prev = NULL;
    return to_pop;
}

t_list *mx_pop_index(t_list **list, int index) {
    if (index <= 0)
        return mx_pop_front(list);
    t_list *cur = *list;
    for (int i = 1; i <= index && cur->next; i++) {
        if (i == index) {
            t_list *to_pop = cur->next;
            cur->next = to_pop->next;
            to_pop->next = NULL;
            to_pop->prev = NULL;
            return to_pop;
        }
        cur = cur->next;
    }
    return mx_pop_back(list);
}

int mx_list_size(t_list *list) {
    int count = 0;
    for (; list != NULL; list = list->next)
        ++count;
    return count;
}

bool mx_in_list(t_list *list, const t_point *sought, t_data_cmp cmp) {
    if (cmp == NULL || sought == NULL)
        return false;
    for (t_list *cur = list; cur != NULL; cur = cur->next)
        if (cmp(sought, &cur->data))
            return true;
    return false;
}

t_list *mx_find_in_list(t_list *list, const t_point *sought, t_data_cmp cmp) {
    if (cmp == NULL || sought == NULL)
        return NULL;
    for (t_list *cur = list; cur != NULL; cur = cur->next)
        if (cmp(sought, &cur->data))
            return cur;
    return NULL;
}

int mx_max_cost(t_list *list) {
    int max = list->cost;
    for (t_list *cur = list->next; cur != NULL; cur = cur->next)
        if (cur->cost > max)
            max = cur->cost;
    return max;
}
