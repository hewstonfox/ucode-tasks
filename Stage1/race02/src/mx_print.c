#include "way_home.h"

void mx_print(int fd, const char *msg) {
    write(fd, msg, mx_strlen(msg));
}

void mx_printerr(t_vstatus err) {
    if (err == VS_NO_FILE)
        mx_print(STDERR_FILENO, MX_E_NO_FILE);
    else if (err == VS_MAP_ERR)
        mx_print(STDERR_FILENO, MX_E_MAP);
    else if (err == VS_UNKNOWN)
        mx_print(STDERR_FILENO, MX_E_UNKNOWN);
    else if (err == VS_OUT_OF_RANGE)
        mx_print(STDERR_FILENO, MX_E_OUT_OF_RANGE);
    else if (err == VS_ENTRY_OBSTACLE)
        mx_print(STDERR_FILENO, MX_E_ENTRY_OBSTACLE);
    else if (err == VS_EXIT_OBSTACLE)
        mx_print(STDERR_FILENO, MX_E_EXIT_OBSTACLE);
    else if (err == VS_ROUTE)
        mx_print(STDERR_FILENO, MX_E_ROUTE);
    else if (err == VS_USAGE)
        mx_print(STDERR_FILENO, MX_E_USAGE);
}
