#ifndef MINILIBMX_H
#define MINILIBMX_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "calculator.h"

void mx_throwerr(const char *s);

void mx_printusage(void);

void mx_throw_standart_err(int e);

bool mx_isdigit(char c);

bool mx_isspace(char c);

int mx_atoi(const char* str);

void mx_printchar(char c);

void mx_printint(int n);

int mx_strlen(const char *s);

t_operation *mx_create_operation(int operator);

#endif
