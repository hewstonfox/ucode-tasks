#include "../inc/minilibmx.h"

t_operation *mx_create_operation(int operator) {
    t_operation *op = (t_operation*)malloc(2);
    op->op = operator;
    switch (operator)
    {
    case SUB:
        op->f = &mx_sub;
        break;
    case ADD:
        op->f = &mx_add;
        break;
    case MUL:
        op->f = &mx_mul;
        break;
    case DIV:
        op->f = &mx_div;
        break;
    case MOD:
        op->f = &mx_mod;
        break;
    }
    return op;
}
