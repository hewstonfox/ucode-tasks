#include "../inc/minilibmx.h"

int main(int argc, char const *argv[]) {
    if (argc != 4) mx_printusage();
    enum e_operation operation;
    switch (argv[2][0]) {
        case '-':
            operation = SUB;
            break;
        case '+':
            operation = ADD;
            break;
        case '*':
            operation = MUL;
            break;
        case '/':
            operation = DIV;
            break;
        case '%':
            operation = MOD;
            break;
        default:
            mx_throw_standart_err(INCORRECT_OPERATION);
            return 0;
    }
    t_operation *op = mx_create_operation(operation);
    int a = mx_atoi(argv[1]);
    int b = mx_atoi(argv[3]);
    if ((op->op == DIV || op->op == MOD) && !b) mx_throw_standart_err(DIV_BY_ZERO);
    mx_printint(op->f(a, b));
    mx_printchar('\n');
    return 0;
}
