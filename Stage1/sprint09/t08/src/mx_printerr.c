#include "../inc/minilibmx.h"

void mx_throwerr(const char *s) {
    write(STDERR_FILENO, s, mx_strlen(s));
    exit(0);
}

void mx_printusage(void) {
    mx_throwerr("usage: ./calc [operand1] [operation] [operand2]\n");
}

void mx_throw_standart_err(int e) {
    switch (e)
    {
        case INCORRECT_OPERAND:
            mx_throwerr("error: invalid number\n");
            break;
        case INCORRECT_OPERATION:
            mx_throwerr("error: invalid operation\n");
            break;
        case DIV_BY_ZERO:
            mx_throwerr("error: division by zero\n");
            break;
    }
}
