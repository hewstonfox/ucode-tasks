#include "../inc/minilibmx.h"

int mx_atoi(const char *str) {
    int sign = 1;
    long long result;
    while (mx_isspace(*str)) mx_throw_standart_err(INCORRECT_OPERAND);
    if (*str == '-') {
        sign = -1;
        str++;
    }
    if (*str == '+') str++;
    if (!mx_isdigit(*str)) str++;
    result = *str - '0';
    str++;
    while (*str == '0') str++;
    while (mx_isdigit(*str)) {
        long long temp = result;
        result *= 10;
        result += *str - '0';
        if (result <= temp) return result * sign >= 0 ? 0 : -1;
        str++;
    }
    if (*str != '\0') mx_throw_standart_err(INCORRECT_OPERAND);
    return (int)(result * sign);
}
