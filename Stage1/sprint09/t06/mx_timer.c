#include <time.h>

double mx_timer(void (*f)()) {
    if (f == NULL) return -1;
    double start;
    double end;
    start = clock();
    f();
    end = clock();
    return end - start;
}
