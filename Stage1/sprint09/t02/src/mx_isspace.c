#include "../inc/header.h"

bool mx_isspace(char c) {
    return (c >= '\t' && c <= '\r') || c == ' ';
}
