#include <stdbool.h>

double mx_pow(double n, unsigned int pow);

bool mx_is_narcissistic(int num) {
    if (num < 0) {
        return false;
    }

    int init_num = num;

    int digits_count = 0;
    while (num) {
        digits_count++;
        num /= 10;
    }
    num = init_num;

    int sum = 0;
    while (num) {
        sum += mx_pow(num % 10, digits_count);
        num /= 10;
    }

    return sum == init_num;
}
