char *mx_strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src) {
        *dst++ = *src++;
    }
    return orig;
}
