int mx_toupper(int c) {
    return c >= 'a' && c <= 'z' ? c - ' ' : c;
}
