void mx_printchar(char c);

void mx_str_separate(const char *str, char delim) {
    int skip = 0;
    while (*str) {
        if (*str == delim) {
            if (!skip) {
                mx_printchar('\n');
                skip = 1;
            }
            str++;
            continue;
        }
        skip = 0;
        mx_printchar(*str);
        str++;
    }
    mx_printchar('\n');
}
