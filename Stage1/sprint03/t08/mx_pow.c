double mx_pow(double n, unsigned int pow) {
    if (!pow) {
        return 0;
    }

    double res = 1;

    for (int i = pow; i; i--) {
        res *= n;
    }

    return res;
}
