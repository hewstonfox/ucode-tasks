#include "../inc/arrays.h"

int *mx_copy_int_arr(const int *src, int size) {
    if (src == NULL || size < 0) return NULL;
    int *arr = (int *)malloc(size * 4);
    for (int i = 0; i < size; i++)
        arr[i] = src[i];
    return arr;
}

t_array *mx_generate_arr_range(int start, int stop) {
    t_array *range = (t_array *)malloc(8);
    range->size = stop - start;
    int *arr = (int *)malloc(range->size * 4);
    for (int i = 0; i < range->size; i++) arr[i] = i + start;
    range->data = arr;
    return range;
}

t_array *mx_del_dup_sarr(t_array *src) {
    if (src == NULL || src -> data == NULL) return NULL;
    int *safe_copy = mx_copy_int_arr(src->data, src->size);
    int pointer = 0;
    int current = 0;
    while (pointer < src -> size) {
        int skip = 0;
        for (int i = 0; i < pointer; i++) {
            if (safe_copy[pointer] == safe_copy[i]) {
                pointer++;
                skip = 1;
                break;
            }
        }
        if (skip)
            continue;
        safe_copy[current++] = safe_copy[pointer++];
    }
    t_array *result = (t_array*)malloc(16);
    free(src->data);
    free(src);
    result->data = mx_copy_int_arr(safe_copy, current);
    free(safe_copy);
    result->size = current;
    return result;
}

t_array *mx_create_empty_array() {
    t_array *array = (t_array*)malloc(4);
    array->data = (int*)malloc(0);
    array->size = 0;
    return array;
}

void mx_array_push(t_array *dst, int value) {
    int new_size = dst->size + 1;
    int * new_data = (int*)malloc(4 * new_size);
    for (int i = 0; i < dst->size; i++)
        new_data[i] = dst->data[i];
    new_data[dst->size] = value;

    free(dst->data);

    dst->data = new_data;
    dst->size = new_size;
}

void mx_free_array(t_array *arr) {
    free(arr->data);
    free(arr);
    arr = NULL;
}

void mx_array_extend(t_array *dst, t_array *src) {
    int new_size = dst->size + src->size;
    int * new_data = (int*)malloc(4 * new_size);
    int i = 0;
    for (; i < dst->size; i++)
        new_data[i] = dst->data[i];
    for (int j = 0; j < src->size; j++)
        new_data[i++] = src->data[j];

    free(dst->data);
    mx_free_array(src);

    dst->data = new_data;
    dst->size = new_size;
}
