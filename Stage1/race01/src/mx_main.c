#include "../inc/main.h"

int main(int argc, char const *argv[]) {
    char **args = mx_prepare_args(argv, argc--);
    mx_validate_args(args, argc);
    t_array *opd1 = mx_get_number_variants(args[0]);
    t_operation **op = mx_get_operations(*args[1]);
    t_array *opd2 = mx_get_number_variants(args[2]);
    t_array *res = mx_get_number_variants(args[3]);
    for (int i = 0; op[i]; i++)
        for (int j = 0; j < opd1->size; j++)
            for (int k = 0; k < opd2->size; k++)
                for (int l = 0; l < res->size; l++)
                    if (op[i]->func(opd1->data[j], opd2->data[k]) == res->data[l]) {
                        mx_printint(opd1->data[j]);
                        mx_printchar(' ');
                        mx_printchar(op[i]->sign);
                        mx_printchar(' ');
                        mx_printint(opd2->data[k]);
                        mx_printstr(" = ");
                        mx_printint(res->data[l]);
                        mx_printchar('\n');
                    }
    return 0;
}
