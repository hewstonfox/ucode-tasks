#include "../inc/variants.h"

bool mx_isnormal_num(const char *num) {
    if (*num == '-' || *num == '+') num++;
    while (*num) if (*num++ == '?') return false;
    return true;
}

bool mx_istotalundefind_num(const char *num) {
    while (*num) if (*num++ != '?') return false;
    return true; 
}

t_array *mx_get_number_variants(const char *num) {
    int len = mx_strlen(num);

    int sign = 1;
    if (*num == '-') {
        sign = -1;
        num++;
        len--;
    }
    if (*num == '+') {
        num++;
        len--;
    }
    
    while (*num == '0') {
        num++;
        len--;
    }
    

    int *defined_indexes = (int*)malloc(4 * (len + 1));
    int defined_len = 0;
    for (int i = 0; i < len; i++)
        if (num[i] != '?') defined_indexes[defined_len++] = i;

    long long max = 10;
    for (int i = 1; i < len; i++) max *= 10;

    long long min = 0;
    const char *tmp_num = num;
    while(*tmp_num == '?' || *tmp_num == '0') tmp_num++;
    if (mx_isdigit(*tmp_num)){
        min = *tmp_num - '0';
        tmp_num++;
        int min_len = mx_strlen(tmp_num);
        while (mx_isdigit(*tmp_num)) {
            min *= 10;
            min += *tmp_num - '0';
            tmp_num++;
            min_len--;
        }
        min *= mx_pow(10, min_len);
    }

    int *filtered = malloc(4 * max - min);
    int f_size = 0;
    for (int i = min; i < max; i++) {
        bool match = true;
        for (int j = 0; j < defined_len; j++) {
            int idx = defined_indexes[j];
            if (
                (i % mx_pow(10, len - idx)) / mx_pow(10, len - idx - 1) + '0' 
                != num[idx]
            ) {
                match = false;
                break;
            }
        }
        if (match)
            filtered[f_size++] = i * sign;
    }

    t_array *variants = (t_array *)malloc(16);

    variants->data = mx_copy_int_arr(filtered, f_size);
    variants->size = f_size;

    free(filtered);
    free(defined_indexes);

    return variants;
}
