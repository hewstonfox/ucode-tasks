#include "../inc/prints.h"

void mx_printchar(char c) {
    write(1, &c, 1);
}

void mx_printstr(const char *s) {
    write(1, s, mx_strlen(s));
}

void mx_printint(int n) {
    if (n < 0) {
        mx_printchar('-');
        n *= -1;
    }
    if (n < 10) mx_printchar(n + '0');
    else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}
