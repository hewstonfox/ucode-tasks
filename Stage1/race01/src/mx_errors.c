#include "../inc/errors.h"

void mx_printerr(const char *s) {
    write(STDERR_FILENO, s, mx_strlen(s));
}

void mx_throw_default_error(int error, const char* arg) {
    switch (error)
    {
    case INVALID_USAGE_ERROR:
        mx_printerr("usage: ./part_of_the_matrix [operand1] [operation] [operand2] [result]");
        break;
    case INVALID_OPERATION_ERROR:
        mx_printerr("Invalid operation: ");
        break;
    case INVALID_OPERAND_ERROR:
        mx_printerr("Invalid operand: ");
        break;
    case INVALID_RESULT_ERROR:
        mx_printerr("Invalid result: ");
        break;
    default:
        break;
    }
    mx_printerr(arg);
    mx_printerr("\n");
    exit(0);
}
