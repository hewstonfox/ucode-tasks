#include "../inc/chars.h"

bool mx_isspace(char c) {
    return (c >= '\t' && c <= '\r') || c == ' ';
}

bool mx_isdigit(char c) {
    return c >= '0' && c <= '9';
}
