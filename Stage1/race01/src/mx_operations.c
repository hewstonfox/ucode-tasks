#include "../inc/operations.h"

int mx_add(int a, int b) {
    return a + b;
}

int mx_sub(int a, int b) {
    return a - b;
}

int mx_mul(int a, int b) {
    return a * b;
}

int mx_div(int a, int b) {
    return a / b;
}

long long mx_pow(int a, int b) {
    if (b <= 0) return 1;
    long long res = a;
    while(--b > 0) res *= a;
    return res;
}

t_operation *mx_create_operation(char sign, t_operation_func func) {
    t_operation *op = (t_operation*)malloc(16);
    op->sign = sign;
    op->func = func;
    return op;
}

t_operation **mx_get_operations(char operation) {
    if (operation == '?') {
        t_operation **operations = (t_operation**)malloc(16 * 5);
        operations[0] = mx_create_operation('+', &mx_add);
        operations[1] = mx_create_operation('-', &mx_sub);
        operations[2] = mx_create_operation('*', &mx_mul);
        operations[3] = mx_create_operation('/', &mx_div);
        operations[4] = NULL;
        return operations;
    }
    t_operation **operations = (t_operation**)malloc(16 * 2);
    switch (operation)
    {
        case '+':
            operations[0] = mx_create_operation(operation, &mx_add);
            break;
        case '-':
            operations[0] = mx_create_operation(operation, &mx_sub);
            break;
        case '*':
            operations[0] = mx_create_operation(operation, &mx_mul);
            break;
        case '/':
            operations[0] = mx_create_operation(operation, &mx_div);
            break;
    }
    operations[1] = NULL;
    return operations;
}
