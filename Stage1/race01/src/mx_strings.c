#include "../inc/strings.h"

int mx_strlen(const char *s) {
    int count;
    for (count = 0; s[count]; count++);
    return count;
}

char *mx_strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src) *dst++ = *src++;
    return orig;
}

char *mx_strncpy(char *dst, const char *src, int len) {
    char *orig = dst;
    for (int i = 0; i != len && *src; i++) *dst++ = *src++;
    *dst = '\0';
    return orig;
}

char *mx_strnew(const int size) {
    if (size < 0) return NULL;
    char *str = malloc(size + 1);
    for (int i = 0; i <= size; i++) str[i] = '\0';
    return str;
}

char *mx_strtrim(const char *str) {
    if (str == NULL) return NULL;
    while (mx_isspace(*str)) str++;
    int len = mx_strlen(str);
    while (mx_isspace(str[len - 1])) len--;
    return mx_strncpy(mx_strnew(len), str, len);
}

char *mx_strdup(const char *str) {
    char *dup = mx_strnew(mx_strlen(str));
    mx_strcpy(dup, str);
    return dup;
}

char *mx_replace_char(const char *s, int index, char symbol) {
    char *result = mx_strdup(s);
    result[index] = symbol;
    return result;
}
