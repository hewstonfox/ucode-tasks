#include "../inc/validations.h"

bool mx_isvalid_operation(const char *operation) {
    return mx_strlen(operation) == 1 && (
        *operation == '+' ||
        *operation == '-' ||
        *operation == '*' ||
        *operation == '/' ||
        *operation == '?'
    );
}

bool mx_isvalid_operand(const char *operand) {
    if (*operand == '+' || *operand == '-') operand++;
    while (*operand) {
        if (!mx_isdigit(*operand) && *operand != '?') return false;
        operand++;
    }
    return true;
}

void mx_validate_args(char** args, int size) {
    if (size != 4) 
        mx_throw_default_error(INVALID_USAGE_ERROR, "");
    if (!mx_isvalid_operation(args[1])) 
        mx_throw_default_error(INVALID_OPERATION_ERROR, args[1]);
    if (!mx_isvalid_operand(args[0]))
        mx_throw_default_error(INVALID_OPERAND_ERROR, args[0]);
    if (!mx_isvalid_operand(args[2]))
        mx_throw_default_error(INVALID_OPERAND_ERROR, args[2]);
    if (!mx_isvalid_operand(args[3]))
        mx_throw_default_error(INVALID_RESULT_ERROR, args[3]);
}
