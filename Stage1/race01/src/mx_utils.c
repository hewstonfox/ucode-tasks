#include "../inc/utils.h"

char **mx_prepare_args(const char** args, int size) {
    char **new_args = (char**)malloc(size - 1);
    for (int i = 1; i < size; i++)
        new_args[i - 1] = mx_strtrim(args[i]);
    return new_args;
}
