#pragma once

#include <stdbool.h>

#include "strings.h"

#include "errors.h"

char **mx_prepare_args(const char** args, int size);
