#pragma once

#include <stdlib.h>

#include "chars.h"

int mx_strlen(const char *s);

char *mx_strcpy(char *dst, const char *src);

char *mx_strncpy(char *dst, const char *src, int len);

char *mx_strnew(const int size);

char *mx_strtrim(const char *str);

char *mx_strdup(const char *str);

char *mx_replace_char(const char *s, int index, char symbol);
