#pragma once

#include "strings.h"

#include "atoi.h"

#include "arrays.h"

#include "operations.h"

bool mx_isnormal_num(const char *num);

bool mx_istotalundefind_num(const char *num);

t_array *mx_get_number_variants(const char *num);
