#pragma once

#include <unistd.h>

#include <stdio.h>

#include <stdlib.h>

#include "strings.h"

enum e_error {
    INVALID_USAGE_ERROR,
    INVALID_OPERATION_ERROR,
    INVALID_OPERAND_ERROR,
    INVALID_RESULT_ERROR
};

void mx_throw_default_error(int error, const char* arg);

void mx_printerr(const char *s);
