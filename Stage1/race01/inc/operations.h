#pragma once

#include <stdlib.h>

int mx_add(int a, int b);

int mx_sub(int a, int b);

int mx_mul(int a, int b);

int mx_div(int a, int b);

long long mx_pow(int a, int b);

typedef int (*t_operation_func)(int, int);

typedef struct s_operation {
    char sign;
    t_operation_func func;
} t_operation;


t_operation **mx_get_operations(char operation);
