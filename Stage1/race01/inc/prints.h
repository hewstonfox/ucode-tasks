#pragma once

#include <unistd.h>

#include "strings.h"

void mx_printchar(char c);

void mx_printstr(const char *s);

void mx_printint(int n);
