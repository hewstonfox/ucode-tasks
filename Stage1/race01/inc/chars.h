#pragma once

#include <stdbool.h>

bool mx_isdigit(char c);

bool mx_isspace(char c);
