#pragma once

#include <stdlib.h>

typedef struct s_array {
    int * data;
    int size;
} t_array;

int *mx_copy_int_arr(const int *src, int size);

t_array *mx_del_dup_sarr(t_array *src);

t_array *mx_generate_arr_range(int start, int stop);

t_array *mx_create_empty_array();

void mx_array_push(t_array *dst, int value);

void mx_free_array(t_array *arr);

void mx_array_extend(t_array *dst, t_array *src);
