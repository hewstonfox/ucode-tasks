#pragma once

#include <stdbool.h>

#include "strings.h"

#include "errors.h"

bool mx_isvalid_operation(const char *operation);

bool mx_isvalid_operand(const char *operand);

void mx_validate_args(char** args, int size);
