#include "../inc/drawing.h"

void mx_draw_frame(t_drop **drops, WINDOW *scr) {
  for (unsigned int i = 0; i <= (unsigned int)scr->_maxx; i++)
    mx_draw_drop(i, drops[i], scr);
}

void mx_draw_drop(unsigned int i, t_drop *drop, WINDOW *scr) {
  int color = 1;
  for (unsigned int j = 0; j <= (unsigned int)scr->_maxy; j++) {
    color = mx_is_white_char(drop, j) ? 2 : 1;
    attron(COLOR_PAIR(color));
    wmove(scr, j, i);
    printw("%C", mx_get_drop_drow_char(drop, j));
    attroff(COLOR_PAIR(color));
  }
}

void mx_run_rain(t_drop **drops, WINDOW *scr) {
  while (true) {
    mx_process_frame_drops();
    mx_draw_frame(drops, scr);
    refresh();
    if (mx_process_key() < 0) return;
    napms(100);
  }
}
