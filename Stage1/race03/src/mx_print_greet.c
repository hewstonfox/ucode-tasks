#include "../inc/keymanager.h"
#include "../inc/main.h"

void mx_print_greet(void) {
  if (has_colors()) {
    char *sentences[] = {
      "Wake up , Neo..", 
      "Use <C> to toggle colors.",
      "The Matrix has you..", 
      "Follow the white rabbit",
      "Knock, knock, Neo"
    };

    char *whitespaces = "\n\n\n      ";

    char *str;
    for (int line = 0; line < 5; line++) {
      str = sentences[line];
      printw("%s", whitespaces);
      for (int i = 0; i < mx_strlen(str); i++) {
        attrset(COLOR_PAIR(1));
        if (str[i] != ' ') beep();
        printw("%c", str[i]);
        refresh();
        attroff(COLOR_PAIR(1));
        if (mx_process_key() < 0) return;
        napms(mx_get_rand_int(50, 300));
      }
      clear();
      napms(700);
    }
  }
}
