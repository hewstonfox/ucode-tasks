#include "../inc/randoms.h"

void mx_set_random() {
  srand((unsigned)time(0));
}

int mx_get_rand_int(int start, int stop) {
  return (rand() % (stop - start + 1)) + start;
}

// wchar_t *SYMBOLS = L"КудаидёммысПятачком-Большой-большойсекрет!ИнерасскажеммыонёмОнет,инет,инет!КудаидёммысПятачком-Большой-большойсекрет!ИнерасскажеммыонёмОнет,инет,и...-Да?..-Нет!";
wchar_t *SYMBOLS = L"ｷﾄｺEﾋ=ｲｿﾔ:5Aｵｽ082ﾅﾃﾐ-ﾊﾂﾘR1ﾍﾝﾎ3ﾇHｼﾚﾈ<ｰｱﾆｹｾ9ﾑﾒﾌﾁﾖﾛｸﾘ4ｦ>ｳﾗ.TﾓﾕｻT\"IMｶ+*ﾏｴXﾀç7ﾜﾙ¦ﾉ";

int mx_get_rand_wcahr() {
  // don`t forget to change size after changing SYMBOLS set
  return SYMBOLS[mx_get_rand_int(0, 77)];
}

int *mx_get_random_chars(unsigned int size) {
  int *arr = (int *)malloc(4 * size);
  for (unsigned int i = 0; i < size; i++) arr[i] = mx_get_rand_wcahr();
  return arr;
}
