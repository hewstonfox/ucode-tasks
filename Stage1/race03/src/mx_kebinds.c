#include "../inc/keymanager.h"

static int TEXT_COLOR = 1;

int mx_process_key() {
  int key = getch();
  if (key == 'q') return -1;
  if (key == 'c') {
    init_pair(1, TEXT_COLOR ? COLOR_RED : COLOR_GREEN, COLOR_BLACK);
    TEXT_COLOR = !TEXT_COLOR;
  };
  return 1;
}
