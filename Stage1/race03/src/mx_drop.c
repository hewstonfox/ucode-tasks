#include "../inc/drop.h"

static unsigned int DROP_SIZE = 0;
static unsigned int DROP_COUNT = 0;
static t_drop **DROPS = NULL;

t_drop *mx_create_drop() {
  t_drop *drop = (t_drop *)malloc(8);
  drop->from = 0;
  drop->to = 0;
  drop->symbols = mx_get_random_chars(DROP_SIZE);
  return drop;
}

void mx_move_drop(t_drop *drop) {
  if (drop->from != drop->to && mx_get_rand_int(-2, 1) > 0)
    drop->from = drop->from + 1 == DROP_SIZE ? 0 : drop->from + 1;
  if (mx_get_rand_int(-2, 4) > 0)
    drop->to = drop->to + 1 == DROP_SIZE ? 0 : drop->to + 1;
  if (drop->to == drop->from) drop->from = 0;
}

void mx_replace_drop_char(t_drop *drop) {
  int choise = mx_get_rand_int(0, DROP_SIZE);
  drop->symbols[choise] = mx_get_rand_wcahr();
}

int mx_get_drop_drow_char(t_drop *drop, unsigned int index) {
  if (drop->from > drop->to) {
    if (index <= drop->to || (index >= drop->from && index < DROP_SIZE))
      return drop->symbols[index];
    return L' ';
  } else {
    if (index >= drop->from && index <= drop->to) return drop->symbols[index];
    return L' ';
  }
}

bool mx_is_white_char(t_drop *drop, unsigned int index) {
  return index == drop->to;
}

t_drop **mx_init_dtops(unsigned int height, unsigned int width) {
  DROP_SIZE = height;
  DROP_COUNT = width;
  DROPS = (t_drop **)malloc(8 * DROP_COUNT);
  for (unsigned int i = 0; i < DROP_COUNT; i++) DROPS[i] = mx_create_drop();
  return DROPS;
}

void mx_clear_drops() {
  t_drop *tmp = NULL;
  for (unsigned int i = 0; i < DROP_COUNT; i++) {
    tmp = DROPS[i];
    DROPS[i] = NULL;
    if (!tmp) continue;
    free(tmp->symbols);
    free(tmp);
  }
  if (DROPS) free(DROPS);
}

void mx_process_frame_drops() {
  for (unsigned int i = 0; i < DROP_COUNT; i++) {
    mx_replace_drop_char(DROPS[i]);
    mx_move_drop(DROPS[mx_get_rand_int(0, DROP_COUNT - 1)]);
  }
}
