#include "../inc/main.h"

int main() {
  mx_set_random();
  setlocale(LC_ALL, "");

  WINDOW *window = initscr();
  scrollok(stdscr, TRUE);
  nodelay(stdscr, TRUE);
  noecho();
  
  start_color();
  init_pair(1, COLOR_GREEN, COLOR_BLACK);
  init_pair(2, COLOR_WHITE, COLOR_BLACK);
  attron(COLOR_PAIR(1));

  t_drop **drops = mx_init_dtops(window->_maxy + 1, window->_maxx + 1);

  mx_print_greet();

  mx_run_rain(drops, window);

  endwin();
  mx_clear_drops();
  return 0;
}
