#include "../inc/main.h"

int mx_strlen(const char *s) {
  int length = 0;
  int i = 0;
  while (s[i] != '\0') {
    length++;
    i++;
  }
  return length;
}
