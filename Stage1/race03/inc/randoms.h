#pragma once

#include <time.h>
#include <stdlib.h>
#include <wchar.h>

void mx_set_random();

int mx_get_rand_int(int start, int stop);

int mx_get_rand_wcahr();

int *mx_get_random_chars(unsigned int size);
