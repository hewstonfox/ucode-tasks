#pragma once

#include <locale.h>
#include <unistd.h>
#include <ncurses.h>

#include "randoms.h"
#include "drop.h"
#include "drawing.h"

int mx_strlen(const char *s);
void mx_print_greet(void);
