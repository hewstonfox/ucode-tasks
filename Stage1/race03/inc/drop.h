#pragma once

#include <stdlib.h>
#include <stdbool.h>

#include "randoms.h"

typedef struct s_drop {
  int *symbols;
  unsigned int from;
  unsigned int to;
} t_drop;

t_drop *mx_create_drop();

void mx_move_drop(t_drop *drop);

void mx_replace_drop_char(t_drop *drop);

int mx_get_drop_drow_char(t_drop *drop, unsigned int index);

bool mx_is_white_char(t_drop *drop, unsigned int index);

bool mx_is_white_char(t_drop *drop, unsigned int index);

t_drop **mx_init_dtops(unsigned int height, unsigned int width);

void mx_clear_drops();

void mx_process_frame_drops();
