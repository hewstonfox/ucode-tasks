#pragma once

#include <ncurses.h>

#include "drop.h"
#include "keymanager.h"

void mx_draw_frame(t_drop **drops, WINDOW *scr);

void mx_draw_drop(unsigned int i, t_drop *drop, WINDOW *scr);

void mx_run_rain(t_drop **drops, WINDOW *scr);
