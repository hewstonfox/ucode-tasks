#pragma once

#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

void mx_printchar(char c);

void mx_printint(int n);

bool mx_isspace(int c);

int mx_strlen(const char *s);

void mx_printstr(const char *s);

void mx_printerr(const char *s);
