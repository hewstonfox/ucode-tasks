#include "../inc/main.h"

bool mx_isspace(int c) {
    return (c >= '\t' && c <= '\r') || c == ' ';
}
