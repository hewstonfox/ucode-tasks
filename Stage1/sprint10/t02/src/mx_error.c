#include "../inc/errors.h"

void mx_printerr(const char *s) { write(STDERR_FILENO, s, mx_strlen(s)); }

void mx_throwstdwerr(int error) {
  switch (error) {
    case INVALID_USAGE_ERROR:
      mx_printerr("usage: ./mx_cp [source_file] [destination_file]\n");
      break;
    default:
      mx_printerr("error\n");
  }
  exit(-1);
}

char *mx_getsyserr() { return strerror(errno); }

void mx_throwsystemerr(const char * arg) {
  mx_printerr("mx_cat: ");
  mx_printerr(arg);
  mx_printerr(": ");
  mx_printerr(mx_getsyserr());
  mx_printerr("\n");
  exit(-1);
}
