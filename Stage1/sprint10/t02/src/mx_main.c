#include "../inc/main.h"

int main(int argc, char const *argv[]) {
  if (argc < 2) {
    char buff[1];
    while (read(0, buff, 1)) write(1, buff, 1);
    return 0;
  }

  mx_print_file(argv[1]);

  return 0;
}
