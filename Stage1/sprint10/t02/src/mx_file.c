#include "../inc/file.h"

void mx_print_file(const char *fname) {
    int fn = open(fname, O_RDONLY);
    if (fn == -1) {
        mx_throwsystemerr(fname);
        return;
    };
    char buff[1];
    while(read(fn, buff, 1)) write(1, buff, 1);
    write(1, "\n", 1);
    close(fn);
}
