#pragma once

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "errors.h"

void mx_print_file(const char *fname);
