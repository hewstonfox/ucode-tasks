#pragma once

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "strings.h"

enum e_error {
  UNDEFINED_ERROR,
  INVALID_USAGE_ERROR,
};

void mx_printerr(const char *s);

void mx_throwstdwerr(int error);

char *mx_getsyserr();

void mx_throwsystemerr(const char *arg);
