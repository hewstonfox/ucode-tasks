#pragma once

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int mx_strlen(const char *s);

char *mx_strcpy(char *dst, const char *src);

char *mx_strcat(char *s1, const char *s2);

char *mx_strnew(const int size);

char *mx_strdup(const char *str);

char *mx_file_to_str(const char *filename);
