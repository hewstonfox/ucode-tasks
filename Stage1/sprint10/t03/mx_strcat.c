#include "file_to_str.h"

char *mx_strcat(char *s1, const char *s2) {
    for (char *i = s1 + mx_strlen(s1); *s2; i++) {
        *i = *s2;
        s2++;
    }
    return s1;
}
