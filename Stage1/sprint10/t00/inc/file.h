#pragma once

#include <fcntl.h>
#include <unistd.h>

#include "errors.h"
#include "prints.h"

int mx_filesize(const char *fname);

void mx_print_file(const char *fname);
