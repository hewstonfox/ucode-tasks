#pragma once

#include <unistd.h>

#include <stdio.h>

#include "strings.h"

enum e_error {
    UNDEFINED_ERROR,
    INVALID_USAGE_ERROR,
};

void mx_printerr(const char *s);

void mx_printstderr(int error);
