#include "../inc/main.h"

int main(int argc, char const *argv[])
{
    if (argc != 2) {
        mx_printstderr(INVALID_USAGE_ERROR);
        return -1;
    }

    mx_print_file(argv[1]);

    return 0;
}
