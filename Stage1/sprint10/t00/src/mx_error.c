#include "../inc/errors.h"

void mx_printerr(const char *s) {
    write(STDERR_FILENO, s, mx_strlen(s));
}

void mx_printstderr(int error) {
    switch (error)
    {
    case INVALID_USAGE_ERROR:
        mx_printerr("usage: ./read_file [file_path]\n");
        break;
    default:
        mx_printerr("error\n");
    }
}
