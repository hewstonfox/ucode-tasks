#include "../inc/file.h"

void mx_print_file(const char *fname) {
    int fn = open(fname, O_RDONLY);
    if (fn == -1) {
        mx_printstderr(UNDEFINED_ERROR);
        return;
    };
    char buff[1];
    while(read(fn, buff, 1)) mx_printchar(buff[0]);
    mx_printchar('\n');
    close(fn);
}
