#include "../inc/main.h"

int main(int argc, char const *argv[]) {
  if (argc != 3) mx_throwstdwerr(INVALID_USAGE_ERROR);
  mx_copy_file(argv[1], argv[2]);
  return 0;
}
