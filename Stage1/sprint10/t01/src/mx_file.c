#include "../inc/file.h"

void mx_copy_file(const char *src, const char *dst) {
    int dst_exist = open(dst, O_RDONLY);
    if (dst_exist != -1) {
        close(dst_exist);
        return;
    };
    int src_f = open(src, O_RDONLY);
    if (src_f == -1) mx_throwsystemerr(src);
    int dst_f = open(dst, O_WRONLY | O_CREAT, 0777);
    if (dst_f == -1) mx_throwsystemerr(dst);
    char buff[1];
    while (read(src_f, buff, 1)) write(dst_f, buff, 1);
    close(dst_f);
    close(src_f);
}
