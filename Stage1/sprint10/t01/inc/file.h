#pragma once

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "errors.h"

void mx_copy_file(const char *src, const char *dst);
