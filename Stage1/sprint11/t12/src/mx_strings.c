#include "../inc/strings.h"

int mx_strlen(const char *s) {
  int count;
  for (count = 0; s[count]; count++)
    ;
  return count;
}

bool mx_isspace(char c) {
    return (c >= '\t' && c <= '\r') || c == ' ';
}

bool mx_isdigit(int c) {
    return c >= '0' && c <= '9';
}

void mx_printstr(const char *s) {
    write(1, s, mx_strlen(s));
}

void mx_printchar(char c) {
    write(1, &c, 1);
}

void mx_printint(int n) {
    if (n == -2147483648) {
        write(1, "-2147483648", 11);
        return;
    }
    if (n < 0) {
        write(1, "-", 1);
        n *= -1;
    }
    if (n < 10) {
        mx_printchar(n + '0');
    } else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}

void mx_strdel(char **str) {
  free(*str);
  *str = NULL;
}

int mx_tolower(int c) {
    return c >= 'A' && c <= 'Z' ? c + ' ' : c;
}

int mx_strcmp(const char *s1, const char *s2) {
    while (*s1 && *s1 == *s2) {
        s1++;
        s2++;
    }
    return *s1 - *s2;
}

int mx_strcmp_unregister(const char *s1, const char *s2) {
    while (*s1 && mx_tolower(*s1) == mx_tolower(*s2)) {
        s1++;
        s2++;
    }
    return mx_tolower(*s1) - mx_tolower(*s2);
}

char *mx_strncpy(char *dst, const char *src, int len) {
  char *init_dst = dst;
  for (int i = 0; i != len && *src; i++) *dst++ = *src++;
  *dst = '\0';
  return init_dst;
}

char *mx_strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src) *dst++ = *src++;
    return orig;
}

char *mx_strnew(const int size) {
  if (size < 0) return NULL;
  char *str = malloc(size + 1);
  for (int i = 0; i <= size; i++) str[i] = '\0';
  return str;
}

int mx_count_words(const char *str, char delimiter) {
  while (*str == delimiter) str++;
  if (!*str) return 0;
  int count = 0;
  int skip = 0;
  while (*str) {
    char c = *str++;
    if (c == delimiter && skip) continue;
    if (c == delimiter) {
      skip = 1;
      count++;
      continue;
    }
    skip = 0;
  }
  if (*(str - 1) != delimiter) count++;
  return count;
}

char *mx_strcat(char *s1, const char *s2) {
  for (char *i = s1 + mx_strlen(s1); *s2; i++) {
    *i = *s2;
    s2++;
  }
  return s1;
}

char *mx_strdup(const char *str) {
    char *dup = mx_strnew(mx_strlen(str));
    mx_strcpy(dup, str);
    return dup;
}

char *mx_strjoin(char const *s1, char const *s2) {
  if (s1 == NULL && s2 == NULL) return NULL;
  if (s1 == NULL) return mx_strdup(s2);
  if (s2 == NULL) return mx_strdup(s1);
  char *str = mx_strnew(mx_strlen(s1) + mx_strlen(s2) - 1);
  mx_strcat(str, s1);
  mx_strcat(str, s2);
  return str;
}

char **mx_strsplit(char const *s, char c) {
  if (s == NULL) return NULL;
  char **words = malloc((mx_count_words(s, c) + 1) * 8);
  int skip = 0;
  int char_count = 0;
  int word_idx = 0;
  while (*s == c) s++;
  while (*s) {
    if (*s++ == c) {
      if (!skip) {
        words[word_idx++] =
            mx_strncpy(mx_strnew(char_count), s - char_count - 1, char_count);
        skip = 1;
        char_count = 0;
      }
      continue;
    }
    skip = 0;
    char_count++;
    if (*s == '\0')
      words[word_idx++] = mx_strncpy(mx_strnew(char_count), s - char_count, char_count);
  }
  words[word_idx] = NULL;
  return words;
}

int mx_atoi(const char *str) {
    int sign = 1;
    long long result;
    while (mx_isspace(*str)) mx_throwerr();
    while (*str == '0') str++;
    if (!mx_isdigit(*str)) mx_throwerr();
    result = *str - '0';
    str++;
    while (mx_isdigit(*str)) {
        long long temp = result;
        result *= 10;
        result += *str - '0';
        if (result <= temp) return result * sign >= 0 ? 0 : -1;
        str++;
    }
    return (int)(result * sign);
}
