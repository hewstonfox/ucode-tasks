#include "../inc/main.h"

int main(int argc, char const *argv[]) {
  if (argc < 3) mx_throwusage();
  const char *filename = argv[1];
  const char *command = argv[2];
  t_list *list = mx_parce_songs(mx_readfile(filename));

  if (!mx_strcmp(command, "print")) {
    if (argc > 3) mx_throwerr();
    mx_print_song_list(list);
  } else if (!mx_strcmp(command, "add")) {
    if (argc != 5) mx_throwerr();
    mx_add_song(&list, argv[3], argv[4]);
  } else if (!mx_strcmp(command, "remove")) {
    if (argc != 4) mx_throwerr();
    int index = mx_atoi(argv[3]);
    mx_del_node_if(&list, &index, &mx_del_filter);
  } else if (!mx_strcmp(command, "sort")) {
    if (argc != 4) mx_throwerr();
    if (!mx_strcmp(argv[3], "name")) mx_sort_list(list, &mx_name_cmp);
    else if (!mx_strcmp(argv[3], "artist")) mx_sort_list(list, &mx_artist_cmp);
    else mx_throwerr();
  } else
    mx_throwerr();

  if (mx_strcmp(command, "print")){
    mx_writefile(filename, mx_song_list_to_str(list));
  }

  return 0;
}
