#include "../inc/song.h"

bool mx_artist_cmp(void *a, void *b) {
  return mx_strcmp_unregister((const char *)((t_song *)a)->artist,
                   (const char *)((t_song *)b)->artist) > 0;
}

bool mx_name_cmp(void *a, void *b) {
  return mx_strcmp_unregister((const char *)((t_song *)a)->name,
                   (const char *)((t_song *)b)->name) > 0;
}

bool mx_del_filter(void *a, void *index) {
  return ((t_song *)a)->index == *(int *)index;
}

void mx_print_song(t_song *song) {
  mx_printint(song->index);
  write(1, ". ", 2);
  mx_printstr(song->artist);
  write(1, " - ", 3);
  mx_printstr(song->name);
  write(1, "\n", 1);
}

void mx_print_song_node(t_list *node) {
  mx_print_song((t_song *)node->data);
}

void mx_print_song_list(t_list *list) {
  mx_foreach_list(list, &mx_print_song_node);
}

t_song *mx_create_song(int index, const char *name, const char *artist) {
  t_song *song = (t_song *)malloc(24);
  song->index = index;
  song->name = mx_strdup(name);
  song->artist = mx_strdup(artist);
  return song;
}

t_list *mx_parce_songs(const char *data) {
  char **str_songs = mx_strsplit(data, '\n');
  if (!*str_songs) return NULL;
  t_list *songs = mx_create_node(NULL);
  t_list *cur = songs;
  char **song_params = NULL;
  for (int i = 0; str_songs[i]; i++) {
    song_params = mx_strsplit((const char *)str_songs[i], ',');
    if (!song_params[0] || !song_params[1]) continue;
    cur->data = mx_create_song(i, song_params[1], song_params[0]);
    if (str_songs[i + 1]) cur->next = mx_create_node(NULL);
    cur = cur->next;
  }
  return songs;
}

char *mx_song_to_str(t_song *song) {
  return mx_strjoin(song->artist, mx_strjoin(",", mx_strjoin(song->name, "\n")));
}

char *mx_song_list_to_str(t_list *songs) {
  char * str = "";
  while (songs) {
    str = mx_strjoin(str, mx_song_to_str(songs->data));
    songs = songs->next;
  }
  return str;
}

void mx_add_song(t_list **list, const char *artist, const char *name) {
  t_song * s = mx_create_song(mx_list_size(*list), name, artist);
  mx_push_back(list, s);
}
