#include "../inc/list.h"

t_list* mx_create_node(void* data) {
    t_list *node = (t_list*)malloc(16);
    node->data = data;
    node->next = NULL;
    return node;
}

void mx_push_back(t_list** list, void* data) {
  if (!*list) {
    *list = mx_create_node(data);
    return;
  }
  t_list *cur = *list;
  for (; cur->next; cur = cur->next);
  cur->next = mx_create_node(data);
}

void mx_push_front(t_list** list, void* data) {
  if (!*list) {
    *list = mx_create_node(data);
    return;
  }
  t_list* node = mx_create_node(data);
  node->next = *list;
  *list = node;
}

void mx_push_index(t_list** list, void* data, int index){
  if (index <= 0) {
    mx_push_front(list, data);
    return;
  }
  t_list *cur = *list;
  for (int i = 1; i <= index && cur->next; i++) {
    if (i == index) {
      t_list *node = mx_create_node(data);
      node->next = cur->next;
      cur->next = node;
      return;
    }
    cur = cur->next;
  }
  mx_push_back(&cur, data);
}

void mx_pop_front(t_list** list) {
  if (!*list) return;
  t_list* cur = *list;
  *list = cur->next;
  free(cur);
  cur->next = NULL;
  cur = NULL;
}

void mx_pop_back(t_list** list) {
  if (!*list) return;
  t_list* cur = *list;
  if (!cur->next) {
    *list = NULL;
    free(cur);
    return;
  }
  for (; cur->next->next; cur = cur->next)
    ;
  free(cur->next);
  cur->next = NULL;
  cur = NULL;
}

void mx_pop_index(t_list**list, int index) {
  if (index <= 0) {
    mx_pop_front(list);
    return;
  }
  t_list *cur = *list;
  for (int i = 1; i <= index && cur->next; i++) {
    if (i == index) {
      t_list *to_delete = cur->next;
      cur->next = to_delete->next;
      free(to_delete);
      return;
    }
    cur = cur->next;
  }
  mx_pop_back(list);
}

int mx_list_size(t_list* list) {
  int count = 0;
  for (; list; list = list->next) count++;
  return count;
}

void mx_foreach_list(t_list* list, void (*f)(t_list* node)) {
  if (!f) return;
  for (t_list *cur = list; cur; cur = cur->next) f(cur);
}

void mx_del_node_if(t_list** list, void* del_data,
                    bool (*cmp)(void* a, void* b)) {
  if (cmp == NULL || *list == NULL || del_data == NULL) return;
  t_list* to_del;
  while (*list && cmp((*list)->data, del_data)) {
    to_del = *list;
    *list = (*list)->next;
    free(to_del);
  }
  if (!*list) return;
  for (t_list* cur = *list; cur->next;) {
    if (!cmp(cur->next->data, del_data)) {
      cur = cur->next;
      continue;
    }
    to_del = cur->next;
    cur->next = to_del->next;
    free(to_del);
  }
}

t_list* mx_sort_list(t_list* list, bool (*cmp)(void* a, void* b)) {
  if (cmp == NULL || list == NULL) return list;
  for (t_list* i = list; i; i = i->next)
    for (t_list* j = list; j->next; j = j->next)
      if (cmp(j->data, j->next->data)) {
        void* data = j->data;
        j->data = j->next->data;
        j->next->data = data;
      }
  return list;
}

