#include "../inc/files.h"

int mx_filesize(const char *fname) {
  if (!mx_strlen(fname)) return -1;
  int size = 0;
  int fn = open(fname, O_RDONLY);
  if (fn == -1) return -1;
  char buff[1];
  while (read(fn, buff, 1)) size++;
  close(fn);
  return size;
}

char *mx_readfile(const char *filename) {
  if (!mx_strlen(filename)) return NULL;
  int size = mx_filesize(filename);
  if (size < 0) return "";
  int fn = open(filename, O_RDONLY);
  char *text = (char *)malloc(size + 1);
  text[size] = '\0';
  read(fn, text, size);
  close(fn);
  return text;
}

void mx_writefile(const char *filename, const char *data) {
  int dst_f = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0777);
  if (dst_f == -1) mx_throwerr();
  write(dst_f, data, mx_strlen(data));
  close(dst_f);
}
