#include "../inc/errors.h"

void mx_printerr(const char *s) { write(2, s, mx_strlen(s)); }

void mx_throwusage() {
  mx_printerr("usage: ./playlist [file] [command] [args]\n");
  exit(0);
}

void mx_throwerr() {
  mx_printerr("ERROR\n");
  exit(-1);
}
