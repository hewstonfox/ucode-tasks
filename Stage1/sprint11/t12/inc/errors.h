#pragma once

#include <unistd.h>

#include "strings.h"

void mx_printerr(const char *s);

void mx_throwusage();

void mx_throwerr();
