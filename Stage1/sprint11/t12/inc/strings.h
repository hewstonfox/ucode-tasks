#pragma once

#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include "errors.h"

int mx_strlen(const char *s);

bool mx_isspace(char c);

bool mx_isdigit(int c);

void mx_printstr(const char *s);

void mx_printchar(char c);

void mx_printint(int n);

int mx_strcmp_unregister(const char *s1, const char *s2);

int mx_strcmp(const char *s1, const char *s2);

char *mx_strncpy(char *dst, const char *src, int len);

char *mx_strcpy(char *dst, const char *src);

char *mx_strnew(const int size);

int mx_count_words(const char *str, char delimiter);

char *mx_strcat(char *s1, const char *s2);

char *mx_strdup(const char *str);

char *mx_strjoin(char const *s1, char const *s2);

char **mx_strsplit(char const *s, char c);

int mx_atoi(const char *str);
