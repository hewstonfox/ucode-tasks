#pragma once

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include "strings.h"
#include "errors.h"

char *mx_readfile(const char *filename);

void mx_writefile(const char *filename, const char *data);
