#pragma once

#include <stdbool.h>

#include "strings.h"
#include "list.h"

typedef struct s_song {
  int index;
  char *name;
  char *artist;
} t_song;

bool mx_artist_cmp(void *a, void *b);

bool mx_name_cmp(void *a, void *b);

bool mx_del_filter(void *a, void *index);

void mx_print_song(t_song *song);

void mx_print_song_node(t_list *node);

void mx_print_song_list(t_list *list);

t_song *mx_create_song(int index, const char *name, const char *artist);

t_list *mx_parce_songs(const char *data);

char *mx_song_to_str(t_song *song);

char *mx_song_list_to_str(t_list *songs);

void mx_add_song(t_list **list, const char *artist, const char *name);
