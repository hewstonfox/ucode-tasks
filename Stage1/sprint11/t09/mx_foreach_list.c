#include "list.h"

void mx_foreach_list(t_list* list, void (*f)(t_list* node)) {
  if (!f) return;
  for (t_list *cur = list; cur; cur = cur->next) f(cur);
}
