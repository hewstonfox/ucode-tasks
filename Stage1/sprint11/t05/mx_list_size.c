#include "list.h"

int mx_list_size(t_list* list) {
  int count = 0;
  for (; list; list = list->next) count++;
  return count;
}
