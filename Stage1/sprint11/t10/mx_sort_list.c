#include "list.h"

t_list* mx_sort_list(t_list* list, bool (*cmp)(void* a, void* b)) {
  if (cmp == NULL || list == NULL) return list;
  for (t_list* i = list; i; i = i->next)
    for (t_list* j = list; j->next; j = j->next)
      if (cmp(j->data, j->next->data)) {
        void* data = j->data;
        j->data = j->next->data;
        j->next->data = data;
      }
  return list;
}
