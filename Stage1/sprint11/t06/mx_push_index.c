#include "list.h"

void mx_push_index(t_list** list, void* data, int index){
  if (index <= 0) {
    mx_push_front(list, data);
    return;
  }
  t_list *cur = *list;
  for (int i = 1; i <= index && cur->next; i++) {
    if (i == index) {
      t_list *node = mx_create_node(data);
      node->next = cur->next;
      cur->next = node;
      return;
    }
    cur = cur->next;
  }
  mx_push_back(&cur, data);
}
