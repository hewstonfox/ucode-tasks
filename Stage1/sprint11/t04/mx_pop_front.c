#include "list.h"

void mx_pop_front(t_list** list) {
  if (!*list) return;
  t_list* cur = *list;
  *list = cur->next;
  free(cur);
  cur->next = NULL;
  cur = NULL;
}
