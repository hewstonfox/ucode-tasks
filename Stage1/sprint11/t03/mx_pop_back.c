#include "list.h"

void mx_pop_back(t_list** list) {
  if (!*list) return;
  t_list* cur = *list;
  if (!cur->next) {
    *list = NULL;
    free(cur);
    return;
  }
  for (; cur->next->next; cur = cur->next)
    ;
  free(cur->next);
  cur->next = NULL;
  cur = NULL;
}
