#include "list.h"

void mx_del_node_if(t_list** list, void* del_data,
                    bool (*cmp)(void* a, void* b)) {
  if (cmp == NULL || *list == NULL || del_data == NULL) return;
  t_list* to_del;
  while (*list && cmp((*list)->data, del_data)) {
    to_del = *list;
    *list = (*list)->next;
    free(to_del);
  }
  if (!*list) return;
  for (t_list* cur = *list; cur->next;) {
    if (!cmp(cur->next->data, del_data)) {
      cur = cur->next;
      continue;
    }
    to_del = cur->next;
    cur->next = to_del->next;
    free(to_del);
  }
}
