#include "list.h"

void mx_pop_index(t_list**list, int index) {
  if (index <= 0) {
    mx_pop_front(list);
    return;
  }
  t_list *cur = *list;
  for (int i = 1; i <= index && cur->next; i++) {
    if (i == index) {
      t_list *to_delete = cur->next;
      cur->next = to_delete->next;
      free(to_delete);
      return;
    }
    cur = cur->next;
  }
  mx_pop_back(list);
}
